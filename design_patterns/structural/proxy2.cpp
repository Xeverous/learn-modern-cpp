#include <string>
#include <iostream>
#include <memory>
#include <utility>
#include <tuple>
#include <type_traits>

/*
 * A more complex implementation, where proxy also stores arguments
 * that need to be passed when the underlying object is constructed
 *
 * WARNING! this file operates on heavy template magic
 * indented users would not have to care about implementation details, only what is below "// example usage"
 *
 * "This is fine as an exercise in template masturbation but do you really want to have to write
 * call_impl<F, Tuple, 0 == std::tuple_size<ttype>::value, std::tuple_size<ttype>::value>::call(f, std::forward<Tuple>(t))
 * anywhere you want to expand a tuple?" ~Jonathan Wakely 26.05.2012 on https://stackoverflow.com/a/10766422
 *
 * Solution for C++14
 * C++17 soultion would simply use std::apply
 */

template<typename T, typename... Args>
class Lazy
{
public:
	constexpr explicit Lazy(Args&&... args) : args(std::make_tuple(args...))
	{
	}

	void Use()
	{
		std::cout << "Trying to use some of object's functionality\n";

		if (obj == nullptr)
			instantiate(std::index_sequence_for<Args...>());

		obj->Use();
	}

private:
	/*
	 * std::tuple<Args...> is used to store arguments
	 *
	 * because tuple is variadicly templated and has unknown amout of recursive members,
	 * the only way to expand them is to again use recursive variadic templates with std::get
	 */
	std::tuple<Args...> args;
	std::unique_ptr<T> obj;

	template<std::size_t... I>
	void instantiate(std::index_sequence<I...>)
	{
		obj = std::make_unique<T>(std::get<I>(args)...);
	}
};

// factory functon to create objects with automatic type deduction (allows auto var = make_var)
// 1. decay is used to convert eg "foo" from const char (&)[3] to const char* and remove cv-qualifiers
// 2. typename keyword required due to dependent member type
template<typename T, typename... Args> constexpr
decltype(auto) make_lazy(Args&&... args)
{
	return Lazy<T, typename std::decay<Args>::type...>(std::forward<typename std::decay<Args>::type>(args)...);
}

// actual usage

class Object
{
public:
	Object() = delete;

	explicit Object(const std::string& str, int val)
	{
		std::cout << "A heavy construction with str = " << str << " and val = " << val << '\n';
	}

	void Use()
	{
		std::cout << "Object is being used\n";
	}
};

int main()
{
	auto lazy = make_lazy<Object>("some string", 7); // auto = Lazy<Object, std::decay<const char (&)[12]>::type, std::decay<int>::type>
	lazy.Use();
	lazy.Use();
}
