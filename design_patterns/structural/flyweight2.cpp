#include <string>
#include <iostream>
#include <iomanip>
#include <initializer_list>
#include <boost/flyweight.hpp>

/*
 * This example is very similar to the previous one, but boost::flyweight is used instead
 *
 * short tutorial + visualization
 * http://www.boost.org/doc/libs/1_65_0/libs/flyweight/doc/tutorial/index.html
 *
 * type of the stored objects
 * boost::flyweight<T>::value_type
 * type of keys
 * boost::flyweight<T>::key_type
 *
 * Note - this program requires enabled thread support to compile (-pthread for GCC)
 */


class Person
{
public:
	Person(const std::string& first_name, const std::string& last_name)
		: first_name(first_name), last_name(last_name)
	{
	}

	const std::string& GetFirstName() const
	{
		return first_name.get();
	}

	const std::string& GetLastName() const
	{
		return last_name.get();
	}

	// print names and addresses of stored objects
	friend std::ostream& operator<<(std::ostream& os, const Person& person)
	{
		const int length = 10;
		return os << std::setw(length) << person.GetFirstName() << " ["
		          << std::hex << &person.GetFirstName() << "] "
		          << std::setw(length) << person.GetLastName()  << " ["
		          << std::hex << &person.GetLastName()  << "]";
	}

private:
	 boost::flyweight<std::string> first_name, last_name;
};

int main()
{
	auto list =
	{
		Person("John", "Rogers"),
		Person("Rogers", "James"),
		Person("James", "John")
	};

	for (const auto& person : list)
		std::cout << person << '\n';
}

/*
 * Example output (note that same names are stored in one memory place):
 *      John [0xd14e30]     Rogers [0xd14e80]
 *    Rogers [0xd14e80]      James [0xd14ed0]
 *     James [0xd14ed0]       John [0xd14e30]
 */
