#include <string>
#include <cstddef>
#include <iostream>
#include <initializer_list>
#include <boost/bimap.hpp>

/*
 * Flyweight pattern
 * - object's data is stored externally to avoid unnecessary copies
 * - objects store only a key/reference to that data
 * - object's data can only be initialized, modyfying requires to delete and recreate the object
 *
 * The resulting behaviour saves memory by storing only unique instances of data (here: std::string)
 * and giving each object a sort of reference to it (here: Key)
 *
 * This example uses boost's bimap to achieve logarithmic time search from either side to the container's elements
 * bimap is an assiociative container indexing in both (left and right) views,
 * boost::bimap<X, Y>::left  works like std::map<X, Y>
 * boost::bimap<X, Y>::right works like std::map<Y, X>
 *
 * More about bimap + examples
 * http://www.boost.org/doc/libs/1_65_0/libs/bimap/doc/html/boost_bimap/the_tutorial/controlling_collection_types.html
 */

// a type used for indexing
typedef std::size_t Key;

class Person
{
public:
	Person(const std::string& first_name, const std::string& last_name)
		: first_name(Add(first_name)), last_name(Add(last_name))
	{
	}

	const std::string& GetFirstName() const
	{
		return names.left.find(first_name)->second;
	}

	const std::string& GetLastName() const
	{
		return names.left.find(last_name)->second;
	}

	friend std::ostream& operator<<(std::ostream& os, const Person& person)
	{
		return os << person.GetFirstName() << " [" << person.first_name << "] "
		          << person.GetLastName()  << " [" << person.last_name  << "]";
	}

private:
	Key Add(const std::string& name)
	{
		// search for the given string
		auto it = names.right.find(name);
		if (it == names.right.end())
		{
			// if it doesn't exist yet, add it
			Key id = seed++; // generate new unique id
			names.insert(boost::bimap<Key, std::string>::value_type(id, name));
			return id;
		}

		// if it exists already, return it's id
		return it->second;
	}

	const Key first_name, last_name; // strings are not sotred directly, but as keys to the map

	static Key seed;
	static boost::bimap<Key, std::string> names;
};

Key Person::seed = 0;
boost::bimap<Key, std::string> Person::names;

int main()
{
	auto list =
	{
		Person("John", "Rogers"),
		Person("Rogers", "James"),
		Person("James", "John")
	};

	for (const auto& person : list)
		std::cout << person << '\n';
}

/*
 * Output (note how same names got same keys):
 * John [0] Rogers [1]
 * Rogers [1] James [2]
 * James [2] John [0]
 */
