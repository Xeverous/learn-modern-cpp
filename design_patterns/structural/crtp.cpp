#include <iostream>
#include <type_traits>

// base class using CRTP to call functions of derived classes
template <class Derived>
class Animal
{
public:
	// all of the public functions should first cast downwards to directly call members
	void Sound() const
	{
		const_derived().Sound();
	}

private: // the magic is here
	Derived& derived()
	{
		return *static_cast<Derived*>(this);
	}

	const Derived& const_derived() const
	{
		return *static_cast<const Derived*>(this);
	}
};

// note how classes inherit from their own template
class Cat : public Animal<Cat>
{
public:
	void Sound() const
	{
		std::cout << "meow\n";
	}
};

class Dog : public Animal<Dog>
{
public:
	void Sound() const
	{
		std::cout << "whoof\n";
	}
};

template<class Derived>
void CallSoundOnAnyAnimal(const Animal<Derived>& a)
{
	a.Sound();
}

int main()
{
	// proof that both Cat and Dog have no virtual functions
	static_assert(!std::is_polymorphic<Cat>::value, "T has virtual function but should have not");
	static_assert(!std::is_polymorphic<Dog>::value, "T has virtual function but should have not");
	
	Cat cat;
	Dog dog;
	
	CallSoundOnAnyAnimal(cat);
	CallSoundOnAnyAnimal(dog);
}
