#include <iostream>
#include <memory>

/*
 * A proxy is simply an abstraction layer to provide additional functionality
 * example proxies are smart pointers - they expose the same functionality as
 * the underlying type (eg via operator->)
 * but also provide memory handling (via operator.)
 *
 * This example creates a proxy to implement lazy initialization idiom
 * object construction is delayed as much as possible to avoid (possibly expensive and unneeded) operations
 *
 * If the object construction requires some arguments - these can be storem in the proxy,
 * and they will be passed when the construction is needed (see proxy2 example)
 */

class Object
{
public:
	explicit Object()
	{
		std::cout << "A heavy construction\n";
	}

	void Use()
	{
		std::cout << "Object is being used\n";
	}
};

template<typename T>
class Lazy
{
public:
	void Use()
	{
		std::cout << "Trying to use some of object's functionality\n";

		if (obj == nullptr)
			obj = std::make_unique<T>();

		obj->Use();
	}

private:
	std::unique_ptr<T> obj;
};

int main()
{
	{
		Object obj; // wasted time and resources on construction of unneeded object
	}

	std::cout << '\n';

	Object obj;
	obj.Use();
	obj.Use();

	std::cout << '\n';

	Lazy<Object> lazy;
	/* ... possibly multiple operations not requiring object to exist ... */
	lazy.Use(); // actual object is constructed at this moment
	lazy.Use();
}

/*
 * Output:
 * A heavy construction

 * A heavy construction
 * Object is being used
 * Object is being used
 *
 * Trying to use some of object's functionality
 * A heavy construction
 * Object is being used
 * Trying to use some of object's functionality
 * Object is being used
 */

