#include <iostream>
#include <vector>
#include <memory>

/*
 * more advanced example with Undo and Redo feature
 * memntos are stored as shared pointers so that only pointers need to be replicated, not saves themselves (which can be heavy)
 */

class Memento
{
private:
	int balance;

public:
	Memento(int balance) : balance(balance) { } // note non-explcit ctor
	friend class BankAccount;
};

class BankAccount
{
	int balance;
	std::vector<std::shared_ptr<Memento>> changes;
	std::size_t current_change;

	void SetBalance(int new_balance)
	{
		balance = new_balance;
	}

public:
	explicit BankAccount(int balance = 0) : balance(balance)
	{
		changes.emplace_back(std::make_shared<Memento>(balance));
		current_change = 0;
	}

	// any operation should return a state of the system (a backup point)
	std::shared_ptr<Memento> Deposit(int amount)
	{
		SetBalance(balance + amount);

		auto memento = std::make_shared<Memento>(balance);
		changes.push_back(memento);
		current_change++;

		return memento;
	}

	/*
	 * Note for restore feature
	 * - every restore also creates a change, which is saved
	 * - only pointers are copied, mementos themselves are not modified
	 */
	void Restore(const std::shared_ptr<Memento>& memento)
	{
		if (memento)
		{
			SetBalance(memento->balance);

			changes.push_back(memento);
			current_change = changes.size() - 1;
		}
	}

	std::shared_ptr<Memento> Undo()
	{
		if (current_change > 0)
		{
			current_change--;
			auto memento = changes[current_change];
			SetBalance(memento->balance);
			return memento;
		}

		return {};
	}

	std::shared_ptr<Memento> Redo()
	{
		if (current_change + 1 < changes.size())
		{
			current_change++;
			auto memento = changes[current_change];
			SetBalance(memento->balance);
			return memento;
		}

		return {};
	}

	friend std::ostream& operator<<(std::ostream& os, const BankAccount& bank_account)
	{
		return os << "balance: " << bank_account.balance;
	}
};

int main()
{
	BankAccount account;

	std::cout << account << '\n';

	account.Deposit(200);
	account.Deposit(350);

	std::cout << account << '\n';

	account.Undo();
	std::cout << account << '\n';

	account.Undo();
	std::cout << account << '\n';

	account.Redo();
	std::cout << account << '\n';

	account.Redo();
	std::cout << account << '\n';
}
