#include <iostream>

// a snapshot (save) of the system at certain point in time
class Memento
{
private:
	int balance;

public:
	Memento(int balance) : balance(balance) { } // note non-explcit ctor
	friend class BankAccount;
};

class BankAccount
{
	int balance = 0;

public:
	// any operation should return a state of the system (a backup point)
	Memento Deposit(int amount)
	{
		balance += amount;
		return { balance }; // implicit ctor, note the order - in this example saves are returned after an operation
	}

	void Restore(const Memento& memento)
	{
		balance = memento.balance;
	}

	friend std::ostream& operator<<(std::ostream& os, const BankAccount& bank_account)
	{
		return os << "balance: " << bank_account.balance;
	}
};

int main()
{
	BankAccount account;

	std::cout << account << '\n';

	auto m1 = account.Deposit(200);
	auto m2 = account.Deposit(350);

	std::cout << account << '\n';

	account.Restore(m1);
	std::cout << account << '\n';

	account.Restore(m2);
	std::cout << account << '\n';
}

/*
 * Memento patern
 * - saving a snapshot (memento) of the system after an operation
 * - ability to restore system's state to any point back in time
 */
