#include <iostream>
#include <string>
#include <functional>
#include <boost/iterator/iterator_facade.hpp>

/*
 * Iterator - a type that:
 * - can access underlying object (T) in the container<T>
 * - knows how to move across the container (actual moves - forward, backward, random, up/down depend on the container)
 * - allows to implement generic form of algorithms that can operate on any container, there are already multiple ones in <algorithm>
 *
 * In C++ iterators are implemented as template classes to ommit any overhead that would occur due to dependency on container
 * This example uses boost::iterator_facade to ease iterator implementation through CRTP
 */

template <typename T>
class SingleList
{
private:
	struct Node
	{
		T value;
		Node* next;

		Node(Node* const next, const T& value) : value(value), next(next) {}
		Node(Node* const next, T&& value) : value(std::move(value)), next(next) {}

		template<typename... Args>
		Node(Node* const next, Args&&... args) : value(std::forward<Args>(args)...), next(next) { }
	};

	Node* root = nullptr;

public:
	class Iterator : public boost::iterator_facade<Iterator, T, boost::forward_traversal_tag>
	{
	public:
		Iterator() = default;
		explicit Iterator(Node* const current) : current(current) { }

	private:
		Node* current = nullptr;

		friend class boost::iterator_core_access;

		void increment()
		{
			current = current->next;
		}

		bool equal(const Iterator& other) const
		{
			return current == other.current;
		}

		T& dereference()
		{
			return current->value;
		}

		const T& dereference() const
		{
			return current->value;
		}
	};

	class ConstIterator : public boost::iterator_facade<Iterator, const T, boost::forward_traversal_tag>
	{
	public:
		ConstIterator() = default;
		explicit ConstIterator(Node* const current) : current(current) { }

	private:
		Node* current = nullptr;

		friend class boost::iterator_core_access;

		void increment()
		{
			current = current->next;
		}

		bool equal(const Iterator& other) const
		{
			return current == other.current;
		}

		const T& dereference() const
		{
			return current->value;
		}
	};

	SingleList() = default;
	~SingleList()
	{
		while (root)
		{
			Node* temp = root;
			root = root->next;
			delete temp;
		}
	}

	void push(const T& value)
	{
		Node* new_node = new Node(root, value);
		root = new_node;
	}

	void push(T&& value)
	{
		Node* new_node = new Node(root, std::move(value));
		root = new_node;
	}

	template<typename... Args>
	void emplace(Args&&... args)
	{
		Node* new_node = new Node(root, std::forward<Args>(args)...);
		root = new_node;
	}

	Iterator begin() const
	{
		return Iterator(root);
	}

	Iterator end() const
	{
		return Iterator(nullptr);
	}

	ConstIterator cbegin() const
	{
		return ConstIterator(root);
	}

	ConstIterator cend() const
	{
		return ConstIterator(nullptr);
	}
};

int main()
{
	SingleList<const std::string> list; // 1. removing const breaks the code (qualifier discard)
	list.push("one");
	list.push("two");
	list.push("three");

	for (const auto& str : list)
	{
		std::cout << str << '\n';
	}

	std::cout << '\n';

	// adding const breaks the code (no match for operator++)
	// this is normal, because for example STL's const std::vector<T>::iterator != std::vector<T>::const_iterator
	// const_iterator is meant to operate on const T, not to be const by itself
	for (/* const */ auto it = list.cbegin(); it != list.cend(); ++it)
	{
		std::cout << *it << '\n';
	}
}

/*
 * Problems with this code
 * 1. (marked) - investigate
 * 2. ensure SingleList<T>::ConstIterator is correct
 */
