#include <iostream>
#include <vector>

/*
 * Command pattern - intented use
 * - each command represents an action
 * - ability to save commands and undo them (very often a GUI/shortcut option)
 * - multiple commands can be merged into composite macros
 */

class BankAccount
{
	int balance = 0;

public:
	void Deposit(int amount)
	{
		balance += amount;
	}

	void Withdraw(int amount)
	{
		balance -= amount;
	}

	int GetBalance() const
	{
		return balance;
	}
};

class Command
{
public:
	enum class Action { deposit, withdraw };

	Command(BankAccount& account, Command::Action action, int amount)
		: account(account),
		  action(action),
		  amount(amount)
	{
	}

	void Do() const
	{
		switch(action)
		{
		case Action::deposit:
			account.Deposit(amount);
			break;
		case Action::withdraw:
			account.Withdraw(amount);
			break;
		default:
			break;
		}
	}

	void Undo() const
	{
		switch(action)
		{
		case Action::deposit:
			account.Withdraw(amount);
			break;
		case Action::withdraw:
			account.Deposit(amount);
			break;
		default:
			break;
		}
	}

private:
	BankAccount& account;
	Action action;
	int amount;
};

class CommandList
{
public:
	std::vector<Command> data;

	void DoAll()
	{
		for (const auto& cmd : data)
			cmd.Do();
	}

	void UndoAll()
	{
		// commands should be undone in reverse order
		for (auto it = data.crbegin(); it != data.crend(); it++)
			it->Undo();
	}
};

int main()
{
	BankAccount bank_account;
	CommandList command_list;
	command_list.data = std::vector<Command>
	{
		Command(bank_account, Command::Action::deposit,  1000),
		Command(bank_account, Command::Action::withdraw, 1200)
	};

	std::cout << "balance: " << bank_account.GetBalance() << '\n';
	command_list.DoAll();
	std::cout << "balance: " << bank_account.GetBalance() << '\n';
	command_list.UndoAll();
	std::cout << "balance: " << bank_account.GetBalance() << '\n';
}

/*
 * Output:
 * balance: 0
 * balance: -200
 * balance: 0
 */
