#include <iostream>
#include <string>
#include <sstream>
#include <cctype>
#include <vector>
#include <memory>

/*
 * Interpreter - ndented use
 * Writing parsers
 * Note the 2 steps:
 * - lexing (splitiing text into tokens)
 * - parsing (processing tokens in corretc order)
 *
 * A more complex parser would use a parser-building library, such as boost::spirit
 */

struct Token
{
	enum class Type { integer, plus, minus, left_parenthesis, right_parenthesis } type;
	std::string text;

	friend std::ostream& operator<<(std::ostream& os, const Token& token)
	{
		return os << '`' << token.text << '`';
	}
};

std::vector<Token> lex(std::string input)
{
	std::vector<Token> result;

	for (unsigned i = 0; i < input.length(); i++)
	{
		switch (input[i])
		{
		case '+':
			result.push_back(Token{Token::Type::plus, "+"});
			break;
		case '-':
			result.push_back(Token{Token::Type::minus, "-"});
			break;
		case '(':
			result.push_back(Token{Token::Type::left_parenthesis, "("});
			break;
		case ')':
			result.push_back(Token{Token::Type::right_parenthesis, ")"});
			break;
		case ' ':
			break;
		default:
			std::ostringstream buffer;
			buffer << input[i];
			for (unsigned j = i + 1; j < input.length(); j++)
			{
				if (std::isdigit(input[j]))
				{
					buffer << input[j];
					i++;
				}
				else
				{
					result.push_back(Token{Token::Type::integer, buffer.str()});
					break;
				}
			}
		}
	}

	return result;
}

class Element
{
public:
	virtual ~Element() = default;
	virtual int Evaluate() const = 0;
};

class Integer : public Element
{
	int value;

public:
	explicit Integer(int value) : value(value) { }
	int Evaluate() const override { return value; }
};

class BinaryOperation : public Element
{
public:
	enum class Type { addition, subtraction } type;
	std::shared_ptr<Element> left, right;

	int Evaluate() const override
	{
		if (type == Type::addition)
			return left->Evaluate() + right->Evaluate();
		else
			return left->Evaluate() - right->Evaluate();
	}
};

std::shared_ptr<Element> Parse(const std::vector<Token>& tokens)
{
	// simplified implementation - we consider every expression to be a Binary operation
	auto result = std::make_shared<BinaryOperation>();
	bool has_left = false;

	for (unsigned i = 0; i < tokens.size(); i++)
	{
		switch (tokens[i].type)
		{
		case Token::Type::plus:
			result->type = BinaryOperation::Type::addition;
			break;
		case Token::Type::minus:
			result->type = BinaryOperation::Type::subtraction;
			break;
		case Token::Type::integer:
			{
				auto integer = std::make_shared<Integer>(std::atoi(tokens[i].text.c_str()));

				if (has_left)
					result->right = integer;
				else
					result->left = integer;

				has_left = true;
			}
			break;
		case Token::Type::left_parenthesis:
			{
				unsigned j = i;
				for (; j < tokens.size(); j++)
					if (tokens[j].type == Token::Type::right_parenthesis)
						break;

				std::vector<Token> subexpression(&tokens[i + 1], &tokens[j]);
				auto element = Parse(subexpression);

				if (has_left)
					result->right = element;
				else
					result->left = element;

				has_left = true;
				i = j;
			}
			break;
		default:
			break;
		}
	}


	return result;
}

int main()
{
	std::string input("(12 + 5) - (3 - 7)");
	auto tokens = lex(input);

	for (const auto& token : tokens)
		std::cout << token << '\t';

	auto parsed_input = Parse(tokens);

	std::cout << "\nValue: " << parsed_input->Evaluate() << '\n';
}

/*
 * Output:
 * `(`     `12`    `+`     `5`     `)`     `-`     `(`     `3`     `-`     `7`     `)`
 * Value: 21
 */
