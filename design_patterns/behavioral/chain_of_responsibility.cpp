#include <boost/signals2.hpp>
#include <iostream>
#include <string>

struct Query
{
	std::string creature_name;
	enum class Argument { attack, defense } argument;
	int result;

	Query(const std::string& creature_name, const Argument argument, int result)
		: creature_name(creature_name),
		  argument(argument),
		  result(result)
	{
	}
};

struct Game
{
	boost::signals2::signal<void(Query&)> queries;
};

class Creature
{
private:
	Game& game;
	int attack, defense;

public:
	std::string name;

	Creature(Game& game, int attack, int defense, const std::string& name)
		: game(game),
		  attack(attack),
		  defense(defense),
		  name(name)
	{
	}

	int GetAttack() const
	{
		Query q(name, Query::Argument::attack, attack);
		game.queries(q);
		return q.result;
	}

	int GetDefense() const
	{
		Query q(name, Query::Argument::defense, defense);
		game.queries(q);
		return q.result;
	}

	friend std::ostream& operator<<(std::ostream& os, const Creature& creature)
	{
		return os << creature.name << ": " << creature.GetAttack() << '/' << creature.GetDefense();
	}
};

class CreatureModifier
{
private:
	Game& game;
	Creature& creature;

public:
	CreatureModifier(Game& game, Creature& creature)
		: game(game),
		  creature(creature)
	{
	}

	virtual ~CreatureModifier() = default;
};

class DoubleAttackModifier : public CreatureModifier
{
private:
	boost::signals2::connection conn;

public:
	DoubleAttackModifier(Game& game, Creature& creature)
		: CreatureModifier(game, creature)
	{
		conn = game.queries.connect([&](Query& q)
		{
			if (q.creature_name == creature.name and q.argument == Query::Argument::attack)
				q.result *= 2;
		});
	}

	virtual ~DoubleAttackModifier()
	{
		conn.disconnect();
	}
};

class AddedAttackModifier : public CreatureModifier
{
private:
	boost::signals2::connection conn;

public:
	AddedAttackModifier(Game& game, Creature& creature, int bonus)
		: CreatureModifier(game, creature)
	{
		conn = game.queries.connect([&, bonus](Query& q)
		{
			if (q.creature_name == creature.name and q.argument == Query::Argument::attack)
				q.result += bonus;
		});
	}

	virtual ~AddedAttackModifier()
	{
		conn.disconnect();
	}
};

int main()
{
	Game game;
	Creature rat(game, 2, 2, "rat");

	std::cout << rat << '\n';

	{
		DoubleAttackModifier m1(game, rat);
		AddedAttackModifier  m2(game, rat, 3);
		std::cout << rat << '\n';
	}

	{
		AddedAttackModifier  m2(game, rat, 3);
		DoubleAttackModifier m1(game, rat);
		std::cout << rat << '\n';
	}

	std::cout << rat << '\n';
}

/*
 * Intended use
 * rapid changes of mechanics affecting the object
 */

/*
 * Output:
 * rat: 2/2
 * rat: 7/2
 * rat: 10/2
 * rat: 2/2
 */
