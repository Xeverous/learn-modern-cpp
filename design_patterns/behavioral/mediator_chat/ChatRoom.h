#ifndef MEDIATOR_CHATROOM_H_
#define MEDIATOR_CHATROOM_H_
#include <vector>
#include <string>

class User;

class ChatRoom
{
public:
	void Join(User* user);
	void Leave(User* user);

	void MessageAll(User* origin, const std::string& message);
	void MessagePrivate(User* origin, const std::string& target_user, const std::string& message);

private:
	std::vector<User*> users;
};

#endif /* MEDIATOR_CHATROOM_H_ */
