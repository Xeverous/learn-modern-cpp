#include "User.h"

#include <iostream>
#include "global.h"

User::User(const std::string& nick)
	: nick(nick)
{
}

User::~User()
{
	if (room)
		room->Leave(this);
}

void User::Join(ChatRoom* new_room)
{
	if (room)
		room->Leave(this);

	new_room->Join(this);
	room = new_room;
}

void User::MessageAll(const std::string& message)
{
	if (room)
		room->MessageAll(this, message);
	else
		ErrorNotConnected();
}

void User::MessagePrivate(const std::string& target, const std::string& message)
{
	if (room)
		room->MessagePrivate(this, target, message);
	else
		ErrorNotConnected();
}

void User::Receive(const std::string& message)
{
	std::cout << GetRoomName(nick) << ' ' << message << '\n';
}

std::string User::GetNick() const
{
	return nick;
}

void User::Error(const std::string& what)
{
	std::cout << GetRoomName(nick) << " Error: " << what << '\n';
}

void User::ErrorNotConnected()
{
	Error("Not connected to any chat room");
}
