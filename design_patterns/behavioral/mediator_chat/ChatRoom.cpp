#include "ChatRoom.h"

#include <algorithm>
#include "User.h"

void ChatRoom::Join(User* user)
{
	users.push_back(user);

	for (auto& u : users)
		if (u != user)
			u->Receive("<chat>: User \"" + user->GetNick() + "\" joined the room.");
}

void ChatRoom::Leave(User* user)
{
	users.erase(std::find_if(users.begin(), users.end(), [&user](const User* u)
	{
		return u == user;
	}));

	for (auto& u : users)
		u->Receive("<chat>: User \"" + user->GetNick() + "\" left the room.");
}

void ChatRoom::MessageAll(User* origin, const std::string& message)
{
	for (auto& u : users)
		if (u != origin)
			u->Receive(origin->GetNick() + ": " + message);
}

void ChatRoom::MessagePrivate(User* origin, const std::string& target_user, const std::string& message)
{
	auto it = std::find_if(users.begin(), users.end(), [&target_user](const User* user)
	{
		return user->GetNick() == target_user;
	});

	if (it != users.end())
		(*it)->Receive(origin->GetNick() + ": " + message);
	else
		origin->Error("User \"" + target_user + "\" is not online");
}
