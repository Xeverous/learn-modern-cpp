#ifndef MEDIATOR_PERSON_H_
#define MEDIATOR_PERSON_H_
#include <string>
#include "ChatRoom.h"

class User
{
public:
	explicit User(const std::string& nick);
	~User();

	void Join(ChatRoom* new_room);
	void MessageAll(const std::string& message);
	void MessagePrivate(const std::string& target, const std::string& message);

	void Receive(const std::string& message);

	std::string GetNick() const;

	void Error(const std::string& what);

private:
	std::string nick;
	ChatRoom* room = nullptr;

	void ErrorNotConnected();
};

#endif /* MEDIATOR_PERSON_H_ */
