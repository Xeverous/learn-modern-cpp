#include "ChatRoom.h"
#include "User.h"

int main()
{
	ChatRoom room;

	User u1("Alice");
	User u2("Bob");
	User u3("Coryl");
	User u4("Dan");

	u1.Join(&room);
	u2.Join(&room);
	u3.Join(&room);

	u1.MessageAll("Hi everyone!");
	u2.MessageAll("Hi Alice");
	u3.MessageAll("Hi Alice");

	u4.Join(&room);
	u4.MessageAll("Hi everyone");

	u4.MessagePrivate("coryl", "Hi");
	u4.MessagePrivate("Coryl", "Hi");
}

/*
 * Mediator pattern
 * - central object (here: ChatRoom) that handles communication between others (here: Users)
 * - users need only to have pointer/reference to the central object, they do not hold ptrs/refs to each other
 */
