#ifndef MEDIATOR_GLOBAL_H_
#define MEDIATOR_GLOBAL_H_
#include <string>

std::string GetRoomName(const std::string& name);

#endif /* MEDIATOR_GLOBAL_H_ */
