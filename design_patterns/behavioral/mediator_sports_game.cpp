#include <iostream>
#include <string>
#include <boost/signals2.hpp>

// base class for all events
class Event
{
public:
	virtual ~Event() = default;
	virtual void Print() const = 0;
};

class PointsScored : public Event
{
public:
	std::string player_name;
	int points_gained;
	int total_points;

	PointsScored(const std::string& player_name, int points_gained, int total_points)
		: player_name(player_name), points_gained(points_gained), total_points(total_points)
	{
	}

	void Print() const override
	{
		std::cout << "Player " << player_name << " has scored " << points_gained << "points, total: " << total_points << '\n';
	}
};

class Game
{
public:
	boost::signals2::signal<void(Event*)> events; // event bus - stores event subscribers and receives events
};

class Player
{
private:
	Game& game;
	std::string name;
	int total_points = 0;

public:
	Player(Game& game, const std::string& name)
		: game(game), name(name)
	{
	}

	void Score(int points)
	{
		total_points += points;
		PointsScored ps(name, points, total_points);
		game.events(&ps); // add a new event to the bus (all subscribers will be noticed by invoking their connections)
	}
};

// base for all subscribers, can be virtual
class Subscriber
{
protected:
	Game& game;
	boost::signals2::connection conn;

	Subscriber(Game& game) : game(game) { }

public:
	~Subscriber()
	{
		conn.disconnect();
	}
};

class Crowd : public Subscriber
{
public:
	explicit Crowd(Game& game) : Subscriber(game)
	{
		// subscribe to any events happening
		conn = game.events.connect([](Event* event)
		{
			// check if the event is of type PointsScored
			PointsScored* ps = dynamic_cast<PointsScored*>(event); // cast downwards
			if (ps != nullptr and ps->points_gained >= 5)
				std::cout << "Congratulations, " << ps->player_name << '\n';
		});
	}
};

class Referee : public Subscriber
{
public:
	explicit Referee(Game& game) : Subscriber(game)
	{
		// subscribe to any events happening
		conn = game.events.connect([](Event* event)
		{
			// check if the event is of type PointsScored
			PointsScored* ps = dynamic_cast<PointsScored*>(event); // cast downwards
			if (ps != nullptr and ps->total_points >= 10)
				std::cout << "Game is over, " << ps->player_name << " has won\n";
		});
	}
};


int main()
{
	Game game;
	Player p1(game, "Alice");
	Player p2(game, "Bob");
	Crowd crowd(game);
	Referee referee(game);

	p1.Score(4);
	p2.Score(3);
	p1.Score(-2);
	p2.Score(5);
	p1.Score(5);
	p2.Score(-6);
	p2.Score(7);
	p1.Score(2);
	p2.Score(4);
}

/*
 * Mediator here is the Game - central object that receives and passes events to subscribers
 * - any object can subscribe to the event bus
 * - any object can put event onto the bus
 * - when an event happens, all subscribers will be called
 * - central object (game) can have multiple busses for different events (in this case polymorphic pointer + dynamic casting is used)
 */
