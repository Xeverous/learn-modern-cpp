# Learn Modern C++ #
### Repository for learning modern programming, covering C++11, C++14 and even some C++17 features but also other programming-related things such as `git`, general OOP principles, popular idioms, building process (compilation, static/dynamic linking, shared libraries) and more ###

Głownie zamierzam prowadzić po angielsku (na pewno kod i pojęcia), wiki i opisy będą prawdopodobnie po polsku.

Jeśli chodzi o kontakt to pisać na mój @, lub osobiście na studiach ETI - Michał Urbański. Dla chcących przekazywać informacje przez FB - pisać do Szymona Miksa. O mail/telefon proszę pytać osobiście - nie będę takich danych tutaj zamieszczał.

TeamSpeaki, na których jestem:

- `31.6.70.205` (podkanały Path of Exile)
- `pathofexile.teamspeak3.pl` (jeśli ten wyżej nie działa)

___

## Zanim cokolwiek zrobisz - patrz na [**>> wiki <<**](https://bitbucket.org/Xeverous/learn-modern-cpp/wiki/browse/)

___

##### *Obecnie w repo jest wciąż sporo rzeczy do napisania - w całym source można znaleźć sporo TODO / FIXME itd. Prace trwają na bieżąco*