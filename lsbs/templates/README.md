TODO:
- function templates
	- type abstraction (ie multiple overloads)
	- automatic template type deduction
	- non-type template arguments
- class templates
	- containers as example
	- CRTP
	- SFINAE