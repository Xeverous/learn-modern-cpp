/*
 * In this example a function will take it's arguments by address
 *
 * In fact, this version also takes it's argumnets by value
 * - but instead of taking the copy of the value   of variable
 * it takes                  a copy of the address of variable 
 */
 
#include <iostream> 
 
void Duplicate(int* x, int* y, int* z)
{
	// you have to dereference pointers to modify variables
	// (otherwise you would modify addresses of variables)
	*x *= 2;
	*y *= 2;
	*z *= 2;
}

void Write(int x, int y, int z)
{
	std::cout << "x = " << x << "\ny = " << y << "\nz = " << z << '\n';
}

int main()
{
	int x = 1, y = 3, z = 4;
	
	std::cout << "Before:\n";
	Write();
	
	Duplicate(&x, &y, &z); // you have to pass addresses, not variables
	
	std::cout << "After:\n";
	Write();
}
