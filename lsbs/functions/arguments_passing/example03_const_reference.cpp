/*
 * Passing by reference (or by copy of address) has also the advantage of
 * avoiding expensive copies: for trivial types (such as bits, integers, floating-point types, etc)
 * passing by value works well, but for ANY bigger type (that consists of at least 2 trivial types)
 * passing by reference is faster
 *
 * Sometimes you want to take a big data to a function, but do not want to modify it
 * - then you should you use const reference
 *
 * const reference advantages:
 * - avoids expensive copies (compared to pass-by-value)
 * - allows compiler to do additional optimizations
 * - requires compiler to emit error message when you try to modify object taken by const reference
 */
 
#include <iostream>
#include <string>

// this function will copy the entire string object
void Expensive(std::string str)
{
	std::cout << "string contents: ";
	
	for(char c : str)
		std::cout << c << ' ';
	
	std::cout << "\nstring length: " << str.length() << '\n';
}

void Cheap(std::string& str)
{
	std::cout << "string contents: ";
	
	for(char c : str)
		std::cout << c << ' ';
	
	std::cout << "\nstring length: " << str.length() << '\n';
}

void CheapAndSafe(const std::string& str)
{
	std::cout << "string contents: ";
	
	for(char c : str)
		std::cout << c << ' ';
	
	std::cout << "\nstring length: " << str.length() << '\n';
}

int main()
{
	std::string s("some long long text");
	CheapAndSafe(s);
}
