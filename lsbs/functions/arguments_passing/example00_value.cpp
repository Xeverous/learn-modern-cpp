/*
 * By default functions take copies of the given values upon calling them.
 * This means that modifying them in the function body has no real effect
 * - you only modify the given copy.
 *
 * This behaviour can be changed by using:
 * - a pointer   to variable
 * - a reference to variable
 */
 
#include <iostream> 
 
// this function has no visible effect
// it only modifies the copies of it's parameters
void Duplicate(int x, int y, int z)
{
	x *= 2;
	y *= 2;
	z *= 2;
}

void Write(int x, int y, int z)
{
	std::cout << "x = " << x << "\ny = " << y << "\nz = " << z << '\n';
}

int main()
{
	int x = 1, y = 3, z = 4;
	
	std::cout << "Before:\n";
	Write();
	
	Duplicate(x, y, z);
	
	std::cout << "After:\n";
	Write();
}
