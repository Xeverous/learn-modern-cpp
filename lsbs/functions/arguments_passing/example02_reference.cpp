/*
 * The new way to encode the same behaviour is to take parameters by reference
 * 
 * The reference syntax is very similar to the passing-by-value
 * only the function signature changes - instead of "*" we use "&"
 *
 * The generated machine code is the same as for previous example - the addresses
 * of variables are taken. But the reference syntax has a big advantage:
 * it's very easy to switch back-and-forth between reference and pass-by-value
 * because function's body doesn't need to be changed
 */
 
#include <iostream> 
 
// note that this function body is identical to the very first example
// only function signature ha changed
void Duplicate(int& x, int& y, int& z)
{
	x *= 2;
	y *= 2;
	z *= 2;
}

void Write(int x, int y, int z)
{
	std::cout << "x = " << x << "\ny = " << y << "\nz = " << z << '\n';
}

int main()
{
	int x = 1, y = 3, z = 4;
	
	std::cout << "Before:\n";
	Write();
	
	Duplicate(x, y, z); // no need to pass addresses of variables - this is done by the compiler
	
	std::cout << "After:\n";
	Write();
}
