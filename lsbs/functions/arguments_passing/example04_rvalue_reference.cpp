/*
 * C++11 introduced rvalue references (as opposed to lvalue)
 * in short, these references can be used to manipulate temporary (unnamed) objects 
 *
 * rvalue references can not be const - the object is thrown out because it's a temporary
 *
 * a function that takes rvalue reference can also be given an lvalue
 * but the reverse can not be done - you can't pass a temporary object to a function
 * that expects a (const) reference (because it's impossible to take address of a temporary)
 */
 
#include <iostream>
#include <string>

// this function can take a temporary object
void UseTemporary(std::string&& str)
{
	std::cout << "string contents: ";
	
	for(char c : str)
		std::cout << c << ' ';
	
	std::cout << "\nstring length: " << str.length() << '\n';
}

int main()
{
	UseTemporary(std::string("some long long text")); // note that the passed object has no name
}
