Currently not going to explain the basics - for now I will assume everyone knows how a function/variable declaration/definition syntax looks like.

- Variables with static storage (such as global variables) that are not explicitly initialized are automatically initialized to zeroes.
- Variables with automatic storage (such as local variables) that are not explicitly initialized are left uninitialized, and thus have an undetermined value.