
/*
 * Ten przyk�ad r�ni si� od poprzedniego tylko tre�ci� funckji main()
 */

#include <iostream>
using namespace std;

class Polygon
{
protected: //zmienne s� protected - private nie pozwoli�by na ich dziedziczenie
	int width, height;
public:
	void set_values(int a, int b);
};

void Polygon::set_values(int a, int b)
{
	width = a;
	height = b;
}





class Rectangle: public Polygon
{
public:
	int area();
};

int Rectangle::area()
{
	return width * height;
}




class Triangle: public Polygon
{
public:
	int area();
};

int Triangle::area()
{
	return width * height / 2;
}

int main()
{
	Rectangle rect;
	Triangle trgl;

	// tworzymy pointery - ale typ pointera to Wieloko�t!
	// Wa�ne: Instruckja Polygon* p1, p2; stworzy�a by:
	//	- pointer p1 do wielok�ta
	//  - obiekt (nie pointer) p2 (jakby nie by�o tej gwiazdki)
	// problem "gubienia gwiazdki" jest zwi�zany z zachowaniem
	// kompatybilno�ci z j�zykiem C
	// W C++ po prostu zaleca si� tworzy� pointery w osobych liniach
	Polygon* p1;
	Polygon* p2;

	// przypisujemy do pointer�w adresy obiekt�w
	p1 = &rect;
	p2 = &trgl;
	
	// obiekty mo�emy traktowa� jako wielko�ty
	// to w�a�nie przyk�ad Polimorfizmu
	p1->set_values(4, 5);
	p2->set_values(4, 5);
	
	// ale poniewa� funckja area jest r�na dla obu
	// obiekt�w dalej trzeba j� wzywa� przez obiekt,
	// bo pointer Polygona nie widzi funkcji area w
	// Polygonie - klasa matka tej funkcji nie ma
	cout << rect.area() << '\n';
	cout << trgl.area() << '\n';
	return 0;
}

/*
 * W kolejnym przyk�adzie problem z
 * coutem wy�ej zostanie rozwi�zany
 */
