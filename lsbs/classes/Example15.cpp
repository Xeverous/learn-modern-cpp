/*
 * Poni�szy przyk�ad realizuje nowe konwencje
 * zwi�zane z C++11
 *
 * Mianowicie chodzi o 2 nowe identifiery:
 * - final
 * - override
 *
 * S� to indetifiery (nie keywordy), bo poza definicj� klasy nie
 * maj� znaczenia (nie wiem czemu tak zrobiono)
 * Jednak dla zasady nie wypada nazywa� zmiennych jak te s�owa
 *
 * final
 * powoduje, �e funckja wirtualna nie mo�e by� dalej wersjonowana
 * m�wi�c kr�tko, zatrzymuje wirtualno�� funkcji
 * Je�li spr�bujesz doda� kolejn� wersj� funkcji oznaczonej jako final
 * w kolejnej klasie pochodnej, spowoduje to b��d kompilacji
 *
 * override
 * upewnia si�, �e dana funckja wirtualna jest faktycznie wirtualna
 * i �e faktycznie tworzy now� wersj� dla danej klasy
 * W przeciwnym wypadku - b��d kompilacji
 *
 * final i override podaje si� tylko przy deklaracji
 *
 * Poniewa� funckja wirtualna nie traci swej wirtualno�ci do
 * momentu napotkania final, zgodnie z konwencj� wypada pisa�
 * virtual za ka�dym razem w ka�dej kalsie pochodnej
 */

#include <iostream>
using namespace std;

class Polygon
{
protected:
	int width, height;
public:
	void set_values(int a, int b);

	virtual int area() = 0;
	void print_area();
};

void Polygon::set_values(int a, int b)
{
	width = a;
	height = b;
}

void Polygon::print_area()
{
	cout << area() << "\n";
}

class Rectangle: public Polygon
{
public:
	virtual int area() override; //nie pisz� final, bo mo�na by jeszcze stworzy�
	// klas� Square, kt�ra by�a by pochodn� prostok�ta
};

int Rectangle::area()
{
	return width * height;
}




class Triangle: public Polygon
{
public:
	virtual int area() override final;
};

int Triangle::area()
{
	return width * height / 2;
}

int main()
{
	Rectangle rect;
	Triangle trgl;

	Polygon* p1;
	Polygon* p2;

	p1 = &rect;
	p2 = &trgl;

	p1->set_values(4, 5);
	p2->set_values(4, 5);

	p1->print_area();
	p2->print_area();
}


