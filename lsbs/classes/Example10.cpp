/*
 * Ten przyk�ad jest �ci�le powi�zany z kolejnymi
 * 
 * Zwr�� uwag�, jak bardzo podobne s� klasy Triangle i Rectangle
 * - obydwie s� pochodnymi klasy Polygon
 * - obydwie zatem maj� te same zmiennie
 * - obydwie maj� swoj� funckj� area(), ale dzia�a ona inaczej
 * - obydwie maj�       funkcj� set_values(), kt�ra dostali od Polygona
 *
 */

#include <iostream>
using namespace std;

class Polygon
{
protected: //zmienne s� protected - private nie pozwoli�by na ich dziedziczenie
	int width, height;
public:
	void set_values(int a, int b);
};

void Polygon::set_values(int a, int b)
{
	width = a;
	height = b;
}





class Rectangle: public Polygon
{
public:
	int area();
};

int Rectangle::area()
{
	return width * height;
}




class Triangle: public Polygon
{
public:
	int area();
};

int Triangle::area()
{
	return width * height / 2;
}

int main()
{
	Rectangle rect;
	Triangle trgl;
	// Funckj� set_values() mo�na uruchomi� na obu obiektach, poniewa�
	// ka�dy tr�jk�t i prostok�t jest te� wielok�tem
	rect.set_values(4, 5);
	trgl.set_values(4, 5);
	cout << rect.area() << '\n';
	cout << trgl.area() << '\n';
}



