/*
 * Poni�szy przyk�ad jest podobny do poprzedniego, tylko teraz
 * operator+ zosta� stworzony jako funkcja poza klas�,
 * zatem funckja ta teraz musi probra� 2 argumenty
 */
#include <iostream>
using namespace std;

class CVector
{
private:
	int x, y;
public:
	CVector() = default;
	CVector(int a, int b);
	friend CVector operator+(const CVector& lhs, const CVector& rhs);

	friend std::ostream& operator<<(std::ostream& os, const CVector& v);
};

CVector::CVector(int a, int b)
: x(a), y(b)
{
	
}

CVector operator+(const CVector& lhs, const CVector& rhs)
{
	CVector temp;
	temp.x = lhs.x + rhs.x;
	temp.y = lhs.y + rhs.y;
	return temp;
}

std::ostream& operator<<(std::ostream& os, const CVector& v)
{
	os <<  v.x << ',' << v.y << '\n';
	return os;
}

int main()
{
	CVector foo(3, 1);
	CVector bar(1, 2);
	CVector result;
	result = foo + bar;
	cout << foo << bar << result;
	return 0;
}
