
/*
 * keyword this
 * ka�dy obiekt posiada pointer do samego siebie
 * (w rzeczywisto�ci ka�da operacj� wewn�trz obiektu odbywa si� przez ten pointer)
 * keyword this pozwala odwo�a� si� do adresu obiektu, b�d�c w jego wn�trzu
 *
 * keyword this przydaje si� w niekt�rych sytuacjach
 * poni�ej tylko przyk�ad (dosy� g�upi i prosty)
 */
#include <iostream>
using namespace std;

class Dummy
{
public:
	bool isItMe(Dummy& param);
};

bool Dummy::isItMe(Dummy& param)
{
	if (&param == this) // if(adres obiektu podanego jako parametr == adres tego obiektu)
		return true;
	else
		return false;
}

int main()
{
	Dummy a, b;
	Dummy* p = &a; //pointer r�wny adresowi obiektu a

	if (p->isItMe(a))
		cout << "yes\n";
	else
		cout << "no\n";

	if (p->isItMe(b))
		cout << "yes\n";
	else
		cout << "no\n";

	if (a.isItMe(a))
		cout << "yes\n";
	else
		cout << "no\n";

	if (b.isItMe(a))
		cout << "yes\n";
	else
		cout << "no\n";

	if (a.isItMe(b))
		cout << "yes\n";
	else
		cout << "no\n";

	if (b.isItMe(b))
		cout << "yes\n";
	else
		cout << "no\n";

	return 0;
}
