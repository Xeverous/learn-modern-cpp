/*
 * Przyk�ad klas friend�w
 * klasa Rectangle ma funckje Convert, kt�ra pobiera Square
 *
 * Dzieki zaprzyja�nieniu klas wszystkie funckje klasy Rectangle
 * maj� dost�p do prywatnych zmiennych klasy Square
 */

#include <iostream>
using namespace std;

class Square;

class Rectangle
{
private:
	int width, height;
public:
	int area();
	void convert(Square a);
};

int Rectangle::area()
{
	return (width * height);
}

class Square
{
	friend class Rectangle;
private:
	int side;
public:
	Square(int a);
};

Square::Square(int a) : side(a)
{

}

void Rectangle::convert(Square a)
{
	width = a.side;
	height = a.side;
}

int main()
{
	Rectangle rect;
	Square sqr(4);
	rect.convert(sqr);
	cout << rect.area();
	return 0;
}

