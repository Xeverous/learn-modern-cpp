/*
 * Ten plik wprowadzi ci� w koncept klas
 * tu si� w�a�nie zaczyna programowanie obiektowe (OOP)
 * Sporo z ni�ej wymienionych w�a�ciwo�ci odnosi si� te� do struct�w, ale
 * structy u�ywa si� tylko do robienia grup zmiennych, najwy�ej z jedn� funkj�
 * w przeciwnym wypadku zawsze u�ywaj class
 *
 * Przyk�adem bardzo cz�sto u�ywanej klasy jest std::string
 * Najcz�ciej u�ywan� funkcj� tej klasy (poza constructorem) jest prawdopodobnie std::string::operator=
 *
 * - Klasa jest niczym innym jak zbiorem wielu zmiennych
 * - Klasy mog� posiada� w�asne funkcje wewn�trzne, kt�re mo�na uruchamia� tylko
 * 		na istniej�cym obiekcie (st�d nazwy funkcji podaje si� po operatorach :: . ->)
 * - Zdefiniowanie klasy jest r�wnoznaczne ze stworzeniem nowego typu
 * - Zmienne i funkcje wewn�trz klasy mo�na oznaczy� jednym z 3 access specifiers:
 * 		- public
 * 			dost�p do zmiennej/funkcji z zewn�trz
 * 		- protected
 * 			dost�p do zmiennej/funkcji tylko wewn�trz
 * 		- private
 * 			dost�p do zmiennej/funkcji tylko wewn�trz
 * 			zmienna/funckja nie mo�e by� dziedziczona
 * 		Z zewn�trz - kod z poza definicji klasy ma do tego dost�p
 * 		Z wewn�trz - tylko kod wewn�trz definicji ma to tego dost�p (zatem tylko funkcje tej klasy maj� do tego dost�p)
 * - Cz�sto zmienne wewn�trz klasy nazywa si� polami (field) a funkcje metodami (method)
 * - Zmienne b�d�ce instancjami danej klasy nazywa si� obiektami, klasa jest typem tej zmiennej
 * - Dla klas     domy�lnie (je�li nie podasz) pole/metoda jest private
 * - Dla struct�w domy�lnie (je�li nie podasz) pole/metoda jest public
 * - Klasy mog� posiada� wyj�tkowe funkcje, kt�re s� zawsze wzywane przy tworzeniu/destrukcji obiektu, s� to:
 * 		- contructor
 * 		- copy constructor
 * 		- move constructor (C++11)
 * 		- destructor
 * 	- Klasy mo�na dziedziczy�, zachowuj�c pola klasy-matki oraz dodaj�c lub nadpisuj�ch metody klasy-matki
 */

//przyk�ad prostej structury i jej u�ycie
#include <iostream>
#include <string>

struct Fruit{
	unsigned long int bar_code;
	double weight;
	std::string name;
	std::string color;
};

void Out(const Fruit& fruit)
{
	std::cout << fruit.color << " " << fruit.name << " weights " <<
				 fruit.weight << "kg, code: " << fruit.bar_code << "\n";
}

int main()
{
	Fruit apple1;
	apple1.bar_code = 7186001362ul;
	apple1.weight = 0.188;
	apple1.name = "apple";
	apple1.color = "green";

	Fruit apple2;
	apple2.bar_code = 3600029145ul;
	apple2.weight = 0.224;
	apple2.name = "apple";
	apple2.color = "red";

	Fruit banana;
	banana.bar_code = 9784512679ul;
	banana.weight = 0.671;
	banana.name = "banana";
	banana.color = "yellow";

	Out(apple1);
	Out(apple2);
	Out(banana);
}
