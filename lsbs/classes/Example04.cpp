/*
 * Bardzo u�ytecznym featurem C++ jest mo�liwo�� prze�adowywania
 * operator�w dla w�asnych typ�w.
 * 
 * Je�li nie usuniesz (= delete;) operator= to kompilator sam stworzy za ciebie
 * tak� funckj�. Po prostu skopiuje wszystkie pola
 */

/*
 * friend
 * ten keyword pozwala deklarowa� funckje zaprzyja�nione
 * - takie funkcje maj� dost�p do prywatnych p�l/metod
 * - takie funckje nie nale�� do klasy (w sensie scopa)
 *
 * Wa�ne:
 * - friendship nie jest obustronny (tylko w 1 stron�, jak chcesz w obie - zadeklaruj w obu klasach)
 * - friendship nie jest dziedziczony (o dziedziczeniu w dalszych przyk�adach)
 */

#include <iostream>
using namespace std;

class CVector
{
private:
	int x, y;
public:
	CVector() = default; //let the compiler do it
	CVector(int a, int b);
	CVector operator+(const CVector&); //funkcja pobiera drugi obiekt (w pierwszym ju� jeste�my)
	//CVector operator=(const CVector&); //kompilator zrobi i tak, chyba, �e usuniesz

	//pozw�l tej funkcji na dost�p do zmiennych - funckja ta nie b�dzie funckj� klasow�
	//zwr�� uwag�, �e keyword podaje si� tylko przy deklaracji
	// zwr�� uwag� na return - nie jest void - wyja�nienie ni�ej
	friend std::ostream& operator<<(std::ostream& os, const CVector& v);
};

CVector::CVector(int a, int b)
: x(a), y(b)
{
	
}

CVector CVector::operator+(const CVector& param)
{
	CVector temp;
	temp.x = x + param.x;
	temp.y = y + param.y;
	return temp;
}

//ta funkcja nie jest w klasie, ale ma dost�p do prywatnych zmiennych
//zwr�� uwag�, �e nazwa to operator<< a nie CVector::operator<<
// Zwr�� uwage, �e funkja nie jest void tylko zwraca zpowrotem referencje do streama
// umo�liwi to wykonanie tak zwanego "operator chaining" (wykonianie funkcji na warto�ci zwr�conej)
std::ostream& operator<<(std::ostream& os, const CVector& v)
{
	os <<  v.x << ',' << v.y << '\n';
	return os;
}

int main()
{
	CVector foo(3, 1); //will use specialized ctor
	CVector bar(1, 2); //will use specialized ctor
	CVector result;		//will use default ctor
	result = foo + bar; // foo i bar s� obiektami, nast�pi u�ycie operator�w + i =
	cout << foo << bar << result; //operator chaining, bez zwrotu streama nie by�o by to mo�liwe
	return 0;
}
