/*
 * Poni�szy przyk�ad w opisuje klasy abstrakcyjne
 * 
 * Klasy, kt�re posiadaj� funkcje wirtualne
 * ale te funkcje s� bez definicji
 * nazywa si� klasami abstrakcyjnymi takie funckje
 * funkcjami pure virtual
 * 
 * Nie mo�na tworzy� obiekt�w klas abstrakcyjnych!
 * Jest to bardzo u�yteczny feature, bo zapezpiecza kod
 * przed stworzeniem nieu�ytecznego obiektu
 */
#include <iostream>
using namespace std;

class Polygon
{
protected:
	int width, height;
public:
	void set_values(int a, int b);
	
	//dla funkcji wirtualnych nie pisze si�
	// = delete tylko = 0
	virtual int area() = 0; 
};

void Polygon::set_values(int a, int b)
{
	width = a;
	height = b;
}


class Rectangle: public Polygon
{
public:
	int area();
};

int Rectangle::area()
{
	return width * height;
}




class Triangle: public Polygon
{
public:
	int area();
};

int Triangle::area()
{
	return width * height / 2;
}

int main()
{
	Rectangle rect;
	Triangle trgl;
	// Polygon pol; //error: cannot create object of abstract class ...

	//Zwr�� uwag�, �e o ile nie mo�emy stworzy� obiektu klasy abstrakcyjnej
	//to pointer klasy abstrakcyjnej da si� bez problemu
	//(pointer tylko wskazuje na obiekt, nie tworzy czegokolwiek)
	Polygon* p1;
	Polygon* p2;

	p1 = &rect;
	p2 = &trgl;

	p1->set_values(4, 5);
	p2->set_values(4, 5);

	cout << p1->area() << '\n';
	cout << p2->area() << '\n';
}

/*
 * Innym dobrym przyk�adem klasy abstrakcyjnej by�a by klasa Animal
 * z dwoma pochodnymi klasami Cat i Dog
 * wtedy funkcj� wirtualn� by�a by na przyk�ad funckja daj_g�os
 * nie ma ona �adnego sensu dla "Animal" dlatego obiektu takiego nie da si� stworzy�
 */


