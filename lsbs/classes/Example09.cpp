/*
 * Dziedziczenie (Inheritance) to drugi z g��wnych koncept�w OOP
 *
 * M�wi�c kr�tko, dziedziczenie to ... dziedziczenie (zachowanie)
 * p�l i metod z klasy-matki
 *
 * C++ nie ogranicza w �adem spos�b dziedziczenia
 * Ka�da klasa mo�e mie� tyle klas-dziedzi i klas-matek ile si� chce
 *
 * (wiele j�zyk�w, jak np Java narzuca ograniczenia na np. ilo�� rodzic�w)
 * Java wgl ma z tym jaki� problem, bo nie wprowadzi�a klas wirtualnych,
 * tylko "interfacy". Java nie ma szablon�w, tylko "generics". Bieda.
 *
 *
 * W niekt�rych przypadkach powstaje tak zwany problem diamentowy
 * O tym problemie i jak C++ to rozwi�zuje w dalszych przyk�adach
 */

/*
 * Co nie jest kiedykolwiek dziedziczone:
 * - constructors
 * - destructors
 * - assignment operator members (operator=)
 * - friends
 * - private fields/methods
 */

/*
 * S� 3 rodzaje dziedziczenia:
 * - public
 * - protected
 * - private
 *
 * public
 * dziedziczy publiczne pola i metody jako publiczne
 * dziedziczy protected pola i metody jako protected
 *
 * protected
 * dziedziczy publiczne pola i metody jako protected
 * dziedziczy protected pola i metody jako protected
 *
 * private
 * dziedziczy publiczne pola i metody jako private
 * dziedziczy protected pola i metody jako private
 *
 * Wszystkie te zasady s� przedstwaione na obrazku w tabelce
 * obrazek jest w tym samym folderze co te przyk�ady
 */

//Je�li mamy ci�g dziedziczenia
// - klasa matka: a
// - b jako pochodna a
// - c jako pochodna b
// - d jako pochodna c
// - itd
// to rodzaj kolejnych dziedzicze� stosuje si� taki sam lub bardziej restrykcyjny w stosunku do klasy wy�ej
// Zwr�� uwag�, �e je�li jaka� klasa dziedziczy w spos�b private, to kolejne klasy nie maj�
// ju� czego dziedziczy�, chyba, �e dodasz nowe funckje/pola publiczne/protected

//Domy�lnie (i w 90% przypadk�w)
//klasy dziedziczy si� w spos�b publiczny i tylko publiczny

// WA�NE
// Zwr�� uwag� w poni�szym przyk�adzie, �e klasy dziedziczone b�d� wzywa�
// ctor i dtor klasy-matki
// Zwr�� uwag� na kolejno�� tych zdarze�

#include <iostream>
using namespace std;

class Mother
{
public:
	Mother();
	~Mother();
};

Mother::Mother()
{
	cout << "Mother object created\n";
}
Mother::~Mother()
{
	cout << "Mother object destroyed\n";
}

class Daughter: public Mother
{
public:
	Daughter();
	~Daughter();
};

Daughter::Daughter()
{
	cout << "Daugther object created\n";
}
Daughter::~Daughter()
{
	cout << "Daugther object destroyed\n";
}

class Son: public Mother
{
public:
	Son();
	~Son();
};

Son::Son()
{
	cout << "Son object created\n";
}
Son::~Son()
{
	cout << "Son object destroyed\n";
}

int main()
{
	{
		Daughter d;
	}
	cout << "\n";
	{
		Son s;
	}
}

