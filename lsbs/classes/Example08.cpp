/*
 * Klasy mog� posiada� tak�e:
 * - zmienne statyczne
 * - funckje statyczne
 *
 * Zmienna (pole) statyczne jest wsp�lne dla wszystkich obiekt�w tego typu
 * Zmienna ta istnieje globalnie, ale jest w scopie klasy
 * W tym przyk�adzie zmienna Dummy::n b�dzie zlicza� ilo�� istniej�cych obiekt�w tego typu
 *
 * Funkcja statyczna dzia�a identycznie dla wszystkich obiekt�w.
 * W zasadzie to jak w przypadku zmiennej - nie jest powi�zana ona z �adnym obiektem
 *
 *
 * Zmienne i funkcje statyczne b�d� istnie� nawet bez istnienia jakichkolwiek obiekt�w
 * Do zmiennch/funkcji statycznym mo�na odwo�ywa� si� przez obiekt (d1.n), ale jest to
 * niezalecane (poprawniej jest d1::n, poniewa� nie s� one powi�zane z jakimkolwiek obiektem)
 */

#include <iostream>
using namespace std;

class Dummy
{
private:
	static int n;
public:
	Dummy(); //ctor
	~Dummy();//dtor
	static void Amount();
};

Dummy::Dummy()
{
	n++;
}

Dummy::~Dummy()
{
	n--;
}

//Zwr�� uwag�, �e keyword static podaje si� tylko przy deklaracji zmiennej/funkcji
int Dummy::n = 0; //Inicjacja zmiennej statycznej - mo�liwa tylko poza obszarem definicji klasy

//Znowu - Zwr�� uwag�, �e keyword static podaje si� tylko przy deklaracji zmiennej/funkcji
void Dummy::Amount()
{
	cout << Dummy::n << '\n';
}

int main()
{
	Dummy::Amount();
	{
		Dummy a;
		Dummy::Amount();
		{
			Dummy b[5];
			Dummy::Amount();
		}
		Dummy::Amount();
		Dummy* c = new Dummy;
		Dummy::Amount();
		delete c;
		Dummy::Amount();
	}
	Dummy::Amount();
}

