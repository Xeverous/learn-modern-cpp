/*
 * Constructor (ctor skr�tem) to funkcja wzywana przy tworzeniu obiektu danje klasy
 * - constructor jest specjaln� funckj�, nie ma jakiegokolwiek zwrotu (nawet void)
 * - funckja b�d�ca ctorem ma tak� sam� nazw� co klasa
 * - default constructor to ctor, kt�ry nie pobiera �adnych argument�w
 * - specialized ctor to ctor pobieraj�cy jakie� argumenty
 * - ctor mo�e mie� wiele overload�w (jak jak zwyk�e funckje)
 * - ctor mo�e by� uruchomiony tylko raz (podczas tworzenia obiektu)
 * - je�li nie zdefiniujesz �adnego ctora kompilator automatycznie stworzy default ctor
 * - default ctor - mo�na wymusi� jego stworzenie (= default) albo zabroni� (= delete)
 * 		usuwanie default ctora jest u�yteczne, je�li chcesz si� upewni�, �e w twoim kodzie
 * 		nigdzie nie powstaje obiekt przez przypadek (default ctor jest cz�sto niechciany,
 * 		bo nie inicjuje jakichkolwiek zmiennych)
 * 	- ka�da klasa musi mie� przynajmniej 1 ctor i dtor aby m�c stworzy� obiekt
 * 		definicja bez jakichkolwiek ctor�w/dtor�w jest prawid�owa, ale ... ma�o u�yteczna
 */

/*
 * Rodzaje:
 * - default ctor      - brak argument�w
 * - specialized ctor  - pobiera argumenty zdefiniowane przez ciebie
 * - copy ctor         - pobiera 1 argument - const  referecje  (&)  do innego obiektu tego typu
 * 		(referencja jest const, bo zadaniem jest wykonanie kopii obiektu - bez modyfikacji)
 * 		je�li nie usuniesz copy ctora kompilator zrobi go za ciebie (kopiuj�c wszystkie dane)
 * - move ctor (C++11) - pobiera 1 argument - rvalue referencje (&&) do innego obiektu tego typu
 *  	zadaniem move ctora jest przeniesienie adres�w danych do innego obiektu, w efekcie nie kopiuj�c,
 *  	tylko kradn�c dane
 *  	move ctor ma sens tylko, gdy cz�� p�l jest w pami�ci dynamicznej - tylko
 *  	takie zmienne da si� ukra�� (kradzie� polega na kopii warto�ci pointera, a nie danych,
 *  	pointer klasy przenoszonej si� zeruje (x = nullptr))
 *  	(poniewa� pointer nie wskazuje do �adnego adresu dtor tych danych nie usunie)
 */

/*
 * Destructor
 * - wzywany automatycnie, kiedy obiekt jest niszczony (koniec czasu �ycia)
 * - dtor nie mo�e pobiera� jakichkolwiek argument�w
 * - dtor mo�e by� wirtualny (o tym w kolejnych przyk�adach)
 */

/*
 * Inne funckje specjalne:
 * - copy-assignment operator (kompilator stworzy i tak, chyba, �e usuniesz)
 * - move-assignment operator
 *
 * Celem tych funkcji jest zazwyczaj robienie tego samego co copy i move ctor,
 * ale ctory mog� by� wezwane tylko podczas tworzenia obiektu, a te funckje zawsze
 *
 * Funckje te zwracaj� te� referencje do samego obiektu umo�liwiaj�c
 * tak zwany operator chaining (wi�cej o tym w przykladzie 05)
 */

/*
 * Tabelka co i kiedy jest automatycznie robione
 */
//Member function							implicitly defined:														default definition:
//Default constructor			if no other constructors																does nothing
//Destructor					if no destructor																		does nothing
//Copy constructor				if no move constructor and no move assignment										copies all members
//Copy assignment				if no move constructor and no move assignment										copies all members
//Move constructor				if no destructor, no copy constructor and no copy nor move assignment				 moves all members
//Move assignment				if no destructor, no copy constructor and no copy nor move assignment				 moves all members


// W poni�szym przyk�adzie podaj� wzory deklaracji wszystkich ctor�w
// ale ich zastosowanie jest pokazane w kolejnych przyk�adach

#include <iostream>
using namespace std;

class Rectangle
{
private:
	int width, height;
public:
	Rectangle() = delete; 	 //usuni�ty default ctor - nie b�dzie mo�na stworzy� obiektu domy�lnie
	Rectangle(int a, int b); //specialized ctor
	Rectangle(const Rectangle& other) = default; //copy ctor
	Rectangle(Rectangle&& other) = delete; //move ctor

	Rectangle& operator= (const Rectangle&) = delete; // copy-assignment operator
	Rectangle& operator= (Rectangle&&) = delete; // move-assignment operator

	~Rectangle(); //dtor
	int area();
}; //definicje klas ko�czy si� �rednikiem

Rectangle::Rectangle(int a, int b)
{
	width = a;
	height = b;
}

Rectangle::~Rectangle()
{
	cout << "Object destroyed\n";
}

int Rectangle::area()
{
	return width * height;
}

int main()
{
	//Rectangle rect; // error: use of deleted function 'Rectangle::Rectangle()'
	//zwr�� uwag�, �e je�li chcesz stworzy� obiekt domy�lnie to nie pisze si�
	//Rectangle rect(); tylko Rectangle rect;
	//poniewa� to pierwsze by�o by deklaracj� funckji bez parametr�w o nazwie rect, zwracaj�c� Rectangle

	Rectangle recta(3, 4);
	Rectangle rectb(5, 6);
	//rectb.width = 10; //error: Rectangle::width is private
	cout << "recta area: " << recta.area() << "\n";
	cout << "rectb area: " << rectb.area() << "\n";
	return 0;
}

















