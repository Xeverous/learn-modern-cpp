
/*
 * Bardzo cz�sto pola si� ustawia si� na private
 * dost�p do nich jest wtedy przez publiczne funckje
 * funckje te nazywa si� accessorami
 * funkcje te maj� na celu sprawdzenie poprawno�ci danych
 * (np aby nie stworzy� obiektu typu Triangle o warto�ciach 1, 2, 100)
 * dla konwencji cz�sto nazwy tych funkcji zaczynaj� si� od
 * getXXXXX() (odczyt zmiennych) lub setXXXXXX() (zapis do zmiennych)
 *
 * funkcje get cz�sto s� funckjami const, poniewa� nie modyfikuj� czegokolwiek
 */

#include <iostream>
#include <cmath>

class Triangle{
private:
	unsigned a, b, c;
public:
	Triangle() = delete;
	Triangle(unsigned a_, unsigned b_, unsigned c_);

	unsigned getA() const;
	unsigned getB() const;
	unsigned getC() const;
	void setA(unsigned val);
	void setB(unsigned val);
	void setC(unsigned val);

	double Area();
};

unsigned Triangle::getA() const
{
	return a;
}

unsigned Triangle::getB() const
{
	return b;
}

unsigned Triangle::getC() const
{
	return c;
}

void Triangle::setA(unsigned val)
{
	if(val != 0)
		a = val;
}

void Triangle::setB(unsigned val)
{
	if(val != 0)
		b = val;
}

void Triangle::setC(unsigned val)
{
	if(val != 0)
		c = val;
}

Triangle::Triangle(unsigned a_, unsigned b_, unsigned c_)
{
	//warukek istnienia tr�jk�ta (napisany odwrotnie)
	if(a_ + b_ <= c_
	or a_ + c_ <= b_
	or b_ + c_ <= a_)
	{	//warunek niespe�niony, ustawmy co� innego
		 a = b = c = 1; //kojarzysz ��czenie << dla cout?
	}
	else //warunek se�niony, mo�na przypisa�
	{
		a = a_;
		b = b_;
		c = c_;
	}
}

double Triangle::Area()
{
	double s = static_cast<double>(a + b + c) / 2.0;
	return std::sqrt(s * (s - a) * (s - b) * (s - c)); //Wz�r Herona
}

int main()
{
	//mo�esz si� pobawi� i sprawdzi�, czy wszystko dzia�a :)
	Triangle t1(5, 1, 6);
	std::cout<< t1.Area() << "\n";
}


