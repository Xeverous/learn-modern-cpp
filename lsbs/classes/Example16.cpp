/*
 * Ten przyk�ad reprezentuje g��wne zastosowanie
 * klas polimorficznych - u�ywanie containera pointer�w klasy-matki
 *
 * Doda�em te� ctory dla ka�dej klasy pochodnej
 */

#include <iostream>
#include <vector>
#include <memory>
using namespace std;

class Polygon
{
protected:
	int width, height;
public:
	void set_values(int a, int b);

	virtual int area() = 0;
	void print_area();
};

void Polygon::set_values(int a, int b)
{
	width = a;
	height = b;
}

void Polygon::print_area()
{
	cout << area() << "\n";
}

class Rectangle: public Polygon
{
public:
	virtual int area() override;
	Rectangle(int a, int b);
};

Rectangle::Rectangle(int a, int b)
// intializer list mo�e tylko ustawia� zmienne danej klasy,
// je�li chcesz ustawi� zmienne klasy-matki musisz to zrobi� w ciele ctora
{
	width = a;
	height = b;
}

int Rectangle::area()
{
	return width * height;
}

class Square: public Rectangle
{
public:
	Square(int side);
	virtual int area() override final;
};

Square::Square(int side)
: Rectangle(side, side) //musimy wezwa� ctor rectangla z odpowiednimi argumentami,
// bo rectangle nie ma default ctora (bo dali�my mu w�asny)
{
	height = 0;
}

int Square::area()
{
	return width * width;
}

class Triangle: public Polygon
{
public:
	Triangle(int a, int h);
	virtual int area() override final;
};

Triangle::Triangle(int a, int h)
{ //jak w komentarzach wy�ej
	width = a;
	height = h;
}

int Triangle::area()
{
	return width * height / 2;
}

int main()
{
	std::vector< std::unique_ptr<Polygon> > vect;
	vect.emplace_back( new Triangle(10, 10) );
	vect.emplace_back( new Rectangle(10, 10) );
	vect.emplace_back( new Square(10) );

	// emplace_back jest taki sam, co push_back,
	// ztym, �e zamiast pcha� kopi� obiektu konstruuje obiekt
	// odrazu na miejscu


	// pomy�l, ile mo�liwo�ci taki polimorfizm zapewnia
	// wyobra� sobi�, gre RPG, gdzie mamy ca�� mas� takich container�w pointer�w
	// oraz ca�� siatk� klas
	// mo�emy mie� container pointer�w obiekt�w, kt�re maj� inteligencj� (ruch do wykonania)
	// kt�re maj� fizyk� do obliczenia
	// kt�re maj� pozycj�/stan do zapisania ... itd
	// np gracz by�by  pochodnymi: Position, Physics, Control
	// skrzynka by�aby pochodnymi: Position, Physics
	// potw�r   by�by  pochodnymi: Position, Physics, Intelligence
	// NPC      by�by  pochodnymi: Position, Quest

	for(auto& i : vect) //przez referencje, aby nie robi� zb�dnych kopii
		i->print_area();

}

