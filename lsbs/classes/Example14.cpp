
/*
 * Zwyk�e (niewirtualne) funkcje te� mog� wzywa� funkje wirtualne
 * tu tak� funkcj� jest print_area
 * 
 * akurat ten feature rzadko si� wykorzystuje
 * cz�ciej funckje wirtualne wzywa si� bezpo�rednio
 */

#include <iostream>
using namespace std;

class Polygon
{
protected:
	int width, height;
public:
	void set_values(int a, int b);
	
	virtual int area() = 0; 
	void print_area();
};

void Polygon::set_values(int a, int b)
{
	width = a;
	height = b;
}

void Polygon::print_area()
{
	cout << area() << "\n";
}

class Rectangle: public Polygon
{
public:
	int area();
};

int Rectangle::area()
{
	return width * height;
}




class Triangle: public Polygon
{
public:
	int area();
};

int Triangle::area()
{
	return width * height / 2;
}

int main()
{
	Rectangle rect;
	Triangle trgl;
	// Polygon pol; //error: cannot create object of abstract class ...

	Polygon* p1;
	Polygon* p2;

	p1 = &rect;
	p2 = &trgl;

	p1->set_values(4, 5);
	p2->set_values(4, 5);

	p1->print_area();
	p2->print_area();
}

