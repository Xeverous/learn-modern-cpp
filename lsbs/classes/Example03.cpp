/*
 * Poni�szy przyk�ad ilustruje u�ycie Initializer List
 * - jest to spos�b na inicjowanie p�l przed dzia�aniem funkcji
 * - jest to jedyny spos�b na inicjowanie p�l const
 * - jest to jedyny spos�b na inicjowanie p�l, kt�re nie mo�na zkonstruowa� domy�lnie
 */
#include <iostream>
using namespace std;

class Circle
{
private:
	double radius;
public:
	Circle(double r); //specialized ctor
	double area();
};

Circle::Circle(double r)
: radius(r) //Initializer List - zwr�� uwag� na : po nawiasach ctora
{

}

double Circle::area()
{
	return radius * radius * 3.14159265;
}



class Cylinder
{
private:
	Circle base;  //klasa Cylinder zawiera obiekt base o typie Circle
	double height;
public:
	Cylinder(double r, double h); //specialized ctor
	double volume();
};

Cylinder::Cylinder(double r, double h)
: base(r), height(h) //Initializer List - zwr�� uwag� na : po nawiasach ctora
{
	//poniewa� obiekt Circle nie ma default-ctora konieczne jest
	//w initializer list podanie argument�w do jego konstrukcji
}



double Cylinder::volume()
{
	return base.area() * height;
}

int main()
{
	Cylinder foo(10, 20);

	cout << "foo's volume: " << foo.volume() << '\n';
	return 0;
}



