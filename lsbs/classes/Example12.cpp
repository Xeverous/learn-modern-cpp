
/*
 * Ten przyk�ad r�ni si� od poprzedniego tym, �e do klasy
 * Wielok�ta wprowadzono funckje wirtualn�
 * 
 * Funkcje wirtualne s� identyczne pod ka�dym wzgl�dem do
 * zwyk�ych funkcji klasowych, z tym, �e:
 *  - ich tre�� (definicja) mo�e mie� w�asn� wersj� w klasach pochodnych
 *  - wirtualno�� funckji jest dziedziczona (tzn funckja mo�e by�
 *  dalej wersjonowana w klasach pochodnych klas pochodnych itd)
 *  - wersjonowanie funckji wirtualnej nie jest obowi�zkowe
 *  	je�li tego nie dokonasz, zostanie wezwana wersja z najbli�szej
 *  	klasy-matki; je�li takiej nie b�dzie - b��d kompilacji
 *  
 *  Gdyby funkcja area nie by�a oznaczona jako wirtualna,
 *  to funkcje area w prostok�cie i tr�jk�cie by�y by osobnymi,
 *  ro�nymi funkcjami
 *  
 *  Dzi�ki zastosowaniu keyworda virtual mo�na uruchomi� t� funckj�
 *  (np przez pointer) bez dok�adnej wiedzy, czy ten wielok�t
 *  to prostok�t czy tr�jk�t - a i tak si� wykona jej wersja dla danej klasy
 */
#include <iostream>
using namespace std;

class Polygon
{
protected:
	int width, height;
public:
	void set_values(int a, int b);
	virtual int area(); //Zwr�� uwag�, �e keyword virtual podaje si� tylko przy deklaracji
};

void Polygon::set_values(int a, int b)
{
	width = a;
	height = b;
}

//generalnie ta wersja tej funkcji nie powinna by� u�ywana
int Polygon::area() //zwracamy 0 aby oznaczy�, �e nieznamy jaka to figura
{
	return 0;
}




class Rectangle: public Polygon
{
public:
	int area();
};

int Rectangle::area()
{
	return width * height;
}




class Triangle: public Polygon
{
public:
	int area();
};

int Triangle::area()
{
	return width * height / 2;
}

int main()
{
	Rectangle rect;
	Triangle trgl;
	Polygon pol; //mo�emy stworzy� obiekt typu Polygon, ale
	// taki obiekt nie ma du�ej u�yteczno�ci

	Polygon* p1;
	Polygon* p2;

	p1 = &rect;
	p2 = &trgl;

	p1->set_values(4, 5);
	p2->set_values(4, 5);

	//Teraz mo�emy uruchomi� funckje area
	//przez pointer. Dzi�ki keywordowi
	//virtual zostanie uruchomiona jej poprawna wersja
	//dla obu obiekt�w
	// Jest to kolejny, g��bszy przyk�ad Polimorfizmu
	cout << p1->area() << '\n';
	cout << p2->area() << '\n';
	
	//Je�li by zakomentowa� keyword virtual przy deklaracji
	//funckji area w klasie Polygon
	//to rezultatem wykonania tego programu by�y by dwa zera
}

