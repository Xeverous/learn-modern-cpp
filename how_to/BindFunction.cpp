/*
 * Bardzo pote�ny szablon
 * 
 * Tworzy funkcje, usuwaj�c potrzeb� wpisywania argument�w
 * 
 * Dowoln� funkcj� mo�na sprowadzi� do typu:
 * std::function<void()>
 * 
 * wyobra� sobie vector funkcji i p�tle po tym vectorze...
 * lub uruchamianie pasuj�cej funkcji z std::map<key, func>
 * dla pasuj�cego klawisza w jakiej� grze
 */

// std::function<T&&...>() i std::bind(Args&&...) mo�na znale�� w:
#include <functional>


// przyk�ady z SO:
// http://stackoverflow.com/documentation/c%2b%2b/2294/stdfunction-to-wrap-any-element-that-is-callable

// przyk�ady std::bind z en.cppreference:
// http://en.cppreference.com/w/cpp/utility/functional/bind

