/*
 * There are no include guards here because
 * source files should never be included
 */

#include <utility>
#include "Example.h"

// default c'tor
Example::Example():
id(0), x(0), y(0), aloc(0), p(nullptr) 	// C++11 Initializer list - the only way to initialize const members
{
	// we dont need to call explicitly string's ctor because it 
}

// specialized ctor
Example::Example(double x, double y, int aloc):
x(x), y(y),	aloc(aloc) // recomended to use the same names - it's valid and compiler will understand
{
	p = new int[aloc];
}

//specialized ctor
Example::Example(const std::string str):
id(0), x(0), y(0), aloc(0), p(nullptr), str(str) // recomended to use the same names - it's valid and compiler will understand
{

}

// copy c'tor
// you can access fields of the given object - it's the same type
Example::Example(const Example& other)
{
	x = other.x;
	y = other.y;
	str = other.str; // this will call std::string::operator=
	
	NewAllocation(other.aloc)
	
	// copy whole memory content
	for(int i=0; i < other.aloc; i++)
		p[i] = other.p[i];
}

// copy assignment operator
// usually we want to do the same as copy ctor
Example& Example::operator= (const Example& other)
{
	Example tmp(other); // avoid code duplication by calling copy ctor
	std::swap(*this, tmp); // then swap copied object with the current one
	
	return *this; // and return currect object (just to allow operator chaining)
}

// dtor
Example::~Example()
{
	FreeAllocation();
}

// move c'tor
Example::Example(Example&& other)
{
	// there is no difference between moving and copying basic types
	x = other.x;
	y = other.y;
	
	str = std::move(other.str); // string will do internally what we do 2 lines below
	
	// dont copy whole memory content - just steal the pointer
	FreeAllocation();
	p = other.p; // now the memory is ours
	other.p = nullptr // prevent other object from releasing stolen address
}

//move assignment operator
Example& Example::operator=(Example&& other)
{
	// jezeli nie masz potrzeby robi� tego inaczej to zr�b to samo co move c'tor
	Example tmp(std::move(other));
	std::swap(*this, other);
	return *this;
}

double Example::GetX() const
{
	return x;
}

double Example::GetY() const
{
	return y;
}

std::size_t Example::GetAllocationSize() const
{
	return aloc;
}

void Example::FreeAllocation()
{
	if(p)
		delete[] p;
}

void Example::NewAllocation(std::size_t size)
{
	FreeAllocation();
	
	p = new int[size];
	aloc = size;
}

// static function - the keyword need not to be placed here
// (see friend function below)
std::string Example::GetDefaultString()
{
	return "foo bar baz";
}

bool Example::operator==(const Example& other) const {
	return (x * y == other.x * other.y);				//actual comparition
}

bool Example::operator!=(const Example& other) const {
	return !(*this == other);							//no code duplication
}

bool Example::operator< (const Example& other) const {
	return (x * y < other.x * other.y);					//actual comparition
}

bool Example::operator<=(const Example& other) const {
	return !(*this > other);							//no code duplication
}

bool Example::operator> (const Example& other) const {
	return (other < *this);								//no code duplication
}

bool Example::operator>=(const Example& other) const {
	return !(*this < other);							//no code duplication
}

// note that "friend" is not specified here - this is because code itself
// does not care - "friend" is just a feature of the language

// also note that these function are not in the class namespace (there is no Example::)

std::ostream& operator<<(std::ostream& os, const Example& example)
{
	os << example.str;
	return os;
}

std::istream& operator>>(std::istream& is,       Example& example)
{
	is >> example.str;
	return is;
}

int Example::operator[](std::size_t index)
{
	return p[index];
}	






































