/*
 * Class Description
 * What are it's tasks etc...
 *
 * Some copyright notice
 */

#ifndef EXAMPE_H_ // if you have multiple directories - you can prepend path to the guard name
#define EXAMPE_H_
#include <string>

class Example {
public:

	// CONSTRUCTION / DESTRUCTION

    Example();                        	 // default 		constructor
    Example(double x, double y, int id); // specialized     constructor
	Example(const std::string str);      // specialized     constructor
    Example(const Example&);             // copy			constructor
    Example& operator=(const Example&);  // copy-assignment operator
    Example(Example&&);                  // move            constructor (C++11)
    Example& operator=(Example&&);       // move-assignment operator    (C++11)
	~Example();                       	 // destructor
	
	// SETTERS AND GETTERS
	
	// these functions can be labeled as const, becase they do not modify anything in the class
    double GetX() const;
    double GetY() const;
	
	std::size_t GetAllocationSize() const;
	void FreeAllocation();
	void NewAllocation(std::size_t size);
	
	// static function - identical behavior regardless of object
	static std::string GetDefaultString();

	// OPERATOR OVERLOADING
	
    // - note that these functions compare 2 objects but they take only 1 as a parameter
	// - they need to take only 1 object because these functions have already access to the current object
    // because comparison ... is just comparison it should not modify the object so all comparison functions are labeled const
	bool operator== (const Example&) const;
	bool operator!= (const Example&) const;
	bool operator<  (const Example&) const;
	bool operator<= (const Example&) const;
	bool operator>  (const Example&) const;
	bool operator>= (const Example&) const;

	// - if these functions were void (and were not returning the stream) you would not be able to chain operator like this: std::cout << a << b << c << ...
	// - these functions need to be friends of stream because you need to let it access your fields 
	friend std::ostream& operator<<(std::ostream& os, const Example& example);
	friend std::istream& operator>>(std::istream& is,       Example& example); // input will modify

	// a very often overloaded operator - especially for containers
	int operator[](std::size_t index);

	//more examples
	//http://en.cppreference.com/w/cpp/language/operators
	
	//templates need to be fully written in header files - they can't be compiled
	template<class T>
	void SetBoth(T new_x, T new_y)
	{
		x = static_cast<double>(new_x);
		y = static_cast<double>(new_y);
	}

private:
	double x, y;
	
	std::size_t aloc;
	int* p;
	
	std::string str;
	
	const int id;
};


#endif // EXAMPLE_H_


















