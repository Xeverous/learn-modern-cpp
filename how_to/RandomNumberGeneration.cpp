/*
 * C++11 wprowadza ca�� bibliotek� zwi�zan� z
 * generacj� liczb pseudolosowych
 *
 * Od tej pory rand() nie ju� uznawany za C++
 * rand() to C!
 */
#include <iostream>
#include <random>
#include <ctime>

int main()
{
	//m�j ulubiony silnik do generacji liczb losowych
	// Mersenne Twister
	// https://en.wikipedia.org/wiki/Mersenne_Twister
	// - Sk�ada si� z 4 tablic po 624 zmienne
	// - Tablice s� inicjowane liczbami pierwszymi
	// - Jak na RNG, ma ca�kiem d�ugi okres, zanim wyniki
	//		si� zap�tl� i zaczn� si� powtarza�
	//		okres ten wynosi: 2^19937 - 1
	// - przechodzi wiele test�w statystycznych
	// Uwaga:
	// MT nie jest bezpieczny kryptograficznie
	// (nie nadaje si� do generacji kluczy, poniewa�
	// na podstawie ilu� kolejnych wynik�w mo�na okre�li� kolejny)

	// Ale nie musisz o tym wiedzie� - w ko�cu po
	// to jest Hermetyzacja i zmienne prywatne

	//stw�rzmy jaki� silnik
	// Mersenne Twister dosta� fajn� nazw� w C++
	// ... prawie MLG
	// do constructora trzeba poda� jak�� warto�� startow�
	// (tak zwany seed - ziarno); oczywi�cie wrzucamy
	// dat� bezwzgl�dn� - nigdy si� nie powt�rzy
	std::mt19937 rng(time(nullptr));
	//std::default_random_engine rng(time(nullptr)); // deafult engine to r�wnie� dobry wyb�r

	//stworzmy jak�� dystrybucje (rodzaj rozk�adu)
	// do constructora dystrybucji podajesz zasi�g
	// przedzia� jest domkni�ty z obu stron
	std::uniform_int_distribution<unsigned int>     dist_i(0, 100); //dla typ�w ca�kowitych
	std::uniform_real_distribution<double> dist_d(0.0, 100.0); //dla typ�w u�amkowych

	//wygenerujmy liczby ca�kowite
	for (int n=0; n<10; n++)
		std::cout << dist_i(rng) << ' ';
	std::cout << '\n';

	//wygenerujmy liczby u�amkowe
	for (int n=0; n<10; n++)
		std::cout << dist_d(rng) << ' ';
	std::cout << '\n';

	//Je�li dziwi ci�, �e po prostu pisze si� dist_i(rng)
	// bez nazwy �adnej funckji to dlatego, bo dystrubucja to struct
	// kr�ry nie ma funkcji tylko operator()
	// operator() pobiera silnik przez referencj�
}


// Wi�cej przyk�ad�w
// http://en.cppreference.com/w/cpp/numeric/random

/*
 * Opr�cz tego wszystkiego (a zwr�� uwag�, �e generacja liczb losowych
 * w C++11 to tylko 3 linie - silnik, dystrybucja, generacja)
 *
 * to C++11 ma mas� innych dystrybucji
 * (np dystrybujce, gdzie znaczna cz�� wynik�w jest po �rodku
 *  - bardzo pasuje do imitacji np gier z kostami wielo�ciennymi)
 *
 * MI�DZY INNYMI:
 * bernoulli_distribution
 * binomial_distribution
 * negative_binomial_distribution
 * geometric_distribution
 * poisson_distribution
 * exponential_distribution
 * gamma_distribution
 * weibull_distribution
 * extreme_value_distribution
 * normal_distribution
 * lognormal_distribution
 * chi_squared_distribution
 * cauchy_distribution
 * fisher_f_distribution
 * student_t_distribution
 * discrete_distribution
 * piecewise_constant_distribution
 * piecewise_linear_distribution
 */