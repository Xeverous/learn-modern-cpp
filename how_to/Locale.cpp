#include <cstdio>
#include <ctime>
#include <clocale>
#include <iomanip> // in/output manipulation: std::put_time()
#include <iostream>

/*
 * Funkcja
 * char* std::setlocale(int category, const char* locale)
 *
 * argument int category
 * 		co dok�adnie (w sensie outputu) ma by� modyfikowane
 * 		te makra roziwjaj� si� do poprawnego inta:
 * 		LC_ALL
 * 		LC_COLLATE
 * 		LC_CTYPE
 * 		LC_MONETARY
 * 		LC_NUMERIC
 * 		LC_TIME
 * 		oczywi�cie radz� u�ywa� LC_ALL
 *
 * argument const char* locale
 * 		tu nale�y poda� dok�adn� nazw� danego kodowania
 * 		przyk�adowe znalezione na en.cppreference:
 * 		"ja_JP.UTF-8"
 * 		"en_US.UTF-8"
 *
 * 		s� trzy gwarantowane, zawsze dzia�aj�ce:
 * 		""  - wywo�a domy�lny na danym systemie (Note: to jest pusty ci�g (zawiera tylko '\0', ale ma adress), to nie jest nullptr)
 * 		"C" - wywo�a bardzo podstawowy, obs�uguj�cy tylko ASCII (ten jest na starcie ka�dego programu)
 * 		nullptr - funkcja nie zmieni czegokolwiek, zwr�ci obecnie ustawiony
 *
 * return char*
 * pointer do ci�gu znak�w zawieraj�cy (nowo) ustawiony locale
 * je�i zmiana si� nie powiedzie funckja zwr�ci nullptr
 *
 * Notes:
 * During program startup, the equivalent of std::setlocale(LC_ALL, "C"); is executed before any user code is run.
 * Although the return type is char*, modifying the pointed-to characters is undefined behavior.
 */

void output_locale_info()
{
	//pobierz obecny locale i go wypisz
	std::cout << "Locale is: " << std::setlocale(LC_ALL, nullptr) << "\n";

	//pobierz czas bezwzgl�dny (POSIX Time - since 01.01.1970)
	std::time_t t = std::time(nullptr);

	//wypisz czas (std::put_time() to funkcja formatuj�ca output dla czasu)
	//jak formatowa�: http://en.cppreference.com/w/cpp/io/manip/put_time
	std::cout << "UTC:   " << std::put_time(std::gmtime(&t), "%c %Z") << '\n';
	std::cout << "local: " << std::put_time(std::localtime(&t), "%c %Z") << '\n';

	//pobierz informacje o formatowaniu waluty
	std::lconv* lc = std::localeconv();
	std::cout << "currency symbol: " << lc->currency_symbol << " (" << lc->int_curr_symbol << ")\n";
}

int main()
{
	//wykonaj funckj� na obecnym locale
	output_locale_info();

	//ustaw locale na domy�lny dla systemu operacyjnego
	std::setlocale(LC_ALL, "");
	std::cout<<"\n";

	//wykonaj funckj� na obecnym locale
	output_locale_info();

	std::cin.get();
	return 0;
}











