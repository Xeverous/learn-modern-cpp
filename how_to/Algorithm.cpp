
#include <algorithm>
#include <string>
#include <iostream>
#include <vector>
#include <utility>
//zawiera ... algorytmy

//cz�� mo�e by� bardzo prosta, np:

//std::sort
	// jak nazwa wskazuje ... sortuje
	// je�li nie podasz 3-go argumentu (to bool funckja_poruwnuj�ca())
	// to automatycznie u�yje std::greater< _tw�j_typ_ >
	// lub operatora < dla twojej klasy

	// przyk�ad u�ycia z w�asn� funckj� por�wnzwcz�
	// idelnie nadaje si� do sortowania obiekt�w klasy user-defined

struct MyStruct
{
	int key;
	std::string stringValue;

	MyStruct(int k, const std::string& s) : key(k), stringValue(s) {}
};

struct less_than_key
{
	inline bool operator() (const MyStruct& struct1, const MyStruct& struct2)
	{
		return (struct1.key < struct2.key);
	}
};

int main()
{
	std::vector < MyStruct > vec;

	vec.push_back(MyStruct(4, "test"));
	vec.push_back(MyStruct(3, "a"));
	vec.push_back(MyStruct(2, "is"));
	vec.push_back(MyStruct(1, "this"));

	std::sort(vec.begin(), vec.end(), less_than_key());

	// inne przyk�ady:
	// http://en.cppreference.com/w/cpp/algorithm/sort




	// std::search
	// dzia�anie jak std::sort, tylko szuka tego, co podasz
	// wg kryteri�w, kt�re podasz

	// przyk�ady
	// http://en.cppreference.com/w/cpp/algorithm/search






	std::string str("Hello world\n");
	std::transform(str.begin(), str.end(), str.begin(), ::toupper); //toupper nie jest w std

	//transform przechodzi p�tl� po containerze (std::string to tak�e container char�w)
	// argument 1 - od pocz�tku stringa
	// argument 2 - do ko�ca stringa
	// argument 3 - tu ma zacz�� zapisywanie rezultat�w
	// argument 4 - funckja uruchomiona na ka�dym elemencie (tu na ka�dym znaku)


	std::cout << str; //wypisze capslockiem







	/*
	 * Zamiana dw�ch warto�ci
	 * dobra, temat jest tak prosty, �e nie ma o czym pisa�
	 * - swap pobiera 2 zmienne (przez referencje (&) lub rvalue ref (&&)), jedynie wymaga by typy zmiennych by�y takie same
	 * - dla obiekt�w wymaga, aby posiada�y copy-constructor lub move-constructor
	 * #include <utiity> i/lub #include <algorithm>
	 */

	int x = 10, y = 5;
	std::swap(x, y);

}

