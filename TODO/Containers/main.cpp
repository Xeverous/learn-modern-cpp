/*
 * Zatem czym s� containery?
 * To klasy stworzone specjalnie pod trzymanie dowolnej liczby
 * zmiennych dowolnego typu (ka�dy container jest na szablonach)
 * (typ to mo�e tak�e by� klasa stworzona przez ciebe)
 *
 * cechy container�w:
 * 	- odpowiednio dobrany container dzia�a CO NAJMNIEJ tak dobrze, jak zwyk�a tablica
 * 	- containery (opr�cz std::array) mog� przyjmowa� dowolny rozmiar w runtime
 * 	- containery b�d� trzyma� obiekty zadeklarowanego typu lub typy pochodne tego typu
 * 		std::vector< Czworokat > vect;
 * 	- containery same za ciebie b�d� rezerwowa� i zwalnia� pami��
 * 	- containery zapewniaj� mas� u�ytecznych funkcji, np:
 * 		- funkcje: .begin() .cbegin() .end() .cend() .rbegin() .crbegin() .rend() .crend() do obs�ugi p�tli
 * 			p�tli prostej (begin/end), const prostej (cbegin/cend), p�tli od ty�u (reverse begin/end) i const od ty�u (cr begin/end)
 * 		- overload operatora [] aby mie� wygodny, bezpo�redni dost�p do zmiennych w �rodku
 * 		- funckje .size() .clear() .empty()
 * 		- funkcje .swap() .insert() .erase()
 * 		- funkcje operator�w < > <= >= == != =
 * 		- i wiele innych
 * 	- ka�dy container ma inn� budow� wewn�trzn�
 * 	- ka�dy container ma sw�j w�asny #include
 * 	- wiele container�w ma kilka constructor�w (tzn mo�na je stworzy� na r�ne sposoby: z elementami, puste, kopia innego containera etc)
 *  - standard C++ gwarantuje bezpiecze�stwo wykonywania r�nych operacji na containerach jednocze�nie przez wiele w�tk�w
 *
 * 	std::array jest wyj�tkowym containerem, bo jako jedyny nie alokuje
 * 	pami�ci w spos�b dynamiczny - u�ywaj std::array do obs�ugi tablic,
 * 	kt�rych wielko�� jest sta�a i znana w czasie kompilacji
 *
 * 	std::array wymaga te� drugiego parametru szablonu - warto�� ca�kowita znana w czasie kompilacji
 *
 * 	UWAGA - ten plik mo�esz ca�y skopiowa�, wklei� i bezpo�rednio skompilowa�
 *
 * 	wi�cej informacji o containerach i por�wnanie ich dzia�ania:
 * 	http://en.cppreference.com/w/cpp/container
 */

#include <vector>
#include <array>
#include <map>
#include <forward_list>
#include <list>
#include <iterator>
#include <iostream>
#include <algorithm>	//std::sort
#include <string>

//do wygodnego wypisania calej listy przez cout
std::ostream& operator<<(std::ostream& ostr, const std::list<int>& list)
{
    for (auto &i : list) {
        ostr << " " << i;
    }
    return ostr;
}

int main()
{
	/*
	 * Sequence containers (tablice ci�g�e/sekwencyjne)
	 * Containery stworzone pod szybki odczyt/zapis zmiennych
	 * maj�ce �redni/najni�szy czas wstawiania
	 * do Sequence container�w nale��:
	 * array, vector, deque, forward_list, list
	 */

	//std::array (static contiguous array) tablica o sta�ej szeroko�ci (znanej compile-time)
	// tworzy blok o stalej szeroko�ci w pami�ci, warto�ci mo�na edytowa�,
	// ale nie mo�na warto�ci usuwa� lub wstawia� nowych
	{
		std::array<int, 3> a1( {5, 2, 3} );	// initialize with brace-enclosed-list
		//std::array<int, 3> a1{5, 2, 3}; 	// (C++14) (does not require parenthesis)
		std::array<int, 3> a2 = {6, 7, 2};  // assignment operator
		std::array<int, 3> a3(a1); 			// initialize as copy of a1 (copy constructor)

		//let's sort
		std::sort(a1.begin(), a1.end());

		//example edit - use std::array::operator[]
		a2[0] = 100;

		for(const auto& i : a3)
			std::cout << i << ' ';

		std::cout<<"\n";

		for(const auto& i : a2)
			std::cout << i << ' ';
	}

	std::cout<<"\n";





	//std::vector<> (dynamic contiguous array)
	// zachowuje si� jak zwyk�a tablica, ale jego rozmiar mo�e zmienia� si� w runtime
	// vector jest tak zbudowany, �e jego elementy ZAWSZE s� trzymane w pami�ci jako blok ci�gy
	// vector sam alokuje potrzebn� pami��, zostawiaj�c cz�� nadmiarowej pami�ci z prawej strony
	// - wrzucenie nowego elementu na koniec jest operacj� o sta�ym czasie wykonania ( O(1) )
	//		chyba �e nie ma ju� wolnej pami�ci - wtedy przepisanie element�w do wi�kszego bloku ( O(n) )
	// - odczyt kolejnych element�w jest najszybszym z mo�liwych (blok ci�g�y w pami�ci)
	// - operacje wstawiania do �rodka bed� powodowa� przesuwanie element�w w prawo ( O(n) ), aby zachowa� ci�g�o��
	//std::deque<> (double-ended queue)
	// zachowanie identyczne co do vectora, tylko:
	// - nadmiarowa pami�� jest trzymana i z lewej i z prawej
	// - opr�cz wstawiania na koniec wstawianie na pocz�tek te� jest natychmiastowe
	// - deque dodatkowo ma funckje .push_front() i .pop_front() (vector ma tylko .push_back() i .pop_back())
	// - wstawianie w �rodek przesunie cz�� element�w w lewo lub w prawo w pami�ci (zale�y, w kt�r� stron� mniej przepisywania)

	//z podowu wy�ej wymienionych cech g��wnym preznaczeniem vectora i deque jest cz�sty odczyt przy rzadkim
	//wstawianiu danych lub wstawianiem nowych danych tylko na koniec i/lub pocz�tek
	{
		std::vector<unsigned int> v = {7, 5, 16, 8};
		std::cout << "Size of vector: " << v.size() << "\n";

		// Add two more integers to vector (at the end)
		v.push_back(25);
		v.push_back(13);
		// insert something at the beginning
		v.insert(v.begin(), 39);
		// insert something in the middle
		v.insert(v.begin() + v.size() / 2, 31);
		//copy one value from another (using std::vector::operator[])
		v[4] = v[6];

		std::cout << "Size of vector: " << v.size() << "\n";

		// Iterate and print values of vector
		for(auto n : v)
			std::cout << n << ' ';
	}
	std::cout<<"\n";


	//std::forward_list<> (singly-linked list)
	// - opr�cz element�w container ten trzyma tak�e pointery (container zajmuje wi�cej pami�ci ni� same elementy)
	//		pointer przy ka�dym elemencie wskazuje na kolejn� par� (element, pointer)
	// - container ten cechuje si� wolniejszym odczytem ( O(n) ) od vectora/deque
	//		(odczyt to podawanie dalej jad�c po pointerach)
	//		ale szybszym wstawianiem (zawsze O(1))
	//		(wstawianie to tylko edycja 2 pointer�w, kt�re s� z lewej i prawej strony nowego elementu)
	// - w przypadku zu�ycia ca�ego nadmiaru pami�ci nie trzeba przepisywa� ca�ego containera do nowego bloku ci�g�ego
	//		(wystarczy zaalokowa� nowe miejsce i ustawi� na to pointer z ostatniego elementu)
	// - pary (element, pointer) tworz� w pami�ci okr�g, po kt�rym mo�na porusza� si� w jedn� stron�
	//std::list<> (doubly-linked list)
	// - zachowanie takie samo co forward_list, tylko przy ka�dym elemencie trzymane s� 2 pointery
	//		(umo�liwia to wykonywanie p�tli od ty�u)
	// - tr�jki (element, pointer, pointer) tworz� w pami�ci okr�g, po kt�rym mo�na porusza� si� w obie strony
	// - w por�wnianiu do listy pojedeynczej zajmuje wi�cej pami�ci, ale umo�liwia wi�cej operacji

	// �adna z list nie wspiera operatora[] poniewa� nie ma bezpo�redniego dost�pu to dowolnego elementu
	//z podowu wy�ej wymienionych cech g��wnym preznaczeniem list jest cz�ste wstawianie
	//nowych element�w przy rzadkim odczycie
	{
		std::forward_list<std::string> words1{"the", "frogurt", "is", "also", "cursed"}; //create list
		std::forward_list<std::string> words2(words1.begin(), words1.end()); //make a copy of words1

		std::list<int> list = { 8,7,5,9,0,1,3,2,6,4 };
		std::cout << "before:     " << list << "\n";
		list.sort();
		std::cout << "ascending:  " << list << "\n";
		list.reverse();
		std::cout << "descending: " << list << "\n";
	}
	std::cout<<"\n";

	/*
	 * Associative containers (tablice asocjacyjne)
	 * Te containery utrzymuj� wszystkie elementy posortowane
	 * dzi�ku temu te containery maj� najszybszy dost�p w czasie szukania
	 * czas szukania: O(log(n))
	 *
	 * dowolna operacja edycji/wstawiania powoduje posortowanie containera
	 *
	 * do tych container�w nale�� (multi)mapy/sety
	 */

	//G��wnym zastosowaniem tych container�w jest bardzo cz�ste szukanie i edycja zmiennych
	// - sety obs�uguj� posortowane warto�ci
	// - mapy obs�uguj� posortowane pary (klucz, warto��) (sortowanie wg kluczy) (warto�ci mog� si� powtarza�)
	// - multi pozwalaj� na duplikaty kluczy
	// - nie-multi posiadaj� overload operator[], operator ten znajdzie (je�li istnieje, inaczej stworzy nowy element) i dokona edycji
	//std::set<key> 			//collection of keys, 	 		 sorted by keys, keys are unique
	//std::map<key, value> 		//collection of key-value pairs, sorted by keys, keys are unique
	//std::multiset<key> 		//collection of keys, 			 sorted by keys
	//std::multimap<key, value> //collection of key-value pairs, sorted by keys
	{
		std::map<char, int> letter_counts {{'a', 27}, {'b', 3}, {'c', 1}};

		std::cout << "initially:\n";
		for (const auto &pair : letter_counts) {
			std::cout << pair.first << ": " << pair.second << '\n';
		}

		letter_counts['b'] = 42;  // update an existing value
		letter_counts['x'] = 9;  // insert a new value

		std::cout << "after modifications:\n";
		for (const auto &pair : letter_counts) {
			std::cout << pair.first << ": " << pair.second << '\n';
		}

		// count the number of occurrences of each word
		// (the first call to operator[] initialized the counter with zero)
		std::map<std::string, std::size_t>  word_map;
		for (const auto &w : { "this", "sentence", "is", "not", "a", "sentence",
							   "this", "sentence", "is", "a", "hoax"}) {
			++word_map[w]; //add new element with value w
		}

		for (const auto &pair : word_map) {
			std::cout << pair.second << " occurrences of word '" << pair.first << "'\n";
		}
	}

	/*
	 * Unordered associative containers (tablice haszuj�ce)
	 * bardzo podobne do Associative container�w, tylko zamiast sortowa� elementy
	 * korzystaj� z funkcji haszuj�cych (mo�na te� poda� w�asn�) do umieszczania i odnajdywania element�w
	 * W rzeczywisto�ci cz�sto okazuj� si� by� szybsze od Associative container�w
	 *
	 * czas szukania: najlepszy przypadek: O(1), najgorszy: O(n)
	 * w praktyce najgorszy przypadek nie wyst�puje nigdy (aby do niego doprowadzi�
	 * trzeba by by�o mie� wszystkie warto�ci identyczne na jednym, wiecznie duplikowanym kluczu)
	 *
	 * Tablice haszuj�ce (w szczeg�lno�ci mapy i multimapy) s� stosowane w bazach danych
	 *
	 * czym s� funkcje haszuj�ce:
	 * https://pl.wikipedia.org/wiki/Funkcja_haszuj�ca
	 * najcz�ciej wykorzystywane funkje haszuj�ce:
	 * https://pl.wikipedia.org/wiki/SHA-2
	 */

	//std::unordered_set<key>				//collection of keys,			 hashed by keys, keys are unqiue
	//std::unordered_map<key, value>		//collection of key-value pairs, hashed by keys, keys are unique
	//std::unordered_multiset<key>			//collection of keys,			 hashed by keys
	//std::unordered_multimap<key, value>	//collection of key-value pairs, hashed by keys
	{
		//obs�uga jest identyczna jak dla associative container�w
		//jedyn� r�nic� widoczn� zewn�trz jest dob�r funkcji haszuj�cej
	}

	return 0;
}






















