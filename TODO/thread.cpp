#include <thread>
#include <vector>
#include <string>
#include <iostream>

void some_different_function(float fl, double d)
{
	std::cout << fl << " * " << d << " = " << fl * d << "\n";
}

void another_function(unsigned amount, const std::string& str)
{
	for (unsigned i = 0; i < amount; i++)
		std::cout << str << "\n";
}

struct why_not_this_pseudofunction
{
	void operator()(int something)
	{
		std::cout << "something = " << something << "\n";
	}
};

template <class Function, class... Args>
void execute_in_threads(unsigned count, Function&& function, Args&&... args)
{
	std::cout << "starting execution\n";

	std::vector<std::thread> vec;
	vec.reserve(count); // reserve memory, but do not create any objects

	// initialize threads with specific function, passing all arguments (thread execution begins instantly)
	for (unsigned i = 0; i < count; i++)
		vec.emplace_back(function, args...);

	for (auto& thread : vec) // for each thread in vector...
		thread.join(); // ...wait untill it finishes (joins parent thread)

	std::cout << "execution finished\n\n\n";
}

int main()
{
	int count = 10;
	double arg3 = 3.14;
	float arg4 = 0.5;

	// Function does not have to be an actual function, it can be anything that has callable operator()
	// C++17 type trait (std::is_invocable<>) can be used to detect (in)valid types
	execute_in_threads(count, some_different_function, arg4, arg3);
	execute_in_threads(count, another_function, arg3, "Foo Bar");
	execute_in_threads(count, why_not_this_pseudofunction(), 5);
	execute_in_threads(count, [](const std::string& str) -> void { std::cout << str << "\n"; }, "a lambda");
}

/*
 * Example usage of std::thread to execute same function simultaneously multiple times
 *
 * Example output for first functon:
 * starting execution
 * 0.5 * 0.5 * 3.14 = 3.14 = 1.57
 * 0.5 * 3.14 = 1.57
 * 1.57
 * 0.50.5 *  * 3.14 = 1.57
 * 0.5 * 3.14 = 1.57
 * 0.50.50.5 * 3.14 =  * 1.57
 * 3.14 = 3.141.57
 *  *  = 1.57
 * 0.5 * 3.14 = 3.14 = 1.57
 * 1.57
 * execution finished
 */
