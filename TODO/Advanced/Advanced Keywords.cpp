/*
 * This file will explain purpose of some rarer C++ keywords
 */

		
		
//alternative operator names
&&	and
&=	and_eq
&	bitand
|	bitor
~	compl
!	not
!=	not_eq
||	or
|=	or_eq
^	xor
^=	xor_eq
		
		
		
		


alignas //(since C++11)
alignof //(since C++11)
//specifies aligment of the given type to occupy certain amount of memory
//rarely used because modern compilers can easily manage and optimize memory layout








asm //can be used to inplace assembler code

#include <iostream>
extern "C" int func();
// the definition of func is written in assembly language
asm(".globl func\n\t"
    ".type func, @function\n\t"
    "func:\n\t"
    ".cfi_startproc\n\t"
    "movl $7, %eax\n\t"
    "ret\n\t"
    ".cfi_endproc");
 
int main()
{
    int n = func();
    // extended inline assembly
    asm ("leal (%0,%0,4),%0"
         : "=r" (n)
         : "0" (n));
    std::cout << "7*5 = " << n << std::endl; // flush is intentional
 
    // standard inline assembly
    asm ("movq $60, %rax\n\t" // the exit syscall number on Linux
         "movq $2,  %rdi\n\t" // this program returns 2
         "syscall");
}








auto //automatic type deduction
//can be used to shorten/automatize type code (especially when function returns a long, nested type)
//is neccessary is some templates

int a = 10;
int b = 20;
auto c = a+b; // c will be int

std::vector<int> container; // ...
for (auto it = container.begin(); it != it.end(); it++) {
    // type of it is std::vector<int>::iterator, but you don't need to state that
}











catch, throw //used to catch, throw, rethrow exceptions
//an uncatched exception will be output to OS, effectively terminating the program (crash)

try
{
	std::cout << "Creating a vector of size 5... \n";
	std::vector<int> v(5);
	std::cout << "Accessing the 11th element of the vector...\n";
	std::cout << v.at(10); // will throw std::out_of_range
}
catch (const std::exception& e)
{
    std::cout << " a standard exception was caught, with message '"
    		  << e.what() << "'\n";
}










concept //Experimental C++17 feature
//allows templates to specify concepts (eg EqualityComparable, CopyConstructible)









constexpr //forces the compiler to calculate the expression during compilation
//when used on a function, it also implies inline
//when used on a variable, it also implies const

constexpr factorial (int n)
{
    return n > 0 ? n * factorial( n - 1 ) : 1;
}

//since factorial function is constexpr, it can now be used where constant expression are required
int array[ factorial(5) ]; //will created array of size 5! (120)

// more info and examples
// http://en.cppreference.com/w/cpp/language/constexpr









decltype //declares the same type as another variable/object

int x;
decltype(x) y; //y is the same type as x

decltype(auto) //ultimate template boost









explicit
//specifies that a constructor or [conversion function (since C++11)] doesn't allow implicit conversions or copy-initialization.
//It may only appear within the decl-specifier-seq of the declaration of such a function within its class definition.

//this way you can prevent implicit (hidden) convertions, eg when a random float enters your double function
explicit OnlyDouble(double d) //float promotion will not happen
{
	//...
}





export
//pre C++11
//Used to mark a template definition exported, which allows the same template to be declared,
//but not defined, in other translation units.

//post C++11
//The keyword is unused and reserved for future






extern
//1.
//static storage duration with external linkage specifier
//eg creating a global variable across multiple files requires it
//to be decalred as extern in all but one file

//all files except one
extern int global_int;
//that one file
int global_int;

//2. extern old C code
extern "C" {
    int open(const char *pathname, int flags); // C function declaration
}

//3.
//A class template defines a family of classes.
//someway it removes export use for templates









inline
//suggests the compiler to inplace the function code directly where the call is
//effectively removing jump in and jump out of the function code during runtime
//inlined functions will create longer files, but the code can run faster due to no jumps
//inlined templates must be placed in header files

//because modern compilers are clever when to optimize and use it, they can both
// - ignore inline keyword and not inline a function at all
// - inline a function that the user didnt specified to be inlined







import, module //experimental C++17 features









mutable, volatile, const
//1.
//specifies that a variable can be changed outside of the program
//used when writing a very close-hardware related code

//most common example
const volatile x;
//volatile tells the compiler that the variable's value can be changed outside of the program
//const prevents the user from accidentally chaning such hardware variable (chaning it would crash or create hardware problems)
//const volatile combo tells the compiler NOT TO optimize it by skipping or replacing such varable in the code by somethig other (possibly faster)

mutable
//2 (only mutable keyword)
//specifies that a lambda function can modify its arguments
//(lambdas can not do this by default)

int a = 1, b = 1, c = 1;
auto m1 = [a, &b, &c]() mutable {
	auto m2 = [a, b, &c]() mutable {
		std::cout << a << b << c << '\n';
		a = 4; b = 4; c = 4;
	};
	a = 3; b = 3; c = 3;
	m2();
};

a = 2; b = 2; c = 2;
m1();                             // calls m2() and prints 123
std::cout << a << b << c << '\n'; // prints 234











namespace
//creates an inner scope to re-use variable names
//allows to decrease pollution of names and differentiate similar but other type
//every library packs their's types into it's namespace to avoid confusion
//(most notable is string and containers, many create very simial classes so they hide them there)

namespace foo {
    namespace bar {
         namespace baz {
             int qux = 42;
         }
    }
}
 
namespace fbz = foo::bar::baz;
 
int main()
{
	{
		std::cout << fbz::qux << '\n';
	}
	//even shorter
	{
		using namespace std;
		using namespace fbz;
		cout << qux << "\n";
	}
}









noexcept()
//1.
//returns true if the expression does not throw any exceptions

//The result is false if the expression contains at least one of the following potentially evaluated constructs:
//- call to any type of function that does not have non-throwing exception specification, unless it is a constant expression.
//- throw expression.
//- dynamic_cast expression when the target type is a reference type, and conversion needs a run time check
//- typeid expression when argument type is polymorphic class type

//In all other cases the result is true.

//Note:
//The noexcept operator does not evaluate expression.

noexcept
//2. specifies that a function can not throw any exceptions
void f() noexcept; // the function f() does not throw










nullptr
//denotes a null pointer constant
//its type is std::nullptr_t
//allows to create templates only for nullpointers and to differentiate it from ambiguous NULL

void func(int x);
void func(void* p);

func(5);
func(NULL); //same as func(0); - very dangerous!
func(nullptr); //guaranteeed to call void* overload








register
//untill C++11
//auto had automatic storage duration (now auto is automatic type deduction)
//untill C++17
//register - automatic storage duration. Also hints to the compiler to place the object in the processor's register.
//post C++17
//The keyword is unused and reserved.



requires
//might arrive in C++17 with concepts
//in a function or function template declaration, specifies an associated constraint (concepts TS)
//specifies an constant expression on template parameters that evaluates a requirement (concepts TS)







sizeof()
//returns a constant of type std::size_t

//Depending on the computer architecture, a byte may consist of 8 or more bits, the exact number being recorded in CHAR_BIT.
//sizeof(char), sizeof(signed char), and sizeof(unsigned char) always return 1.
//sizeof cannot be used with function types, incomplete types, or bit-field glvalues.
//When applied to a reference type, the result is the size of the referenced type.
//When applied to a class type, the result is the size of an object of that class plus any additional padding required to place such object in an array.
//When applied to an empty class type, always returns 1.





sizeof...()
//returns amount of parameters in a parameter pack (useful in templates)

#include <iostream>
#include <array>
#include <type_traits>
template<typename... Ts>
constexpr auto make_array(Ts&&... ts)
    -> std::array<std::common_type_t<Ts...>,sizeof...(ts)>
{
    return { std::forward<Ts>(ts)... };
}
 
int main()
{
    auto b = make_array(1, 2, 3);
    std::cout << b.size() << '\n';
    for (auto i : b)
        std::cout << i << ' ';
}
//output:
//3
//1 2 3





static
//1. used in a free funcion
//the variable is initialized, but never destroyed
//it will stay alive after the function returns but can not be called outside the function
//memory is released when the preogram finishes

//2. used in class member/function declaration
//creates a variable that persists regardless of any instance of an object of its type
//it's simply inside the object namespace, but its the same for all objects of this type
//the value can eb accessed even when there exists no objects of its type

//Static member functions are not associated with any object. When called, they have no this pointer.
//Static member functions cannot be virtual, const, or volatile.
//The address of a static member function may be stored in a regular pointer to function, but not in a pointer to member function.

class X { static int n; };
int X::n = 1; //value is (and will be) the same for all objects, despite no existing objects of type X






synchronized //experimantal C++17
//Transactional memory is a concurrency synchronization mechanism that combines groups of statements in transactions, that are
//- atomic (either all statements occur, or nothing occurs)
//- isolated (statements in a transaction may not observe half-written writes made by another transaction, even if they execute in parallel)






template
//1. Declaration of a template
//2. Inside a template definition, template can be used to declare that a dependent name is a template.





this
//The keyword this is a prvalue expression whose value is the address of the object, on which the member function is being called
//Useful when you need inside an object to know its memory address
//if an object is of type T, this is of type T*

//It is possible to execute
delete this;
//if the program can guarantee that the object was allocated by new, however,
//this renders every pointer to the deallocated object invalid, including the this pointer itself:
//after delete this; returns, such member function cannot refer to a member of a class
//(since this involves an implicit dereference of this) and no other member function may be called.




thread_local
//specifies that a variable has local storage in the given thread







typedef
// creates an alias that can be used anywhere in place of a (possibly complex) type name.

// it also allows template-like declaration depending on macros
#if _WIN32
	typedef int64 long long;
	constexpr SIZE = sizeof(void*) //4
#elif _WIN64
	typedef int64 long;
	constexpr SIZE = sizeof(void*) //8
#endif

//The int example is wrong but it shows how it can be used to create simple types
//which are guaranteed to be long enough
//Note: many libraries introduce such typedefs, espcially: int32, int64, uint32, uint64, uchar8, uchar16, uchar32
		
		
		
		
		
		
		
typeid
//Used where the dynamic type of a polymorphic object must be known and for static type identification
//This is how its usually doen at runtime - polymorphic types check their's type id and use it through vtable
//This behaviour is inherently done by the compiler for any polymorphic type

//In all cases, cv-qualifiers (const/volatile) are ignored by typeid (typeid(T) == typeid(const T))
//typeid cannot be applied to an incomplete type.






typename
//In a template declaration, typename can be used as an alternative to class to declare type template parameters
//[and template template parameters (since C++17)] - previously only possible for class keyword.
//Inside a declaration or a definition of a template, typename can be used to declare that a dependent name is a type.





union
//A union is a special class type that can hold only one of its non-static data members at a time.
//The class specifier for a union declaration is similar to class or struct declaration:

//The union is only as big as necessary to hold its largest data member.
//The other data members are allocated in the same bytes as part of that largest member.
//The details of that allocation are implementation-defined, and it's undefined behavior
//to read from the member of the union that wasn't most recently written.
//Many compilers implement, as a non-standard language extension, the ability to read inactive members of a union.




wchar_t //(funamental type)
//type for wide character representation
//Required to be large enough to represent any supported character code unit
//(32 bits on systems that support Unicode. A notable exception is Windows, where wchar_t is 16 bits)
//It has the same size, signedness, and alignment as one of the integral types, but is a distinct type.





