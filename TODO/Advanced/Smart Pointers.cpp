

/*
 * C++11 smart pointers
 * 
 * C++11 standarises new smart pointers
 * These pointers call appropriate ctor/dtor when needed
 * It's actually impossible to create memory leaks with smart pointers - 
 * they will always release memory when their life time has passed
 * because they are templates, they can be used to point to any type, also user-defined classes
 *
 * Basic rule:
 * when all shared pointers to a given object or unique pointer die, the pointed object is destroyed
 * 
 * pre-C++11 smart pointer:
 * std::auto_ptr is now deprecated
 * std::auto_ptr will be removed in C++17
 * 
 */

//dont forget to
#include <memory>

/*
 * IMPORTANT
 * note the difference between . and -> operators
 * .  will access own object functions (smart pointer functions inside its class)
 * -> will access member functions and variables pointed by the pointer
 */
//Example
std::shared_ptr<std::string> ps = std::make_shared<std::string>("Hello");
ps.  //will display std::shared_ptr functions (eg reset, free memory, copy pointer)
ps-> //will display std::string     functions (eg clear, c_str, substring)








/*
 * HOW TO CREATE
 * Note: you NEVER have to use delete
 */

//the 5 is the place when you can pass any arguments to the ctor
std::shared_ptr<int> p1( new int(5) );
std::shared_ptr<int> p2 = std::make_shared< int >(5); //make_shared C++11
std::unique_ptr<int> u1( new int(5) );
std::unique_ptr<int> u2 = std::make_unique< int >(5); //make_unique C++14
//Note: weak can't create and destroy objects


//unique_ptr
//has deleted copy ctor and assigment operator (=)
//this will prevent any code from copying the pointer's value
//and using the same object at two different places (thats why its unique)
//unique_ptr will call pointed object dtor when it dies
{
	std::unique_ptr<int> p1( new int(5) );
}//int is deleted here


//shared_ptr
//used to share the same object across multiple places
//this way you can have multiple pointers pointing to the same object
//shared_ptr imposes a runtime use-count-check - the object will be destroyed only when all shared pointers to that object have died
{
	std::shared_ptr<int> p2(new int (8) );
	std::cout << p2.use_count(); //will print 1
	
	{
		std::shared_ptr<int> p3 = p2;
		std::cout << p2.use_count(); //will print 2
		std::cout << p3.use_count(); //will print 2
	}//int is not destroyed here, there is still 1 pointer using it
    std::cout << p2.use_count(); //will print 1
}//int is destroyed here


//weak_ptr (rarely used)
//can not create/delete any objects
//it will simply hold a "weak reference" to the object
//weak_ptr content can not be accesed easily, you need to assign it to a shared_ptr before
std::weak_ptr<int> w = p1; //p1 is a shared_ptr



/*
 * Most useful smart pointer features
 */

//make_shared - can be used with inherited types to create specific object and put in later in generic container
std::shared_ptr< BaseClass > sp1 = std::make_shared< DerivedClass >(12);
std::shared_ptr< BaseClass > sp2 = std::make_shared< OtherDerivedClass1 >(13);
std::shared_ptr< BaseClass > sp3 = std::make_shared< OtherDerivedClass2 >(14);

std::vector< std::shared_ptr< BaseClass > > vect;
vect.push_back(sp1);
vect.push_back(sp2);
vect.push_back(sp3);
//now you can loop through vector and don't care about the type, all appropriate functions will be called

//make_unique - same as above, but only for unqiue_ptr
std::unique_ptr< BaseClass > sp1 = std::make_unique< DerivedClass >(12); //make_unique available in C++14

//.reset()
//forces to release owned memory
sp1.reset(); //object is destroyed
sp2.reset( new OtherDerivedClass(130)); //can also instantly create new objects

//.get()
//returns the stored pointer (C-like)
//eg std::shared_ptr<int>::get() will return int*



