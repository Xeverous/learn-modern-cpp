/*
 * CASTS
 * C++11 introduces new convertion syntax, effectively removing need
 * for old syntax and providing more usability and safety
 */

//Generalnie zalecam u�ywanie nowych cast�w, bo generuj� bezpieczniejszy kod
//wiecej przyk�ad�w: http://www.cplusplus.com/doc/tutorial/typecasting/

int i;
double d;

//old syntax (C99 (1999) standard), it's not considered C++ anymore, it's C
i = int(d);
i = (int)d;

//new syntax (C++11 standard)
i = static_cast<int>(d);

/*
 * NEW CONVERTIONS
 */

static_cast<>()
//replaces 99% of old C-like casts
//static_cast is CPU code generated during compilation
//it simply casts/promotes (promotion is lossless accuracy cast, eg float to double) the value to another type
		
dynamic_cast<>()
//Safely converts pointers and references to classes up, down, and sideways along the inheritance hierarchy
//converting some classes sideways/up may cause object slicing (losing data because you are moving to simpler type)
//may cause runtime exceptions, the cast needs to be performed during runtime because objects can be polymorphic

reinterpret_cast<>()
//Converts between types by reinterpreting the underlying bit pattern.
//Does not compile to any CPU instructions.
//It simpy instructs the compiler to treat the sequence of bits (object representation) of expression as if it had the new type

const_cast<>()
//removes or adds const/volatile keyword to the variable
//converting from and to types must be the same, they can only differ by const/volatile keyword
//trying to modify const-removed const variable will result in UB - use it only when needed to match funtion argument signature!


/*
 * Rules of thumb
 * 
 * if converting basic type to another basic type
 * static_cast
 * 
 * if converting pointer to another pointer
 * dynamic_cast
 * 
 * if adding/removing const/volatile (also for const pointers)
 * const_cast
 * 
 * if trying to do something ... well you must know what you are doing (probably some void* stuff)
 * reinterpret_cast
 */





/*
 * EXAMPLES
 */

//static
double y = 50.6/38.9;
float x = static_cast<float>(y);

//dynamic
class A{};
class B: public A{};
class C: public A{};

A a;
B b;
C c;

A* ap = dynamic_cast<A*>(&b); //  upwards - always safe if &b points to valid B object
C* cp = dynamic_cast<C*>(&a); //downwards - UB if &a does not point to C object or it's inheritants (in this case it's UB)
B* bp = dynamic_cast<B*>(&c); //sideway   - may cause loss of data

//const
void StupidFunctionThatNeedsNonConstButDoesNotModifyArgument(int val)
{
	std::cout << val;
}

int const val = 100;
StupidFunctionThatNeedsNonConstButDoesNotModifyArgument( const_cast<int>(val) );

//reinterpret
double x = 0.5;
void* xp = &x; //will hold memory address of x
std::cout << "x address: " << reinterpret_cast<int>(xp); //print address like integer










