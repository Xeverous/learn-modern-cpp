Poni�szy text raczej nie pomo�e w samym pisaniu kodu,
ale jak bedziecie czego� szuka� w necie, to bedziecie wiedzie� o co chodzi
i generanie mie� wi�kszy ogar w ca�ym kontek�cie

Obrazek przedstawia 4 etapy kompilacji.
1. Preprocesor robi resolve wszelkiech #define i #include
2. Compiler parsuje kod (tu s� typowe b��dy)
3. Assembler (tu w zasadzie wszystko jest zawsze OK) (tworzy .o/.obj)
4. Linker (do��cza ("linkuje") do programu .dll/.lib/.a i skleja .o/.obj w tak zwany target (najcz�ciej .dll/.exe))
   Tu si� pojawi� b��dy, jak co� zepsujesz w komendach do kompilacji
   (najcz�ciej jak .cpp u�ywa kodu z biblioteki ale nie podasz wszystkich .dll kt�re kod u�ywa)
   B��dy wygl�daj� strasznie, poniewa� jest to ju� kod maszynowy. Czyta� si� nie da
   helloworld.o(.eh_frame+0x11): undefined reference to `__gxx_personality_v0`
   Naszcz�scie wystarczy tylko sprawdzi� i doda� brakuj�ce komendy/�cie�ki do dlli
   
Na b��dy linkera raczej nie wpadniesz dop�ki nie b�dziesz u�ywa� jaki� dodatkowych bibliotek
   



Zatem jak pewnie wiecie g��wnymi sk�adnikami IDE (Integrated Development Enviroment) jest:
- edytor textu
- parser (to, co podpowiadia i koloruje kod)
- kompilator
- debugger (podgl�d/edycja zmiennych runtime)
- profiler (podgl�d u�ycia CPU/GPU oraz w�tk�w)

Najbardziej znane IDE:
- MS Visual Studio (C/C++, C#, .NET)
- Code::Blocks
- Qt (jeden z najwi�kszych rywali Visuala, u�ywa MinGW)
- Eclipse (Java, C/C++)
- XCode (Mac OS) (C#, C/C++)

Najbardziej znane kompilatory:
- LLMV (Microsoft''s Compiler)
- GCC (U�ywany gdziekolwiek indziej)

GCC pochodzi od GNU C Compiler. Jest wydawany przez Free Software Foundation.
Obecnie jest w stanie kompilowa�: Ada, Fortran, Java, C, C++

GCC od d�u�szego czasu istnieje te� na Windowsie, pod dwoma postaciami:

	Cygwin
	Jest to pewnego rodzaju port/wirtualizacja, programy pisane pod to odwo�uj�
	si� do �cie�ek typowo unixowych (/etc, /usr) (Architektura POSIX)
	(Portable Operating System Interface for Unix). Generalnie proces kompilacji
	(wg mnie) jest do�� skompikowany a programy trac� performance przez wirtualizacje
	
	MinGW (Minimalist GNU for Windows)
	Po prostu GCC wydany aby dzia�a� na Windowsie. Poniewa� tworzy natywne (bez wirtualizacji)
	exe o performace rywalizuje nawet z Visual Studio. Stack Overflow podawa� ju� wiele kod�w,
	gdzie MinGW wygrywa. G�ownie, poniewa� MinGW ma swoje w�asne biblioteki pod Windowsa - programy
	nie wymagaj� cholernego MS Redistributable; dzi�ki temu programy s� w zasadzie niezale�ne od tego
	co jest w systemie (Ah ile gier ma probemy ze zgodno�ci� paczek Redist i ka�� instalowa� nowe)
	Fun Fact: MinGW jest pisany w C++
	
	TDM / TDM-GCC
	Je�li spotka�e� takie skr�ty, to jest to po prostu MinGW 64-bit
	
Wad� MinGW jest to, �e jest (i chyba zawsze b�dzie) troch� w plecy w stosunku do GCC.
Obecnie najnowszy MinGW 4.9.3 (U�ywaj� anagicznych werjsi co GCC)
jest w zasadzie wolny od jakichkolwiek bug�w. Oczyw�cie pe�ne wsparcie C++11.

GCC 6.2 jest w pe�ni dzia�aj�cy na Linuxie bez potrzeby jakiegokolwiek IDE.
Pe�ne wsparcie C++14. Prawie ju� pe�ne wsparcie C++17. Wsparcie experymentalnych
feature�w planowanych na C++17 lub p�niej



	
Teraz o samym j�zyku.
Definicja prosta:   C++ - j�zyk programowania og�lnego przeznaczenia
Definicja w�a�ciwa: C++ - paradygmowy j�zyk programowania, wspieracj�cy m.in:
- concurrent programming
- constraints
- dataflow
- declarative programming
- distributed programming
- functional  programming
-         metaprogramming (mechanizm temlat�w jest Turing-Complete)
- generic     programming
- imperative  programming
- logic       programming
- reflection  programming
- object-oriented programming
- pipelines
- rule-based  programming
	
	
C++11 (cz�sto nazywany C++0x - bo mia� wyj�� przed 2010)
Wprowadiz� mas� nowych feature�w (w szczeg�lno�ci ogromn� ilo�� template'�w),
totalnie od�wie�y� j�zyk
Ilo�� feature�w jest ogromna, zerknij na artyku� na wikipedii:
https://en.wikipedia.org/wiki/C%2B%2B11

Od czas�w C++11 cz�sto mo�na spotka� 2 nazwy rdzeni j�zyka:
- std - standard library
- STL - standard template library

C++14
Nie wprowadzi� wielu super-featur�w. Jest potocznie nazywany bugfixem C++11.
Standard rozlu�nia pewne rzeczy lub poprawia pewne featury

C++17 (lub C++1z)
Nowe featury zwi�zane z keywordem auto, template'ami, lambdami
Nowy element j�zyka: concept. Koncepty s� dosy� proste (np je�li twoja klasa
ma default constructor i nie ma funkcji wirtualnych to klasa ta spe�nia concept
TriviallyConstructible)
Koncepty umo�liwi� wygodniejsze pisanie template'�w oraz uproszcz� bardzo d�ugie
b��dy kompilacji zwi�zane z niepoprawnym/b��dnym u�yciem szablonu
Poza tym C++17 b�dzie mia� #include <filesystem>; wyobra� sobie co� takiego:
std::for_each(file : directory)
{
	//...
}



Wa�ne:
Je�li jakikolwiek #include ko�czy si� nazw� na .h to
jest to biblioteka z C!.
Sporo z tych bibliotek przesz�o do C++, ale wyst�puj�
pod inna nazw�, poni�ej przyk�ady - u�ywaj nowych

	C		C++
<time.h>		<ctime>
<math.h>		<cmath>
	
	
	
	
	
	
	
	
	
