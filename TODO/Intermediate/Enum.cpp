/*
 * Enumeracja pozwala na stworzenie typu, kt�ry b�dzie mog� przyjmowa� tylko
 * warto�ci okre�one przez ciebie
 * warto�ci te podajesz w postaci textu
 *
 * jest to po prostu wygodne zast�pienie zabawy z intami czym�, co da si� wygodnie czyta�
 * enumy tak naprawe b�da zast�pione rejestrami/bitami/liczbami w czasie kompilacji,
 * w rezultacie dzialajac tak samo jak liczby
 *
 * zyskasz nawet performance, bo (je�linie nie podasz) to kompilator sam dobierze
 * na jakim typie te wartosci b�dzia faktycznie dzia�a� (najcz�ciej char/int)
 *
 * C++ gwarantucje poprawne konwersje miedzy intami a tymi nazwami textowymi
 *
 */


// przyk�adowa definicja
enum card_color{
	HEART,
	PIKE,
	DIAMOND,
	CLUB
};

//przyklad wykorzystania - obiekty typu Card maja zmienn� typu card_color
class Card{
public:
	int value;
	card_color color;
	/*
	 * ...
	 */
};

Card c1, c2;

// o wiele �atwiej, ni� zapami�tywanie kt�ry int to by� trefl
// na pewno lepsze, ni� czytanie: if(card.color == 3)
if(c1.color == CLUB)
{
	//...
}
	
//konwersja (niejawna)
int i = c1.color;
c2.color = 1;



///////////////////////////////////////////
// je�eli nie podasz warto�ci to domy�nie b�d� to kolejne liczby ca�kowite zaczynaj�c od 0
// je�eli podasz gdzie� warto�c, to kolejne enumy b�d� rosn�� o 1
// mo�esz posiada� enumy o takiej samej warto�ci
enum browser{
	IE,				//  0
	FIREFOX = 10, 	// 10
	CHROME,			// 11
	OPERA = 5, 		//  5
	SAFARI,			//  6
	SPARTAN = IE	//  0
};






/*
 * (C++11)
 * Enumy zystaly kilka usprawnien, generalnie pozwalaj�c
 * na bezpieczniejszy i wygodniejszy kod
 */

////////////////////////////////////////////
// przyklad mozliwego konfliktu

enum Antivirus_{
	AVAST,
	KASPERSKY,
	AVG,
	ESET
};

//avg to popularny skr�t od average (�rednia)
enum Statistic_{
	MAX,
	MIN,
	MEDIAN,
	AVG		//Error: nazwa 'AVG' jest ju� zaj�ta
};

////////////////////////////////////////////
// poprawny spos�b: (Wa�ne: keyword class ma tu inne znaczenie)

enum class Antivirus{
	AVAST,
	KASPERSKY,
	AVG,
	ESET
};

enum class Statistic{
	MIN,
	MAX,
	MEDIAN,
	AVG		//brak errora, antivirus::AVG oraz statistic::AVG to warto�ci dw�ch ro�nych typ�w
};

/////////////////////////////////////////////
// przypisuj�c warto�ci nale�y teraz u�ywa� scope operator (::)

Antivirus a = Antivirus::AVG;
Statistic s = Statistic::AVG;

////////////////////////////////////////////
// wszystkie konwersje musz� by� jawne
// static_cast poniewa� wszystkie enumy s� compile-time

int bad  = a; // Error: no implicit convertion from type 'Antivirus' to 'int'
int good = static_cast<int>(a);
//w drug� stron�
Statistic stat = static_cast<Statistic>(i); //je�eli 'i' ma warto�� zpoza zakresu enuma - Undefined Behaviour

///////////////////////////////////////////
// C++11 pozwala na przypisywanie warto�ci do enum�w
// ze wszystkiego, co jest mo�liwe do obliczenia w trakcie kompilacji
enum class Something{
	ONE = 1,
	TWO = ONE + 1,
	SIX = ONE + 5,
	OTHER = func() //tylko je�li funkcja func() jest oznaczona constexpr
};

///////////////////////////////////////////
// jesli bardzo tego pragniesz mozesz poda�
// na jakim typie enum ma dziala� faktycznie
enum class Color : unsigned char {
	WHITE,
	BLACK,
	RED,
	GREEN,
	BLUE
};












