/*
 * POINTERS
 * a pointer is a variable/constant/object that holds memory address of another variable/constant/object
 */

/*
 * there is a convention that uninitialized pointers should point to 0
 */
int* np1 = 0; 		//easy to misinterpret, np IS NOT an integer 0, its a pointer to nothing
int* np2 = NULL;	//easier to understand, but NULL is a macro and has the same dangers as all macros (NULL is simply #define NULL 0)
int* np3 = nullptr; //correct C++11 way to do it
/*
 * dereferencing nullpointer crashes the program
 */

//declares an int
int x;
//declares a pointer to int
int* p;
//modifying int
x = 5;
//changing/setting pointer (now it will point to x)
p = &x; //address-of operator
//chaning x through pointer
*p = 10; //asterisk (*) is dereferencing pointer, without it the instruction would change the pointer itself

std::cout <<  p << "\n"; //print memory address
std::cout << *p << "\n"; //print value pointed by p


      int*       p1 = &x;   //       pointer to       int
const int*       p2 = &x;   //       pointer to const int
int const*       p2 = &x;   //       pointer to const int (same as above)
      int* const p3 = &x;   // const pointer to       int
const int* const p4 = &x;   // const pointer to const int
int const*       p4 = &x;   //       pointer to const int (same as above)

      int * const * const p5 = &p3;//const pointer to const pointer to an int

//more examples
//http://stackoverflow.com/questions/1143262/what-is-the-difference-between-const-int-const-int-const-and-int-const




int array[10]; 
//array[0] ... array[9] is int
//but array is int*

*(array + 5) = 0;	//dereference(pointer + 5) - changing value 5 memory cells past array start
array[5] = 0;		//same as above

int* p2 = &( a[5] );//p2 points to a[5]
p2++; 				//p2 points to a[6]


/*
 * VOID POINTERS
 * 
 * The void type of pointer is a special type of pointer.
 * In C++, void represents the absence of type.
 * Therefore, void pointers are pointers that point to a value that has no type
 * (and thus also an undetermined length and undetermined dereferencing properties).
 *
 * This gives void pointers a great flexibility, by being able to point to any data type,
 * from an integer value or a float to a string of characters.
 * In exchange, they have a great limitation: the data pointed to by them cannot be directly dereferenced
 * (which is logical, since we have no type to dereference to), 
 * and for that reason, any address in a void pointer needs to be transformed
 * into some other pointer type that points to a concrete data type before being dereferenced.
 *
 * Possible uses may be:
 * - pass generic parameters to a function
 * - get the integer value of the memory address
 * - complicated hardware templates
 *
 * WARNING
 * on 32bit hardware sizeof(void*) =/= sizeof(void*) on 64bit
 * (usually 4 and 8 cells, 32bits and 64 bits)
 * 
 */

void* p3;

























