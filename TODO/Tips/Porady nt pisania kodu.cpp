//Jest par� rzeczy, o kt�rych warto pami�ta�. Pozwol� ci na szybsze pisanie kodu oraz latwe wykrycie b��d�w.

/*
 * 1. Dziel kod na segmenty, w idealnym programie int main() ma ponizej 10 linii
 * Je�li funckja musi mies dost�p do jaki� zmiennych po prostu zr�b jej argumenty.
 * Btw taki podzielony kod �atwiej jest optymalizowa� dla kompilatora.
 * 
 * Samo czytanie kodu te� si� staje �atwiejsze. Mo�esz si� skupi� na jednym zadaniu.
 * Szybciej to napiszesz. Mo�iwe, �e b�dziesz potrzebowa� u�y� tej funkcji w przysz�o�ci.
 * Dzi�ki temu b�dziesz ju� mie� to wykonane w wygodnej postaci; bez potrzeby szukania w
 * dlugim kodzie gdzie to jest, co to robi, i wgl jakie zmienne to potrzebuje
 * 
 * Staraj si� nie tworzy� kilku wielkich funcji (bo to nie ma sensu), tylko wiele ma�ych
 * Oko�o 25-50 (moze 100 dla wiekszych projekt�w) linijek na funckje wystarczy.
 * Je�eli ci si� jaka� funckja rozrasta mocno, podziel j� na kilka mniejszych
 * 
 */
	
/*
 * 2. ABSOLUTNIE WA�NE (mocno powi�zane z 1.)
 * NIGDY NIE POWTARZAJ �ADNEGO KODU.
 * Je�eli w jakichkolwiek 2+ miejscach programu widzisz te same instrukcje
 * utw�rz funkcj�, i wklej tam ten kod (nawet je�li to tylko 1 linia).
 * Bardzo mo�liwe, �e b�dzie potrzeba w przysz�o�ci te linie zmieni�.
 * Dzi�ki temu zaoszcz�dzisz czas na CTRL+F i ewentualne b��dy wynikaj�ce z
 * ro�nic 2 linijek, kt�re w rzeczywisto�ci mia�y by� takie same
 * 
 * Je�eli masz 2 funkcje, kt�re maj� np pierwsze 5 linijek takie same ... c�
 * zastosuj t� zasad� jeszcze raz
 * 
 * Je�li dwie linie wygl�daj� podobnie, rozwa� pisanie funckji z argumentami lub szablonu
 * (je�i obie linie wykonuj� bardzo podobne zadanie albo ro�ni� si� tylko typem)
 * 
 * Nawet nie wyobra�asz sobie, ile kodu mo�na dzi�ki temu zaoszcz�dzi�.
 * W szeg�lno�ci kiedy funckje wzywaj� inne funkcje a te kolejne... Wyobra� sobie,
 * jakby wygl�da� kod gdyby to wszystko rozwin�� ... sphagetti code
 *
 * Je�eli 2 linie wygl�daj� podobnie, realizuj� podobne zadanie, ale nie wiesz jak
 * napisa� pod to funckje zg�o� si� z tym do mnie. Bardzo mo�liwe, �e czego� cb naucz�
 * i/lub masz gdzie� jaki� "bad design" w programie
 */
	
	
	
	
/*
 * 3. "Magic numbers". Taki idiom. Chodzi o to, aby nie pisa� w kodzie liczb, ktore nie maj� nadanego znaczenia
 */
//zamiast:
while(klawisz != 27)
//lepiej jest pisa�
//gdzies daleko, wysoko u g�ry, lub w jakims includzie
const int KB_ESCAPE = 27;
//a w kodzie
while(klawisz != KB_ESCAPE)

//w ten spos�b nie pogubisz si� jak bedziesz sprawda� kod, �atwo zmienisz warto�� KB_ESCAPE
//nie muszisz szuka� w kodzie gdzie to i ile razy tego u�y�e�; ponadto kod staje si� bardziej czytelny dla innych

//Je�li co� ma przyjmowa� warto�ci tylko okre�lone, kt�re ciezko reprezontwa� liczbami
//(np etap tury, kolor karty, frakcja/gildia)- u�yj enumeracji (patrz Enum.cpp)




	
	
/*
 * 4. Nie u�ywaj zmiennych globalnych. PRAWIE �ADNYCH
 * Zobaczysz jaki to jest b�l jak zacziesz pisa� programy powyzej 1 pliku
 * (da si�, ale ilo�� warunk�w/includ�w ci� zabije) (nie m�wi�c juz o sprawdzaniu
 * gdzie te zmnienne s� zmieniane)
 * 
 * Jedyne s�uszne zmienne/obiekty globalne to takie, kt�re naprawde
 * s�usz� do r�nych zada� i/lub dost�p jest potrzebny wsz�dzie, np:
 * - generator liczb losowych
 * - obiekty/funckje do pisania plik�w log�w
 * - obiekt trzymaj�cy assety (szczeg�lnie w grach, gdzie ten sam obrazek mo�e by� potrzebny w r�nych miejscach)
 * - obiekt trzymaj�cy aktualnie wczytan� wersj� j�zykow� (np language_loader.getText(place::MAIN_MENU, lang::FRENCH);)
 */
	
	
/*
 * 5. Pisz komentarze. Ale sensowne komentarze (nie komentuj linijek oczywistych typu i++)
 * Zaleca si� pisanie komentarzy przy deklaracji funkcji. Pomaga przy sprawdzaniu, czy funkcja
 * robi dok�adnie to co si� oczekuje; a i tak�e jak chce si� wykorzysta� kod napisany wcze�niej - 
 * b�dziesz wiedzie� co si� stanie bez potrzeby czytania cia�a funkcji
 */
	
	
/*
 * 6. Stosuj si� do konwencji. Nie muszisz �ci�le pod��a� za znanymi,
 * mo�esz mie� w�asne, ale grunt to �eby si� trzyma� tego samego w obr�bie jednego projektu
 *
 * obja�nienia:
 * camelCase		wyra�enie rozpoczynaj�ce si� od ma�ej    litery, reszta s��w z du�ej, brak odst�p�w
 * PascalCase		wyra�enie rozpoczynaj�ce si� od wielkiej litery, ka�de s�owo z du�ej, brak odst�p�w
 * UPPERCASE		pe�ny capslock
 * lower_case		wszystko z ma�ej, slowa oddziela sie underscorem
 * _underscore		_ zamiast spacji
 *
 * Podstawowe konwencje
 * nazwaZmniennej					(camelCase)
 * nazwaFunkcji / NazwaFunkcji 		(oba s� w porz�dku, ale trzymaj si� jednego)
 * NazwaTypu						(PascalCase)
 * NazwaPliku						(PascalCase)
 * NAZWA_STALEJ						(UPPERCASE)
 * nazwa_namespaca					(lower_case)
 * T 								(nazwa typu do szablonu - pojedyncza du�a litera, przewa�nie N, T, U, V)
 *
 * //nie pisze sie skrotow duzymi literami
 * exportHtmlSource(); // NOT: exportHTMLSource();
 * openDvdPlayer();    // NOT: openDVDPlayer();
 *
 * //je�li u�ywasz zmiennych globalnych, oznacz to (wykorzystaj fakt, �e nie nale�� do jakiegokolwiek namespaca)
 * ::mainWindow.open();
 * ::logger.log("Successful Launch");
 *
 * //funkcje pobieraj�ce typy polimorficzne lub generalnie bardzo r�znie obiekty - nazwij argument tak samo
 * void connect(Database* database);
 *
 * //nazwy funkcji powinny sugerowa� mniej-wi�cej jak dzia�aj�
 * getLength()			//zapewne zwraca jak�� warto�� ze �rodka obiektu, jednorazowa, szybka operacja
 * setLength(10);		//ustawia co� w �rodku obiektu, raczej na sta�e
 * findMaxElement()		//sugeruje, �e takich element�w mo�e by� w obiekcie kilka
 * initializeFontSet();	//zapewne wystarczy u�y� tylko raz, na pocz�tku programu //The american "initialize" should be preferred over the English "initialise". Abbreviation init should be avoided.
 * isVisible();			//funkcje "is..." zwracaj� bool (true/false)
 *
 * //je�li kt�re� z pary wyst�puje, to pewnie drugie te�
 * get/set, add/remove, create/destroy, start/stop, insert/delete,
 * increment/decrement, old/new, begin/end, first/last, up/down, min/max,
 * next/previous, old/new, open/close, show/hide, suspend/resume
 *
 * //u�ywaj liczby mnogiej dla zbior�w
 * vector<Point>  points; 	//NOT: point
 * int            values[];
 *
 * //referencje/pointery to cecha typu, nie nazwy zmiennej (sporo takiego kodu spotkasz na necie, to relikt z C)
 * //dlatego pisz znaki przy typie
 * float* x; // NOT: float *x;
 * int& y;   // NOT: int &y;
 *
 * //p�tle
 * //zgodnie jak si� p�tle zag��biaj�: i, j, k, l, ...
 * //ewentualnie x, y, z je�li pasuje do kontekstu
 * //p�tle niesko�czona to najlepiej while(true)
 *
 * //wyr�wnuj kod, je�li jest bardzo analogiczny (zwr�c uwage jak wszystkie operatory s� w jednym pionie)
 *	if      (a == lowValue)    compueSomething();
 *	else if (a == mediumValue) computeSomethingElse();
 *	else if (a == highValue)   computeSomethingElseYet();
 *
 *	value = (potential        * oilDensity)   / constant1 +
 *			(depth            * waterDensity) / constant2 +
 *			(zCoordinateValue * gasDensity)   / constant3;
 *
 *	minPosition     = computeDistance(min,     x, y, z);
 *	averagePosition = computeDistance(average, x, y, z);
 *
 *	switch (value) {
 *	  case PHASE_OIL   : strcpy(phase, "Oil");   break;
 *	  case PHASE_WATER : strcpy(phase, "Water"); break;
 *	  case PHASE_GAS   : strcpy(phase, "Gas");   break;
 *	}
 *
 *
 *
 * <- dlugie multi-line commenty powinny mie� gwiazdki na poczt�tku kazdej linii
 * wiele edytor�w kodu robi to automatycznie
 *
 *
 * Stack Overflow zabije cie za u�ywanie hungarian notation. Jest to relikt przesz�o�ci.
 * Lepiej nie pr�b�j szuka� co to jest.
 *
 *
 * Je�li konwertujesz plik z wci�ciami kodu w postaci [wybierz jedno: tab�w/spacji]
 * na drugi spos�b wci�� to zgodnie z konwencj� tab to 4 spacje
 */



/*
 * 7. Nie u�ywaj NULL. Jest to makro z C.
 * C++ ma keyword nullptr
 */
	



/*
 * 8. U�ywaj enum�w je�i co� mo�e przybiera� r�ne warto�ci, ale inty s� nie do zrozumienia
 */


	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
