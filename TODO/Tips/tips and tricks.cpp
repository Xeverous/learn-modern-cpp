

/*
 * Fact: Mozesz definiowa� w�asny namespace
 * (je�li nie chcesz za�mieca� wolnych nazw)
 * Rzadko stosowane, bo chowanie zmiennych w �rodku klasy generalnie wystarczy
 *
 * mn�stwo przyk�ad�w: http://en.cppreference.com/w/cpp/language/namespace
 */


/*
 * ONS�UGA PLIK�W I INPUTU
 */
	
	//wczytanie textu z konsoli - uzyj getline()
	//getline znajduje si� w <string> - nie trzeba �adnych innych includ�w
	
	//template< class CharT, class Traits, class Allocator >
	//std::basic_istream<CharT,Traits>& getline( std::basic_istream<CharT,Traits>&& input,
	//                                           std::basic_string<CharT,Traits,Allocator>& str,
	//                                           CharT delim );
	
	/*
	 * Troszeczke teori
	 * std::string to tak naprawd� alias szablonu:
	 * typedef std::basic_string<char> string;

	 * jest w tym sporo szablon�w, gwrantuj� one wygodne w/wy prowadzanie danych
	 * generalnie ca�a sprawa stream, istream, basic_istream jest troch� zagmatwana
	 * (jedne streamy s� klasami pochodnymi innych)
	 *
	 * wracaj�c do funkcji:
	 * argument 1 - stream, z kt�rego maj� by� pobierane znaki
	 * 				mo�e to by� zar�wno std::cin jak i std::ifstream (czyli plik) czy r�wnie� std::stringstream
	 * argument 2 - string (lub cos podobnego), do czego znaki maj� zosta� zapisane
	 * 				zwr�� uwag�, �e jest pobierany przez referencje (nast�pi modyfikacja)
	 * argument 3 - znak, na kt�rych getline si� zatrzyma, domy�lnie '\n', je�li nie
	 * 				chcesz zmienia�, nie muszisz podawa�
	 * return	  - referencja to streama, z kt�rego pobiera�e� (raczej nie potrzebujesz)
	 * 				generalnie mo�esz to zignorowa�, u�ywa si� tego do "operator chaining"
	 *				mo�esz wykorzysta� fakt, �e je�i getline() si� nie powiedzie (np koniec pliku)
	 *				to konwersja (niejawna) referencji na bool zwr�ci false
	 *
	 */
	
	//wczytanie z konsoli
	#include <iostream>
	std::string str;
	std::getline(std::cin, str);

	//wczytanie 1 linii z pliku
	#include <fstream>
	std::ifstream file("file.txt");
	std::getline(file, str);

	//obr�bka pliku linia po linii
	for(std::string line; std::getline(file, line); ) //referencja zwr�cona przez getline b�dzie si� konwertowa� do false jak plik si� sko�czy
	{
		//... rob co� z line
	}

	//jak podzieli� string na cz�ci
	// je�li wiesz jakie to cz�ci - u�yj std::string::substr()
	// �adne przyk�ady: http://en.cppreference.com/w/cpp/string/basic_string/substr
	// mo�esz tak�e u�yc stringstream (taka klasa co nie wypisuje jak cout i obsluguje operatory = >> itp) i klas pochodnych
	// przyk�ady na SO: http://stackoverflow.com/documentation/c%2b%2b/5348/stdstringstream

	// Wa�ne - nie mieszaj wczytywa� bezpo�rednio cin i getline
	// je�li ju� musisz to przed getlinem u�yj cin.ignore()
	#include <limits> //potrzebny do lini ni�ej
	std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');


	// Jak najlepiej zatrzyma� program konsolowy
	// - uzyj std::getline (zatrzyma tylko enter)
	// - uzyj std::getchar() (jest w <cstdio>)


/*
 * Jak zwr�cic kilka warto�ci z funkcji naraz?
 * Nie da sie, ale mo�esz zwr�ci� obiekt
 */

	#include <utility>
	std::pair<int, int> pr = getMinAndMax( /* jakis array lub vector */ );
	//dost�p do warto�ci masz przez pr.first i pr.second

	//jesli potrzebujesz wiecej niz 2:
	//std::tuple<T...>
	//dost�p jest przez std::get< _nr_ >( _tuple_ ) gdzie nr to liczba calkowita a tuple to obiekt, z ktorego chesz pobrac te warto�ci

	//wiecej przyk�ad�w: http://stackoverflow.com/documentation/c%2b%2b/487/returning-several-values-from-a-function


/*
 * TYPOWE ERRORY
 */
	//KOMPILACJA

		//brak funkcji, kt�ra pobiera takie typy/tak� ilo�� argument�w - zapewne zle uzycie funcji, lub brak konwersji
			//error: no match for operator'...' (operand types are '...' and '...')
			//error: no matching function for call to '...'
				//note: candidate '...' expects '...' arguments, '...' provided						//niew�a�ciwa ilo�� argument�w (dla tego overloada)
				//note: candidate '...', no known convertion for argument '...' from '...' to '...' //kt�ry� argument nie pasuje typem

		//brak jakiegos #include moze powodowac:
			//error: '...' is not a member of std
			//error: '...' does not name a type
			//error: '...' has initializer but incomplete type

		//niewlasciwe uzycie const lub brak koniecznego casta
			//error: passing const '...' as '...' argument of '... func(...)' discards qualifiers

		//brak casta
			//error: could not convert '...' to '...'
			//error: no known convertion for argument '...' from '...' to '...'

	//LINKING

		//plik exe nie zosta� wy��czony przed kompilacj� / lub zbyt p�no
			//error: write permission denied (lub co� w tym stylu)
			//error: ld returned 1

		//deklaracja czego�, ale brak definicji w jakimkolwiek pliku
			//error: undefined reference to '...'
	





/*
 * PRZYDATNE PARAMETRY DO KOMPILACJI / LINK
 */
	//konieczne do pracy w standardzie C++11
		//-std=c++11 lub -std=c++0x
		//-std=gnu++11 (dla MinGW powyzej wersji 4.9.3) (wersje sprawdz w cmd komenda g++ --version)

	//optymalizacja
		//-O0 - aby zachowac wszystkie linie kodu - przydatne w debugu do skakania z linii do linii (instrukcje sa drobniejsze)
	
		//-Os (optimize size) (nie wiem komu potrzebny mniejszy .exe)
			//dziala jak -O2 ale bez  -falign-functions -falign-jumps -falign-loops -falign-labels -freorder-blocks -fprefetch-loop-arrays
	
		//-O1 optymalizuje to:
			//-fdefer-pop -fmerge-constants -fthread-jumps -floop-optimize -fcrossjumping -fif-conversion -fif-conversion2 -fdelayed-branch -fguess-branch-probability -fcprop-registers
	
		//-O2 optymalizuje wsyzstko to co -O1, ale tez:
			//-fforce-mem -foptimize-sibling-calls -fstrength-reduce -fcse-follow-jumps -fcse-skip-blocks -frerun-cse-after-loop -frerun-loop-opt -fgcse -fgcse-lm -fgcse-sm -fdelete-null-pointer-checks -fexpensive-optimizations -fregmove -fschedule-insns -fschedule-insns2 -fsched-interblock -fsched-spec -fcaller-saves -fpeephole2 -freorder-blocks -freorder-functions -fstrict-aliasing -falign-functions -falign-jumps -falign-loops -falign-labels

		//-O3 optymalizuje wsyzstko to co -O2, ale tez:
			//-finline-functions -frename-registers

		// UWAGA czasem optymalizacja przyspiesza nawet czas kompilacji!

	//budowa pod .dll/.lib/.a/.exe z wielu pikow naraz (podzial osobne klasy w plikach)
		// -c (compile, but do not link) (output: .o file) (potem linker zlaczy wszystkie .o w jeden .dll lub .exe)

	//zmien nazwe pliku wyjsciowego
		//-o name
		//(the default is to put an executable file in a.out, the object file for source.* in source.o, its assembler file in source.s)

	//check syntax only (do not compile) (jesli gubisz sredniki)
		//-fsyntax-only

	//do not show any warnings
		//-w
	
	//Show most common warings
		//-Wall
			//Bardzo polecam, bardzo pomaga bo ostrzega o takich reczach jak np:
			if(x = y) //brak ==
			//lub to (ten else jest do drugiego ifa - warning zasugeruje uzycie { } )
			if(a)
				if(b)
					foo();
			else
				bar();
			//lub warningi typu: unused variable / variable set but not used


	//sciezka do folderu z uzywanymi blibliotekami lub includami
		//-L"C:/folder/folder/folder/"

	//linker (dodaj dll potrzebny do uruchomienia) (przyklad dla sfml-system.dll)
		//-lsfml-system

	//nie tworz okna konsoli (kiedy piszesz program uzywajacy okien z WindowsAPI / OpenGL / DirectX)
		//-mwindows


	
	
	
	
	
	
	
	
	
	
	
	
