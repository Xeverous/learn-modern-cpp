/*
 * Litera�y to sta�e, znane w czasie kompilacji
 * domy�lnie:
 * 		warto�ci ca�kowite to int
 * 		warto�ci u�amkowe  to double
 * 		warto�ci znakowe   to char
 * 		ci�gi    znak�w    to const char*
 *
 * C++11: Wszystkie litera�y mog� by� u�yte jako constexpr
 */


/*
 * LITERALS
 * (unnamed compile-time constants)
 * a suffix can be appended to modify its type
 * Defaults:
 * - for integer types: int
 * - for fractions: double
 * - for 1 character: char
 * - for multiple characters: const char*
 */

75         // decimal
0113       // octal
0x4b       // hexadecimal

75         // int
75u        // unsigned int
75l        // long
75ul       // unsigned long 
75lu       // unsigned long (order doesn't matter)
		
// all double
3.14159    // 3.14159
6.02e23    // 6.02 x 10^23
1.6e-19    // 1.6 x 10^-19
3.0        // 3 but the type is double

3.14159L   // long double
6.02e23f   // float  

'x' //char
"Hello world" //const char*

//escape sequences
'\n'	//newline
'\r'	//carriage return
'\t'	//tab
'\v'	//vertical tab
'\b'	//backspace
'\f'	//form feed (page feed)
'\a'	//alert (beep)
'\''	//single quote (')
'\"'	//double quote (")
'\?'	//question mark (?)
'\\'	//backslash (\)

//line endings
/*
 * Text files created on DOS/Windows machines have different line endings than files created on Unix/Linux.
 * DOS/Windows uses carriage return and line feed ("\r\n")
 * Unix uses just line feed ("\n")
 * MacOS uses just carriage return ("r")
 * 
 * .txt files created on Windows will be displayed correctly on Unix systems
 * but files created on Unix/MacOS will appear to have no linebreaks on Windows
*/
		
//splitting across multiple lines
std::string x = "expressed in \
multiple \
lines";
		
//Raw string (disables escape sequences)
std::string x = R"string with \backslash"











	
