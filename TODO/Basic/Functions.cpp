/*
 * Postwawowe teorie o funckcjach znacie, wiec nie bd si� rozwodzi�
 * co to jest i dlaczego jest opate na definicji funkcji z matematyki
 *
 * Ale warto wspomnie� kilka rzeczy:
 */

//1. Return mo�e by� zignorowany
int square(int x)
{
	/*
	 * jakies inne linijki, na kt�rych wykonianu ci zale�y
	 */
	return x * x;
}

square(5); //po prostu, returnu nie trzeba zapisywa�

//3. Ka�dy operator to te� funkcja (1- lub 2-argumentowa)
//	przyk�adem (cz�sto u�ywanym) jest std::string::operator=
std::string s1 = "s1";
//	lub std::ostream::operator<< kt�ry ma prze�adowania dla wielu typ�w
std::cout << s1 << "\n";

//4. Wiele funkcji zwraca referencje to tego samego obiektu
//	pozwalaj�c na tak zwany operator chaining - wykonanie funkcji na tym, co zwr�ci poprzednia funkcja
// 	najprostszym przyk�adem jest std::ostream::operator<< kt�ry zwraca ref do siebie
std::cout << "a" << "b" << "c" << "d";
//jest to r�wnoznaczne z:
operator<<((operator<<((operator<<((operator<<(std::cout, "a")), "b")), "c")), "d"); //return po d zostaje zignorowany
// Wi�cej informacji i przyk�ad: Klasy/Przyklad 04.cpp

//5. funckje kt�re s� wewn�trz klasy, mo�emy uruchomi� tylko na istniej�cym obiekcie
std::string str;
std::string* p = &str;
int x = str.length(); //przez obiekt  - u�yj .
int y =  p->length(); //przez pointer - u�yj ->
// samo "int z = length();" nie ma logicznego sensu
// std::string::length jest funkcj� klasow� (metod�) i wymaga istniej�cego obiektu


//6. Argumenty mog� mie� warto�ci domy�lne
//	domy�lne warto�ci musz� by� argumentami z prawej
int f(int x, int y = 5, int z = 10);

int f(int x, int y, int z) //zwr�c uwag�, �e domy�lne warto�ci podaje si� tylko w deklaracji
{
	return x * y * z;
}

f(3); //takie samo co f(3, 5, 10);

// 7. return 0 w funkcji int main() jest domy�lny
//	je�li nie podasz, kompilator zako�czy maina zerem



/*
 * przyk�ady sygnatur funkcji
 * rozne sbosoby na pobieranie argument�w
 */
class SomethingBig { /* ... */ }; //zaloznmy, ze cos duzego

//pass by value (working on a copy of the value)
//used when arguments are the simplest types
//passing big objects by value should be avoided (expensive and unneccessary copies)
void func(int a, int b, int c, SomethingBig d);

//pass by const value (working on a copy of the value)
//used when arguments are the simplest types and there is no need to modify them
void func(const int a, const int b, const int c, const SomethingBig d);//again expensive copy here

//pass by reference, variables can be modified
//used when you need to modify arguments or access members of an object (only pointer to d is created)
void func(int& a, int& b, int& c, SomethingBig& d);

//pass by const reference
//useful when you need to acces an element (especially of a big object) but dont want to modify it
void func(const int& a, const int& b, const int& c, const SomethingBig& d);

//pass by pointer, you can modify both the pointer and the value
//used when you need to modify arguments or access members of an object (only pointer to d is created)
void func(int* a, int* b, int* c, SomethingBig* d)
{
    //note that a, b, c, d are now pointers, you need to dereference them
    (*a) = (*a) * 2;
    std::cout<<"b addres = "<< std::hex <<   b  <<"\n";
    std::cout<<"b value  = "<< std::dex << (*b) <<"\n";
    std::cout<<"d function = "<< d->SomeFunction() <<"\n"; //note the -> (arrow operator), thats how you access things from an object pointer
}

//pass by pointer to const
//useful when you need to acces an element (especially of a big object) but dont want to modify it
void func(const int* a, const int* b, const int* c, const SomethingBig* d)
{
    //same as above
    //you just can't modify values
}


/*
 * Podstawowe zasady przy budowaniu funckji
 * poni�sze instrukcje wynikaj� z tego, �e funckje to faktyczne jumpy w kodzie maszynowym
 * zatem jesli nie trzeba, to nie nalezy kopiowac duzej ilosci danych
 * przy czym tez nie nalezy robic do wszystkiego referencji (s� wolniejsze od kopii 1 zmiennej ale szybsze od kopiowania 2+)
 *
 * Domy�lnie:
 * - jezeli typ     jest podstawowy (char, int, float, double, etc) - przez warto��
 * - jezeli typ nie jest podstawowy (jakis struct/class, np std::string) - przez reference (&) (kompilator zast�pi to pointerami za ciebie)
 * Dodatkowo:
 * - jezeli nie masz potrzeby modyfikacji arguent�w lub ich kopii - zawsze dodaj const (unikniesz b�ed�w i pozwolisz na optymalizacje)
 * 		(bardzo cz�sto funkcje jak np getLength(), getSize(), etc s� const)
 * - jezeli chcesz, aby argument zostal zmieniony na sta�e - dodaj reference (&) (je�ie jej nie masz)
 * - wersji z pointerami (*) uzywaj naprawde tylko wtedy, kiedy musisz
 * - jezeli piszesz kod oparty na wyj�tkach, a twoja funckja nigdy ich nie rzuca - dodaj noexcept
 * Optymalizacja
[1]- je�eli funckja nie modyfikuje absolutnie niczego, oznacz funkcje jako const
[2]- je�eli funckja ze swoimi danymi mo�e by� obliczona podczas kompilacji (i tego chcesz), oznacz funckje jako constexpr
 *		C++11: constexpr funckja nie moze miec w �rodku zadnych dodatkowych zmiennych/instrukcji (mozna uzywac tylko argument�w i return)
 *		C++14: bugfix tego powyzej
 * - [Historia] je�eli chcesz, by tre�� funckji zosta�a wklejona w kod (bez jumpa) (dziala szybciej, ale zape�nia cache CPU) - inline
 * 		UWAGA: Wsp�lczesne kompilatory s� na tyle w tym dobre, �e ignoruj� ten keyword i same decyduja czy to sie op�aca czy nie
 * 		mog� nawet inlinowa� funkcje, kt�rych nie oznaczysz jako inline
 * 		ponadto inline komplikuje sprawy, bo definicja takiej funkcji musi by� w headerze
 */

//podaje wz�r i deklaracji i definicji bo niekt�re keywordy podaje si� tylko przy deklaracji (akurat te nie)

//[1] deklaracja
void const_func(/*...*/) const; // <- tutaj
//[1] definicja
void const_func(/*...*/) const  // <- tutaj
{
	// ...
}

//[2] deklaracja
constexpr int compile_time_func(/*...*/);
//[2] definicja
constexpr int compile_time_func(/*...*/)
{
	// ...
}







/*
 * FUNCTION OVERLOADING
 * Wa�ne: nie mo�na prze�adowa� funkcji tylko na podstwanie typu returna
 */

int multiply(int a)
{
	return a * 5;
}

double multiply(double a)
{
	return a * 5.0;
}

//disable implicit convertions from float to double (this will make impossible to call multiply() with float)
float multiply(float a) = delete;


int    mi = multiply(3);
double md = multiply(3.0);
float  mf = multiply(3.0f); //error




/*
 * Nizej featury dla pragn�cych wi�cej
 * templaty pozwalaj� stworzy� wszystkie mo�liwe overloady naraz
 */


/*
 * FUNCTION TEMPLATES
 * - T can be used to denote the same type
 * - multiple TYPES can be used (eg void printTwoDifferent(T a, U b))
 * - its possible to make a template of another template
 */
//templates can create multiple overloads with specific, or non-specific types
template<class T>
T multiply(T a)
{
	return a * static_cast<T>(5);
}

template<class T, class U> 
T multiply(T a, U b)//will cast b to type of a and return value of type of a
{
	return a * static_cast<T>(b);
}

template<class T>
T multiply(T a, int b)//will cast b to the type of a
{
	return a * static_cast<T>(b);
}


/*
 * C++11 VARIADIC TEMPLATES
 * elipsis (...) can be used to denote any 0+ amount of arguments
 * there is no simple way to extract each argument
 * its usually done by using first parameter and passing rest to next recursion
 */

void variadic_cout(){}//generic case (last in recursion)

template<typename First, typename ...Rest>
void variadic_cout(First && first, Rest && ...rest)//arguments passed by rvalue reference
{
    std::cout << std::forward<First>(first);	//make use of 1st argument
    variadic_cout(std::forward<Rest>(rest)...); //next recursion step, 1 less argument
}

//function variadic_cout() will accept ANY amount of arguments with ANY types that std::cout::operator<< suppports
// variadic_cout(5, 7.0, "asdf");
// variadic_cout(9.7f, std::string("abc"), 1, 9ul, 3.0L);
























