
/*
 * OPERATORS
 * more info
 * http://www.cplusplus.com/doc/tutorial/operators/
 */

//funkcje logiczne (zwracaja bool)
==	//Equal to
!=	//Not equal to
<	//Less than
>	//Greater than
<=	//Less than or equal to
>=	//Greater than or equal to

//bramki
&	//AND	Bitwise AND
|	//OR	Bitwise inclusive OR
^	//XOR	Bitwise exclusive OR
~	//NOT	Unary complement (bit inversion)
<<	//SHL	Shift bits left
>>	//SHR	Shift bits right

//Niestety dopiero C++17 ma mozliwosc pisania binarnych literal�w w kodzie
//C++11 tylko dziesietne, hex i osemkowy dlategow tych przykladach pisze 0x

//przesuwanie bitow (tu o 3 w lewo)
int x = 3;  //binary 0000 0011
x = x << 3; //binary 0001 1000

//bramka & (dla kazdej pary kolejnych bit�w)
int a = 0xF0;  //binary 1111 0000
int b = 0xAA;  //binary 1010 1010
int c = a & b; //binary 1010 0000

/*
 * Alternative tokens
 * There are alternative spellings for several operators defined as tokens with special meaning in the C++ standard.
 */
&&	and
&=	and_eq
&	bitand
|	bitor
~	compl
!	not
!=	not_eq
||	or
|=	or_eq
^	xor
^=	xor_eq


/*
 * SWITCH
 */

switch (x) {
  case 1:
  case 2:
  case 3:
    cout << "x is 1, 2 or 3";
    break;
  case 4:
	cout << "x is 4";
	break;
  default:
    cout << "x is not 1, 2 nor 3";
  }

//IF TREES

//nested if inside if
if()
{
	if()
	{
		
	}
	else
	{
		
	}
}
else
{
	if()
	{
		
	}
	else
	{
		
	}
}

//switch-like if
if()
{
	
}
else if()
{
	
}
else if()
{
	
}
else //this else is not neccessary
{
	
}

//while loops - use true to make infinite loop
while(true)
{
	
}

//will be executed at least once
do{
	
}while(/*...*/)



//FOR LOOPS
//multiple initializaions and increase
for(int n=0, int i=100; n!=i; ++n, --i ) //generalnie niezalecane
{
   if(n == 70)
	   break; //will exit the loop
   
   if(n == 50)
	   continue; //will instantly move to the next iteration (not performing cout below)
   
   std::cout<<"n = "<<n<<"\ni = "<<i<<"\n";
}






//begin/end loops are done very often with containers (vectors, lists, maps, etc)
//iterators provide safety of used type and will never go out of bound
std::vector<int> v = {0, 1, 2, 3, 4, 5};
for (std::vector<int>::iterator it = v.begin(); it != v.end(); ++it) {
    /* std::cout << *it; ... */
}

//auto can shorten the declaration (automatic type deduction, std::vector<int>::begin() returns std::vector<int>::iterator)
for (auto it = v.begin(); it != v.end(); ++it) {
    /* std::cout << *it; ... */
}

/*
 * C++11 for loops
 * podstawa to u�ycie operatora :
 * teraz i b�dzie tym samym, co poprzednio t[i]
 */

//range-based
std::string str = "hello";
for(char c : str)
	std::cout << c << " ";

std::vector<int> v = {0, 1, 2, 3, 4, 5};
for (const int& i : v) // access by const reference (read-only)
	std::cout << i << ' ';
std::cout << '\n';

for (auto i : v) // access by copy, the type of i is int
	std::cout << i << ' ';
std::cout << '\n';

for (auto& i : v) // access by reference, the type of i is int&
	std::cout << i << ' ';
std::cout << '\n';

for (const auto& i : v) // access by const reference, the type of i is const int& (read-only)
	std::cout << i << ' ';
std::cout << '\n';

for (int n : {0, 1, 2, 3, 4, 5}) // the initializer may be a braced-init-list
	std::cout << n << ' ';
std::cout << '\n';

int a[] = {0, 1, 2, 3, 4, 5};
for (int n : a) // the initializer may be an array
	std::cout << n << ' ';
std::cout << '\n';


//C++17
std::map< std::string, int > m;
// ... some map operations
for (auto const& [key, value] : m) {
    std::cout << "The value for " << key << " is " << value << '\n';
}













