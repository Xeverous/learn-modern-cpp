/*
 * Generalnie nie mozna nazywa� zmiennych nazwami jak jakies keywordy
 * cz�� keyword�w jest zarezerwowanych na przysz�o�� lub ma znaczenie tylko historyczne
 */

/*
 * KEYWORDS
 * 
 * C++ uses a number of keywords to identify operations and data descriptions;
 * therefore, identifiers created by a programmer cannot match these keywords.
 * The standard reserved keywords that cannot be used for programmer created identifiers are:
 */

alignas, alignof, and, and_eq, asm, auto, bitand, bitor, bool, break, case, catch, char, char16_t, char32_t, class, compl,
const, constexpr, const_cast, continue, decltype, default, delete, do, double, dynamic_cast, else, enum, explicit, export,
extern, false, float, for, friend, goto, if, inline, int, long, mutable, namespace, new, noexcept, not, not_eq, nullptr,
operator, or, or_eq, private, protected, public, register, reinterpret_cast, return, short, signed, sizeof, static,
static_assert, static_cast, struct, switch, template, this, thread_local, throw, true, try, typedef, typeid, typename,
union, unsigned, using, virtual, void, volatile, wchar_t, while, xor, xor_eq
		
//In addition to keywords, there are identifiers with special meaning, which may be used as names of objects or functions, but have special meaning in certain contexts.
override //(C++11)
final //(C++11)
transaction_safe //(TM TS)
transaction_safe_dynamic //(TM TS)


/*
 * IDENTIFIERS
 * 
 * A valid identifier is:
 * 		a sequence of one or more letters, digits, or underscore characters (_)
 * 		spaces, punctuation marks, and symbols cannot be part of an identifier
 * 		shall always begin with a letter or underline character (_) (can not begin with a digit)
 * Reserved indentifiers for compiler-specific/external libriaries:
 * 		all starting with 2 underscores (__...)
 * 		all starting with 1 underscore and uppercase letter (_A...)
 * 
 */
		

		
		
		

	
/*
 * DELCARATION vs DEFINITION
 * A declaration can appear as many times as desired. A definition can only happen once
 */	
/*
 * A declaration introduces an identifier and describes its type, be it a type, object, or function.
 * A declaration is what the compiler needs to accept references to that identifier. These are declarations:
 */
extern int bar; //extern is needed here to prevent complier/linker from uisng it as a definition
extern int g(int, int);
double f(int, double); // extern can be omitted for function declarations
class foo; // no extern allowed for type declarations
/*
 * A definition actually instantiates/implements this identifier.
 * It's what the linker needs in order to link references to those entities.
 * These are definitions corresponding to the above declarations:
 */
int bar;
int g(int lhs, int rhs) {return lhs*rhs;}
double f(int i, double d) {return i+d;}
class foo {};
		
		
		
		
		

		
		
		
		
		
		
		
		
		
		
