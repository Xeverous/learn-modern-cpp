/*
 * Tu zostanie om�wiony Diamond Problem
 */


// Zatem rozwa�my taki uk�ad klas:
class A { public: void Foo() {} };
class B : public A {};
class C : public A {};
class D : public B, public C {};

/*
 * tak wygl�da uk�ad dziedzicze�:
 *   A
 *  / \
 * B   C
 *  \ /
 *   D
 */

/*
 * Na czym polega problem?
 * Klasa D b�dzie zawiera� w sobie podwojon� tre�� klasy A
 * (obie definicje funckji, 2x te same zmienne itd)
 * (jedn� z B i jedn� z C)
 */

// Jak si� przed tym ustrzec?
// Wirtualne dziedziczenie!

class A { public: void Foo() {} };
class B : public virtual A {};
class C : public virtual A {};
class D : public B, public C {};

// teraz klasa D nie b�dzie mia�a problem�w
// zwi�zanych z podwojon� tre�ci�

// Wi�cej wyja�nie� i przyk�ad�w:
// http://stackoverflow.com/questions/21558/in-c-what-is-a-virtual-base-class
