#include <iostream>

const unsigned int ARRAY_SIZE = 997;

template <class T>
class single_list
{
public:
	single_list(): head(nullptr) {}
	~single_list() { clear(); }

	// TODO: emplace_front()?
	void push_front(T value)
	{
		single_list_node* new_node = new single_list_node;
		new_node->data = value;
		new_node->next = head;

		head = new_node;
	}

	void pop_front()
	{
		if(head) // empty list error checking
		{
			single_list_node* temp = head;
			head = head->next;
			delete temp;
		}
	}

	void clear()
	{
		while(head)
			pop_front(); // code reuse
	}

	bool empty()
	{
		return head == nullptr;
	}

private:
	struct single_list_node
	{
		T data;
		single_list_node* next;
	};

	single_list_node* head;

public:
	class iterator
	{
	public:
		iterator(single_list_node* ptr): ptr(ptr) {}
		iterator operator++() { ptr = ptr->next; return *this; }
		bool operator!=(const iterator& other) const { return ptr != other.ptr; }
		T& operator*() { return ptr->data; }
	private:
		single_list_node* ptr;
	};

	iterator begin() const
	{
		return iterator(head);
	}

	iterator end() const
	{
		return iterator(nullptr);
	}
};

unsigned short hash_func(const char* input)
{
	unsigned short hash = 0;

	while (*input)
		hash = hash * 101  +  *input++;

	return hash % ARRAY_SIZE;
}


bool compare_names(const char* c1, const char* c2)
{
	while(*c1 && *c2)
	{
		if(*c1 != *c2)
			return false;

		c1++;
		c2++;
	}

	return true;
}

const unsigned MAX_LENGTH = 34;

struct transaction
{
	char name[MAX_LENGTH];
	int value;

	bool operator==(const transaction& other) const
	{
		return compare_names(name, other.name);
	}

	transaction()
	{
		name[0] = '\0';
		value = 0;
	}

	transaction(const char* c1, const char* c2, int value) : value(value)
	{
		unsigned i;
		for(i = 0; *c1; c1++)
		{
			name[i] = *c1;
			i++;
		}

		for(; *c2; c2++)
		{
			name[i] = *c2;
			i++;
		}

		name[i] = '\0';
	}
};


class hash_table
{
public:
	void add(const transaction& object)
	{
		unsigned index = hash_func(object.name);

		for(single_list<transaction>::iterator it = array[index].begin(); it != array[index].end(); ++it)
		{
			// found => sum up and exit function
			if(*it == object)
			{
				(*it).value += object.value;
				return;
			}
		}

		// not found => create new object in list
		array[index].push_front(object);
	}

	void print(const char* c1, const char* c2) const
	{
		transaction t(c1, c2, 0);
		unsigned index = hash_func(t.name);

		for(single_list<transaction>::iterator it = array[index].begin(); it != array[index].end(); ++it)
		{
			// found => print associated value
			if(compare_names((*it).name, t.name))
			{
				std::cout << (*it).value << '\n';
				return;
			}
		}

		// not found => print 0
		std::cout << "0\n";
	}

private:
	single_list<transaction> array[ARRAY_SIZE];
};

int main()
{
	hash_table table;

	char c;
	int val;

	char input1[MAX_LENGTH / 2];
	char input2[MAX_LENGTH / 2];
	while(std::cin >> c)
	{
		std::cin >> std::ws;
		std::cin >> input1;
		std::cin >> std::ws;
		std::cin >> input2;

		if(c == '+')
		{
			std::cin >> std::ws;
			std::cin >> val;

			transaction t(input1, input2, val);
			table.add(t);
		}
		else if(c == '?')
		{
			table.print(input1, input2);
		}
	}
}
