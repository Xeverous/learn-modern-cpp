#include <iostream>

template <class T>
struct single_list_node {
	T data;
	single_list_node<T>* next;
};

template <class T>
class single_list
{
public:
	single_list(): head(nullptr) {}
	~single_list() { clear(); }

	void push_front(T value)
	{
		// Katastrofa! kompilator na STOSie nie obsługuje aggregate initialization
		//single_list_node<T>* new_node = new single_list_node<T>{value, head}; 
		
		// obejście
		single_list_node<T>* new_node = new single_list_node<T>;
		new_node->data = value;
		new_node->next = head;
		
		head = new_node;
	}
	
	void pop_front()
	{
		if(head) // empty list error checking
		{
			single_list_node<T>* temp = head;
			head = head->next;
			delete temp;
		}
	}
	
	void insert_after(single_list_node<T>* target, T value)
	{
	    //single_list_node<T>* new_node = new single_list_node<T>{value, target->next};
		
		// objeście
		single_list_node<T>* new_node = new single_list_node<T>;
		new_node->data = value;
		new_node->next = target->next;
		
	    target->next = new_node;
	}
	
	void swap_first_2()
	{
		single_list_node<T>* temp = head;
		head = head->next;
		temp->next = head->next;
		head->next = temp;
	}
	
	void sum_first_2()
	{
		T x = head->data;
		pop_front();
		T y = head->data;
		pop_front();
		push_front(x + y);
	}
	
	void pop_and_move()
	{
		T n = head->data;
		pop_front();
		
		if(n == 0 || n == 1)
			return;
		
		T start = head->data;
		single_list_node<T>* temp = head;
		for(int i = 1; i < n; i++)
		{
			if(temp->next == nullptr)
				return;
			
			temp = temp->next;
		}
		
		pop_front();
		insert_after(temp, start);
	}
	
	void clear()
	{
		while(head)
			pop_front(); // code reuse
	}

	bool empty()
	{
		return head == nullptr;
	}
	
	void print()
	{	
		single_list_node<T>* temp = head;
		while(temp != nullptr)
		{
			std::cout << temp->data << ' ';
			temp = temp->next;
		}
		
		std::cout << '\n';
	}
	
private:
	single_list_node<T>* head;
};

int main()
{
	char buf;
	single_list<int> list;

	do {
		buf = std::getchar();

		if (buf == 'p') {
			list.print();
		}
		else if (buf == 'x') {
			list.pop_front();
		}
		else if (buf == 's') {
			list.swap_first_2();
		}
		else if (buf == '+') {
			list.sum_first_2();
		}
		else if (buf == 'r') {
			list.pop_and_move();
		}
		else if (buf == 'q') {
			break;
		}
		else if(buf == ' ' || buf == '\n') {
			continue;
		}
		else {
			int x = buf - '0';
			do
			{
				buf = std::getchar();
				
				if(buf == ' ' || buf == '\n')
					break;
				
				x *= 10;
				x += (buf - '0');
			} while(true);
			
			list.push_front(x);
		}
	} while (true);
}



























