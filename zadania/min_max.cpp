#include <iostream>
#include <limits>

template<class T>
const T& min(const T& a, const T& b)
{
    return (b < a) ? b : a;
}

template<class T>
const T& max(const T& a, const T& b)
{
    return (a < b) ? b : a;
}

// given value v, returns smallest 2^n that is >= v
// http://stackoverflow.com/questions/466204/rounding-up-to-next-power-of-2
unsigned long closest_power_of_two(unsigned long v)
{
    v--;
    v |= v >> 1;
    v |= v >> 2;
    v |= v >> 4;
    v |= v >> 8;
    v |= v >> 16;
    //v |= v >> 32; // for 64-bit
    v++;
    return v;
}

// https://en.wikipedia.org/wiki/Segment_tree
// Segment trees support searching for all the intervals that contain a query point in O(log n + k), k being the number of retrieved intervals or segments
template <class T>
class segment_tree
{
public:
	segment_tree(const T* source, unsigned length)
	{
		// in theory uses O(n log n) storage, but not all cells are used, therefore
		// the amount of needed memory is equal to 2 * (closest 2^n >= length) - 1
		data = new pair[2 * closest_power_of_two(length) - 1];
		size = length;
		// time to build: O(n log n)
		build(source, 0, length, 0);
	}

	~segment_tree()
	{
		delete[] data;
	}

	struct pair
	{
		pair() {}
		pair(const T& left, const T& right): min(left), max(right) {}
		pair& operator=(const pair& other)
		{
			min = other.min;
			max = other.max;
			return *this;
		}
		T min, max;
	};

	// not used - only as example
	void print()
	{
		for(unsigned i = 0; i < 2 * closest_power_of_two(size) - 1; i++)
		{
			std::cout << "data[" << i << "] = " << data[i].min << "\n";
			std::cout << "data[" << i << "] = " << data[i].max << "\n";
		}
	}

	// returns min and max element in range [begin, end)
	// complexity: O(log size + k), where in this implementation always k = 2
	pair find_minmax(unsigned begin, unsigned end)
	{
		return find_minmax(0, size, begin, end, 0);
	}

private:
	unsigned get_middle_index(unsigned left, unsigned right)
	{
		return left + (right - left + 1) / 2;
	}

	void build(const T* source, unsigned begin, unsigned end, unsigned current)
	{
		if(begin + 1 == end)
		{
			data[current].min = source[begin];
			data[current].max = source[begin];
			return;
		}

		unsigned mid = get_middle_index(begin, end);
		build(source, begin, mid, current * 2 + 1);
		build(source, mid,   end, current * 2 + 2);

		data[current].min = min(data[current * 2 + 1].min, data[current * 2 + 2].min);
		data[current].max = max(data[current * 2 + 1].max, data[current * 2 + 2].max);
	}

	pair find_minmax(unsigned s_begin, unsigned s_end, unsigned q_begin, unsigned q_end, unsigned index)
	{
		if ( ! (s_begin < q_begin || q_end < s_end) )
			return data[index];

		if (s_end <= q_begin || q_end <= s_begin)
			return pair(
				std::numeric_limits<T>::max(),
				std::numeric_limits<T>::min()
			);

		unsigned mid = get_middle_index(s_begin, s_end);
		pair left, right;

		left  = find_minmax(s_begin, mid,   q_begin, q_end, index * 2 + 1);
		right = find_minmax(mid,     s_end, q_begin, q_end, index * 2 + 2);

		return pair(
			min(left.min, right.min),
			max(left.max, right.max)
		);
	}

	pair* data;
	unsigned size;
};

int main()
{
	int* data;
	unsigned count;

	std::cin >> count;
	data = new int[count];

	for(unsigned i = 0; i < count; i++)
		std::cin >> data[i];

	segment_tree<int> tree(data, count);

	std::cin >> count;
	unsigned begin, end;
	for(unsigned i = 0; i < count; i++)
	{
		std::cin >> begin >> end;
		auto p = tree.find_minmax(begin, end + 1);
		std::cout << p.min << ' ' << p.max << '\n';
	}

	delete[] data;
}
