// good examples
// http://www.algorithmist.com/index.php/Counting_sort
// https://en.wikipedia.org/wiki/Counting_sort

#include <iostream>

int get_int()
{
	int val = 0;
	char buf;
	
	do {
		buf = std::getchar();
		
		if(buf == ' ' or buf == '\n')
			return val;
		
		val *= 10;
		val += (buf - '0');
	} while(true);
}

struct pair{
	int key, value;
};

int main()
{
	int n = get_int();
	pair* a = new pair[n];
	
	for(int i=0; i<n; i++)
	{
		a[i].key   = get_int();
		a[i].value = get_int();
	}
	
	int test = std::getchar();
	{
		int* count = new int[n]{}; // { } means zero-initialization
			
		for(int i=0; i<n; i++)
			count[a[i].key]++; 
		
		if(test == '0')
		{
			for(int i=0; i<n; i++)
				for(int j=0; j<count[i]; j++)
					std::cout << i << '\n';
		}
		else if(test == '1')
		{
			int sum = 0;
			int old_sum;
			for(int i=0; i< n; i++)
			{
				old_sum = count[i];
				count[i] = sum;
				sum += old_sum;
			}
			
			pair* out = new pair[n];
			for(int i=0; i< n; i++)
			{
				out[count[a[i].key]].key   = a[i].key;
				out[count[a[i].key]].value = a[i].value;
				count[a[i].key]++;
			}
			
			for(int i=0; i<n; i++)
				std::cout << out[i].key << ',' << out[i].value << '\n';
			
			delete[] out;
		}
		
		delete[] count;
	}
	
	delete[] a;
}






















