#include <iostream>
#include <utility>
#include <limits>

template <class T>
void swap(T& a, T& b)
{
	T temp = a;
	a = b;
	b = temp;
}

template <class T, class Lambda>
class heap
{
public:
	heap(Lambda&& lambda): data(nullptr), is_max(true), count(0), allocated(0), compare(lambda) {}
	~heap() { clear(); }

	void build(const T* array, unsigned size)
	{
		clear();
		reserve(size);
		count = size;

		for(int i = 0; i < size; i++)
			data[i] = array[i];

		for(int i = size / 2; i >= 0; i--)
			repair_down(i);
	}

	void push(const T& value)
	{
		if(count < allocated)
		{
			data[count] = value;
			count++;
			repair_up(count - 1);
		}
		else
		{
			if(size() == 0)
				reserve(2);
			else
				reserve(size() * 2);

			push(value);
		}
	}

	void pop()
	{
		if(count > 0)
		{
			count--;
			data[0] = data[count];
			repair_down(0);
		}
	}

	T top() const
	{
		return data[0];
	}

	// not used in this program
	T& operator[](unsigned index)
	{
		return data[index];
	}

	// not used in this program
	const T& operator[](unsigned index) const
	{
		return data[index];
	}

	unsigned size() const
	{
		return count;
	}

	bool empty() const
	{
		return count == 0;
	}

	void clear()
	{
		if(data)
		{
			delete[] data;
			data = nullptr;
			count = 0;
			allocated = 0;
		}
	}

	void reserve(unsigned new_size)
	{
		T* new_array = new T[new_size];

		if(data)
		{
			for(unsigned i=0; i<count; i++)
				new_array[i] = data[i];

			delete[] data;
		}

		data = new_array;
		allocated = new_size;
	}

	void resize(unsigned new_count)
	{
		reserve(new_count);
		count = new_count;
	}

	// complexity: O(n)
	void reverse()
	{
		is_max = !is_max;

		if(count == 0)
			return;

		for(unsigned i = count - 1; i > 0; i--)
			repair_down(i);

		// can't loop until i >= 0 due to unsigned integer underflow
		repair_down(0);
	}

	void print() const
	{
		for(unsigned i=0; i<count; i++)
			std::cout << data[i] << ' ';
	}

	class iterator
	{
	public:
		iterator(T* ptr): ptr(ptr) {}
		iterator operator++() { ++ptr; return *this; }
		bool operator!=(const iterator& other) const { return ptr != other.ptr; }
		T& operator*() { return *ptr; }
	private:
		T* ptr;
	};

	iterator begin()
	{
		return iterator(data);
	}

	iterator end()
	{
		return iterator(data + size());
	}

	// force errors upon usage
	heap(const heap& other);
	heap& operator=(const heap& other);


	// complexity: O(log n)
	void repair_down(unsigned index)
	{
		unsigned left = 2 * index + 1;
		unsigned right = left + 1;
		unsigned best;

		if((left < count) && compare(data[index], data[left]))
			best = left;
		else
			best = index;

		if((right < count) && compare(data[best], data[right]))
			best = right;

		if(best != index)
		{
			swap(data[index], data[best]);
			repair_down(best);
		}
	}

	// complexity: O(log n)
	void repair_up(unsigned index)
	{
		while((index > 0) && compare(data[parent(index)], data[index]))
		{
			swap(data[parent(index)], data[index]);
			index = parent(index);
		}
	}

	// complexity: O(log n), O(n) worst case
	template<class Lambda2>
	T* find(const T& val, Lambda2 verify_func)
	{
		return find(0, val, verify_func);
	}

private:
	template<class Lambda2>
	T* find(unsigned index, const T& val, Lambda2 verify_func)
	{
		if(index >= count)
			return nullptr;

		if(compare(data[index], val))
			return nullptr;

		if(verify_func(data[index], val))
			return &data[index];

		T* result = find(index * 2 + 1, val, verify_func);
		if(result)
			return result;
		else
			return find(index * 2 + 2, val, verify_func);
	}

	unsigned parent(unsigned index) const
	{
		return (index - 1) / 2;
	}

	T* data;
	bool is_max;
	unsigned count;
	unsigned allocated;
	Lambda compare;
};

template<class T, class Lambda>
heap<T, Lambda> make_heap(Lambda&& t)
{
    // return { std::forward<Lambda>(t) };
	return heap<T, Lambda>(t);
}

template <class T>
class single_list
{
public:
	single_list(): head(nullptr) {}
	~single_list() { clear(); }

	// TODO: emplace_front()?
	void push_front(T value)
	{
		single_list_node* new_node = new single_list_node;
		new_node->data = value;
		new_node->next = head;

		head = new_node;
	}

	void pop_front()
	{
		if(head) // empty list error checking
		{
			single_list_node* temp = head;
			head = head->next;
			delete temp;
		}
	}

	void clear()
	{
		while(head)
			pop_front(); // code reuse
	}

	bool empty()
	{
		return head == nullptr;
	}

private:
	struct single_list_node
	{
		T data;
		single_list_node* next;
	};

	single_list_node* head;

public:
	class iterator
	{
	public:
		iterator(single_list_node* ptr): ptr(ptr) {}
		iterator operator++() { ptr = ptr->next; return *this; }
		bool operator!=(const iterator& other) const { return ptr != other.ptr; }
		T& operator*() { return ptr->data; }
	private:
		single_list_node* ptr;
	};

	iterator begin() const
	{
		return iterator(head);
	}

	iterator end() const
	{
		return iterator(nullptr);
	}
};


template <class T>
class vector
{
public:
	vector(): data(nullptr), count(0), allocated(0) {}
	vector(const char* letters): data(nullptr), count(0), allocated(0)
	{
		while(*letters != '\0')
		{
			push_back(*letters);
			letters++;
		}
	}

	vector(const vector<T>& other)
	{
		data = new T[other.size()];
		allocated = other.size();

		for(unsigned i=0; i<other.size(); i++)
			data[i] = other[i];

		count = other.size();
	}

	~vector() { clear(); }

	void push_back(const T& value)
	{
		if(count < allocated)
		{
			data[count] = value;
			count++;
		}
		else
		{
			if(size() == 0)
				reserve(2);
			else
				reserve(size() * 2);

			push_back(value);
		}
	}

	void insert(unsigned pos, const T& value)
	{
		if(count == allocated)
		{
			if(size() == 0)
				reserve(2);
			else
				reserve(size() * 2);
		}

		//move objects
		for(unsigned i = size(); i > pos; i--)
			data[i] = data[i - 1];

		data[pos] = value;
		count++;
	}

	void pop_back()
	{
		if(count > 0)
			count--;
	}

	T& operator[](unsigned index)
	{
		return data[index];
	}

	const T& operator[](unsigned index) const
	{
		return data[index];
	}

	void operator=(const vector<T>& other)
	{
		if(allocated < other.size())
		{
			if(data)
				delete[] data;

			data = new T[other.size()];
			allocated = other.size();
		}

		for(unsigned i=0; i<other.size(); i++)
			data[i] = other[i];

		count = other.size();
	}

	// based on std::lexicographical_compare
	// http://en.cppreference.com/w/cpp/algorithm
	bool operator<(const vector<T>& other) const
	{
		unsigned i;
		for(i=0; i<size(); i++)
		{
			if(data[i] < other.data[i])
				return true;
			else if(other.data[i] < data[i])
				return false;
		}

		return (i == size()) && (i != other.size());
	}

	bool operator>(const vector<T>& other) const
	{
		return other < *this; // avoid code duplication
	}

	unsigned size() const
	{
		return count;
	}

	bool empty() const
	{
		return count == 0;
	}

	void clear()
	{
		if(data)
		{
			delete[] data;
			data = nullptr;
			count = 0;
			allocated = 0;
		}
	}

	void reserve(unsigned new_size)
	{
		T* new_array = new T[new_size];

		if(data)
		{
			for(unsigned i=0; i<count; i++)
				new_array[i] = data[i];

			delete[] data;
		}

		data = new_array;
		allocated = new_size;
	}

	void resize(unsigned new_count)
	{
		reserve(new_count);
		count = new_count;
	}

	void print() const
	{
		for(unsigned i=0; i<count; i++)
			std::cout << data[i];
	}

	void erase(unsigned index)
	{
		count--;

		for(unsigned i = index; i < count; i++)
			data[i] = data[i + 1];
	}

	template<class Lambda>
	unsigned find(Lambda compare)
	{
		unsigned result = 0;

		for(unsigned i = 1; i < count; i++)
			if(compare(data[i], data[result]))
				result = i;

		return result;
	}

private:
	T* data;
	unsigned count;
	unsigned allocated;
};

enum /* class */ village_type // : char
{
	normal,
	bush_seller,
	black_night_camp,
	ni_night_camp,
	arthurs_castle,
	holy_grail
};

struct road
{
	unsigned target_index;
	unsigned time_to_travel;
};

struct village
{
	village_type type;
	single_list<road> adjacent;
	unsigned dist_to_me;
	unsigned prev_index; // previous node in shortest path to this node
	unsigned to_bush_seller_index; // which road takes to bush seller
	unsigned time_to_reach_bush_seller;
	bool reviewed;
};

unsigned dijkstra(village* villages, unsigned v_size, unsigned source_index, village_type target_type, unsigned time_to_beat_black_night)
{
	// setup tentative values
	for(unsigned i = 0; i < v_size; i++)
	{
		villages[i].dist_to_me = std::numeric_limits<unsigned>::max();
		villages[i].prev_index = std::numeric_limits<unsigned>::max();
		villages[i].reviewed = false;
	}

	villages[source_index].dist_to_me = 0;

	// compare roads by their distance
	auto sort_by_dist = [](const road& left, const road& right) -> bool
	{
		return left.time_to_travel > right.time_to_travel;
	};

	// make heap of vertexes
	auto heap = make_heap<road>(sort_by_dist);
	heap.resize(v_size);
	for(unsigned i = 0; i < v_size; i++)
	{
		heap[i].target_index = i;
		heap[i].time_to_travel = villages[i].dist_to_me;
	}
	heap.repair_down(0);

	unsigned return_distance;
	unsigned target_index;

	while(!heap.empty())
	{
		road u = heap.top();
		heap.pop();

		// target has been found
		if(villages[u.target_index].type == target_type)
		{
			target_index = u.target_index;
			return_distance = u.time_to_travel;
			break;
		}

		villages[u.target_index].reviewed = true;

		// find all neighbours of u - they are in the list of adjacent villages
		for(auto it = villages[u.target_index].adjacent.begin(); it != villages[u.target_index].adjacent.end(); ++it)
		{
			unsigned neighbour_index = (*it).target_index;

			// don't check already reviewed vertexes
			if(villages[neighbour_index].reviewed)
				break;

			unsigned alternate_dist = u.time_to_travel + (*it).time_to_travel;
			if(villages[neighbour_index].type == village_type::black_night_camp)
				alternate_dist += time_to_beat_black_night;

			if (villages[neighbour_index].dist_to_me > alternate_dist)
			{
				//std::cout << "updating: " << villages[neighbour_index].dist_to_me << "\n";
				villages[neighbour_index].dist_to_me = alternate_dist;
				villages[neighbour_index].prev_index = u.target_index;

				// decrease distance for neighbour in heap
				// but first find that neighbour
				for(unsigned i = 0; i < heap.size(); i++)
					if(heap[i].target_index == neighbour_index)
					{
						// update dist - requires repairing
						//std::cout << "updating: " << heap[i].time_to_travel << "\n\n";
						heap[i].time_to_travel = alternate_dist;
						heap.repair_up(i);
						break;
					}
			}
		}
	}

	single_list<unsigned> path;
	while(villages[target_index].prev_index != std::numeric_limits<unsigned>::max())
	{
		path.push_front(target_index);
		target_index = villages[target_index].prev_index;
	}
	path.push_front(target_index);

	// print path
	for(auto it = path.begin(); it != path.end(); ++it)
		std::cout << *it << ' ';

	return return_distance;
}

int main()
{
	unsigned villages_amount, time_to_beat_black_night;
	std::cin >> villages_amount >> time_to_beat_black_night;

	village* p = new village[villages_amount];

	unsigned source_index;

	for(unsigned i = 0; i < villages_amount; i++)
	{
		unsigned t;
		std::cin >> t;
		p[i].type = static_cast<village_type>(t);

		if(p[i].type == village_type::arthurs_castle)
			source_index = i;

//		if(p[i].type == village_type::holy_grail)
//				target_index = i;

		unsigned roads_amount;
		std::cin >> roads_amount;

		road r;
		for(unsigned j = 0; j < roads_amount; j++)
		{
			std::cin >> r.target_index;
			std::cin >> r.time_to_travel;
			p[i].adjacent.push_front(r);
		}
	}

	dijkstra(p, villages_amount, source_index, village_type::holy_grail, time_to_beat_black_night);
	std::cin >> std::ws;
	std::cin >> villages_amount;

	delete[] p;
}























