#include <iostream>
#include <vector>

const unsigned MAX_WORD_SIZE = 16 + 1;

template <class T>
class vector
{
public:
	vector(): data(nullptr), count(0), allocated(0) {}

	vector(const char* letters): data(nullptr)
	{
		resize(MAX_WORD_SIZE);
		for(unsigned i=0; i<MAX_WORD_SIZE; i++)
		{
			if(letters[i] != '\0')
			{
				data[i] = letters[i];
			}
			else
			{
				count = i;
				break;
			}
		}
	}

	vector(const vector<T>& other)
	{
		data = new T[other.size()];
		allocated = other.size();

		for(unsigned i=0; i<other.size(); i++)
			data[i] = other[i];

		count = other.size();
	}

	~vector() { clear(); }

	void push_back(const T& value)
	{
		if(count < allocated)
		{
			data[count] = value;
			count++;
		}
		else
		{
			if(size() == 0)
				resize(2);
			else
				resize(size() * 2);

			push_back(value);
		}
	}

	void insert(unsigned pos, const T& value)
	{
		if(count == allocated)
		{
			if(size() == 0)
				resize(2);
			else
				resize(size() * 2);
		}

		//move objects
		for(unsigned i = size(); i > pos; i--)
			data[i] = data[i - 1];

		data[pos] = value;
		count++;
	}

	void pop_back()
	{
		if(count > 0)
			count--;
	}

	T& operator[](unsigned index)
	{
		return data[index];
	}

	const T& operator[](unsigned index) const
	{
		return data[index];
	}

	void operator=(const vector<T>& other)
	{
		if(allocated < other.size())
		{
			if(data)
				delete[] data;

			data = new T[other.size()];
			allocated = other.size();
		}

		for(unsigned i=0; i<other.size(); i++)
			data[i] = other[i];

		count = other.size();
	}

	unsigned size() const
	{
		return count;
	}

	bool empty() const
	{
		return count == 0;
	}

	void clear()
	{
		if(data)
		{
			delete[] data;
			data = nullptr;
			count = 0;
			allocated = 0;
		}
	}

	void resize(unsigned new_size)
	{
		T* new_array = new T[new_size];

		if(data)
		{
			for(unsigned i=0; i<count; i++)
				new_array[i] = data[i];

			delete[] data;
		}

		data = new_array;
		allocated = new_size;
	}

	void print() const
	{
		for(unsigned i=0; i<count; i++)
			std::cout << data[i];
	}


private:
	T* data;
	unsigned count;
	unsigned allocated;
};

class node
{
public:
	node(): letter('\0') {}
	node(char letter): letter(letter) {}
	node(char letter, const vector<char>& transl): letter(letter) { translation = transl; }
	~node() { clear(); }

	void clear()
	{
		for(unsigned i=0; i<children.size(); i++)
			delete children[i];

		children.clear();
	}

	node* get_child(char c) const
	{
		for(unsigned i=0; i<children.size(); i++)
			if(children[i]->letter == c)
				return children[i];

		return nullptr;
	}

	node* add_child(char c)
	{
		// find first higher character
		unsigned i;
		for(i = 0; i < children.size(); i++)
		{
			if(children[i]->letter > c)
				break;
		}

		node* temp = new node(c);
		children.insert(i, temp);
		return temp;
	}

public:
	char letter;
	vector<node*> children;
	vector<char> translation;
};

class trie
{
public:
	void add_translation(const vector<char>& word, const vector<char>& translation)
	{
		node* p = &root;
		node* temp;

		for(unsigned pos = 0; pos < word.size(); pos++)
		{
			temp = p->get_child(word[pos]);

			if(temp == nullptr)
				p = p->add_child(word[pos]);
			else
				p = temp;
		}

		p->translation = translation;
	}

	vector<char> get_translation(const vector<char>& word)
	{
		node* p = &root;

		for(unsigned pos = 0; pos < word.size(); pos++)
		{
			p = p->get_child(word[pos]);

			if(p == nullptr)
				return vector<char>("-");
		}

		if(p->translation.empty())
			return vector<char>("-");
		else
			return p->translation;
	}

	void print_all_translations(const vector<char>& prefix)
	{
		node* p = &root;

		for(unsigned pos = 0; pos < prefix.size(); pos++)
		{
			p = p->get_child(prefix[pos]);

			if(p == nullptr)
			{
				std::cout << "-\n";
				return;
			}
		}

		bool found = false;
		print_all_children(p, found);
		if(found == false)
			std::cout << "-\n";
	}

	void clear()
	{
		root.clear();
	}

private:
	void print_all_children(node* p, bool& found)
	{
		if(!p->translation.empty())
		{
			found = true;
			p->translation.print();
			std::cout << "\n";
		}

		for(unsigned i=0; i < p->children.size(); i++)
			print_all_children(p->children[i], found);
	}

	node root;
};

int main()
{
	trie dictionary;

	char buf;
	char word[MAX_WORD_SIZE];

	while(std::cin >> buf)
	{
		std::cin >> std::ws;
		std::cin >> word;

		if(buf == '+')
		{
			std::cin >> std::ws;
			char translation[MAX_WORD_SIZE];
			std::cin >> translation;

			dictionary.add_translation(word, translation);
		}
		else if(buf == '?')
		{
			dictionary.get_translation(word).print();
			std::cout << '\n';
		}
		else if(buf == '*')
		{
			dictionary.print_all_translations(word);
		}
		else if(buf == 'q')
			break;
	}
}











