Poszczególne zadania można zrobić na różne sposoby
___
**Trie**

Są 2 zasadnicze sposoby na punkty prostego trie: (jest więcej, ale to wariacje tych dwóch)

1.
```
#!c++
struct node {
    bool end;
    node* children[MAX_LENGTH];
    char translation[MAX_LENGTH];
};
```
- node posiada tablicę o stałej długości na dzieci (domyślnie same nulle)
- sam node nie wie jaką jest literą - taką informację może jedynie zdeterminować rodzic noda na podstawie która pozycja w tablicy children nie jest adrezem zerowym (to może trochę utrudnić szukanie/dodawania nowych elementów do drzewa)
- jeśli node chce sprawdzić, czy posiada dziecko o określonej literze `letter`, musi tylko sprawdzić czy `children[letter - 'a'] != nullptr`
- jeśli node chce wypisać swoje wszystkie dzieci, musi odbyć pętle prez cała tablicę `children` (dokładnie `MAX_LENGTH` iteracji)

2.
```
#!c++
struct node {
    char letter;
    vector<node*> children;
    vector<char> translation;
};
```
- node posiada dynamiczne tablice na dzieci i tlumaczenie
- node wie jaką jest literą
- jeśli node chce wiedzieć, czy nie jest kończem jakiegoś słowa, wystarczy sprawdzić, czy `translation.empty()`
- jeśli node chce sprawdzić, czy posiada dziecko o określonej literze `letter`, musi przejść przez całą tablicę dzieci (tj `children.size()` iteracji)
- jeśli node chce wypisać swoje wszystkie dzieci, musi odbyc pętlę przez `children` (ale tylko `children.size()` iteracji)

Ogólnie podsumowując:

- rozwiązanie 1 nie ma pętli pzy szukaniu dziecka o danej literze, ale...
- rozwiązanie 1 wymaga dodatkowej logiki związanej z faktem, że to rodzic zna literę dziecka, a nie ono samo
- rozwiązanie 2 szybciej odnajduje wszystkie dzieci, bo wykonuje tylko tyle iteracji, jaki jest rozmiar tablicy dynamicznej
- rozwiązanie 2 wymaga implementacji tablicy dynamicznej
- rozwiązanie 2 ze względu na tablicę dynamiczną (zamiast statycznej o możliwie największym rozmiarze) zajmuje znacząco mniejszą ilość pamięci