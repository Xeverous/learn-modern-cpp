#include <iostream>

template <class T>
void swap(T& a, T& b)
{
	T temp = a;
	a = b;
	b = temp;
}

template <class T>
class heap
{
public:
	heap(): data(nullptr), is_max(true), count(0), allocated(0) {}
	~heap() { clear(); }

	void build(const T* array, unsigned size)
	{
		clear();
		resize(size);
		count = size;

		for(int i = 0; i < size; i++)
			data[i] = array[i];

		for(int i = size / 2; i >= 0; i--)
			repair_down(i);
	}

	void push(const T& value)
	{
		if(count < allocated)
		{
			data[count] = value;
			count++;
			repair_up(count - 1);
		}
		else
		{
			if(size() == 0)
				resize(2);
			else
				resize(size() * 2);

			push(value);
		}
	}

	void pop()
	{
		if(count > 0)
		{
			count--;
			data[0] = data[count];
			repair_down(0);
		}
	}

	T top() const
	{
		return data[0];
	}

	// not used in this program
	T& operator[](unsigned index)
	{
		return data[index];
	}

	// not used in this program
	const T& operator[](unsigned index) const
	{
		return data[index];
	}

	unsigned size() const
	{
		return count;
	}

	bool empty() const
	{
		return count == 0;
	}

	void clear()
	{
		if(data)
		{
			delete[] data;
			data = nullptr;
			count = 0;
			allocated = 0;
		}
	}

	void resize(unsigned new_size)
	{
		T* new_array = new T[new_size];

		if(data)
		{
			for(unsigned i=0; i<count; i++)
				new_array[i] = data[i];

			delete[] data;
		}

		data = new_array;
		allocated = new_size;
	}

	// complexity: O(n)
	void reverse()
	{
		is_max = !is_max;

		if(count == 0)
			return;

		for(unsigned i = count - 1; i > 0; i--)
		{
			print();
			std::cout << "\n";
			repair_down(i);
		}

		// can't loop until i >= 0 due to unsigned integer underflow
		repair_down(0);
	}

	void print() const
	{
		for(unsigned i=0; i<count; i++)
			std::cout << data[i] << ' ';
	}

private:
	bool compare(const T& left, const T& right) const
	{
		if(is_max)
			return left < right;
		else
			return right < left;
	}

	// complexity: O(log n)
	void repair_down(unsigned index)
	{
		std::cout << "repairing down for [" << index << "] = " << data[index] << "\n";

		unsigned left = 2 * index + 1;
		unsigned right = left + 1;
		unsigned best;

		if((left < count) && compare(data[index], data[left]))
			best = left;
		else
			best = index;

		if((right < count) && compare(data[best], data[right]))
			best = right;

		if(best != index)
		{
			swap(data[index], data[best]);
			repair_down(best);
		}
	}

	// complexity: O(log n)
	void repair_up(unsigned index)
	{
		std::cout << "repairing up   for [" << index << "] = " << data[index] << "\n";

		while((index > 0) && compare(data[parent(index)], data[index]))
		{
			swap(data[parent(index)], data[index]);
			index = parent(index);
		}
	}

	unsigned parent(unsigned index) const
	{
		return (index - 1) / 2;
	}

	T* data;
	bool is_max;
	unsigned count;
	unsigned allocated;
};

int main()
{
	heap<int> h;

	std::cout << "filling heap\n";

	// some random-like insertions
	for(int i : {10, 5, 6, 16, 7, 0, 2, 4, 8, 1, 3})
		h.push(i);

	std::cout << "\n";

	std::cout << "maximized:\n";
	h.print();
	std::cout << "\n\n";

	std::cout << "reversing:\n";
	h.reverse();
	std::cout << "\n";

	std::cout << "minimized:\n";
	h.print();
	std::cout << "\n";

	std::cin.get();
}














