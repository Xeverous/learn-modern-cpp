#include <iostream>
#include <cstdio>

template <class T>
struct single_list_node {
	T data;
	single_list_node<T>* next;
};

template<class T>
class circular_list
{
public:
	circular_list(): head(nullptr), tail(nullptr) {}
	~circular_list() { clear(); }

	void clear()
	{
		if(empty())
			return;

		if(head == tail)
		{
			delete head;
			head = nullptr;
			tail = nullptr;
			return;
		}

		single_list_node<T>* temp1 = head;
		single_list_node<T>* temp2;

		do
		{
			temp2 = temp1->next;
			delete temp1;
			temp1 = temp2;
		} while(temp1 != head);
	}

	void push_back(const T& value)
	{
		single_list_node<T>* new_node = new single_list_node<T>;
		new_node->data = value;

		if(head == nullptr)
		{
			new_node->next = new_node;
			head = new_node;
			tail = new_node;
		}
		else
		{
			new_node->next = head;
			tail->next = new_node;
			tail = new_node;
		}
	}

	void erase_after(single_list_node<T>* pos)
	{
		if(head == nullptr)
			return;

		if(head == tail && pos == head)
		{
			delete head;
			head = nullptr;
			tail = nullptr;
			return;
		}

		single_list_node<T>* temp = pos->next->next;

		if(pos == tail)
			head = temp;
		else if(pos->next == tail)
			tail = pos;

		delete pos->next;
		pos->next = temp;
	}

	bool empty()
	{
		return head == nullptr;
	}

	void advance()
	{
		if(head == nullptr)
			return;

		tail = head;
		head = head->next;
	}

	single_list_node<T>* find_before(const T& data)
	{
		if(empty())
			return nullptr;

		single_list_node<T>* temp1 = head;
		single_list_node<T>* temp2 = tail;

		do
		{
			if(temp1->data == data)
				return temp2;

			temp1 = temp1->next;
			temp2 = temp2->next;
		} while(temp1 != head);

		return nullptr;
	}

	single_list_node<T>* begin()
	{
		return head;
	}

	void print()
	{
		if(empty())
			return;

		single_list_node<T>* temp = head;

		do
		{
			std::cout << temp->data << ' ';
			temp = temp->next;
		} while(temp != head);
	}

private:
	single_list_node<T>* head;
	single_list_node<T>* tail;
};

//constexpr int LISTS_COUNT = 3; // constexpr not supported
const int LISTS_COUNT = 3;

template<class T>
void delete_from_list(circular_list<T> lists[], const T& val)
{
	single_list_node<T>* target;

	for(int i=0; i<LISTS_COUNT; i++)
	{
		if((target = lists[i].find_before(val)) != nullptr)
		{
			lists[i].erase_after(target);
			return;
		}
	}
}

int main()
{
	char buf;
	circular_list<unsigned> lists[LISTS_COUNT];

	while(std::cin >> buf)
	{
		if (buf == 'n')
		{
			bool found = false;
			for(int i=LISTS_COUNT - 1; i >= 0; i--)
			{
				if(!lists[i].empty())
				{
					std::cout << lists[i].begin()->data << '\n';
					lists[i].advance();
					found = true;
					break;
				}
			}

			if(!found)
				std::cout << "idle\n";
		}
		else if (buf == 'c')
		{
			std::cin >> std::ws;
			unsigned id;
			std::cin >> id;
			int index;
			std::cin >> index;
			index++;

			lists[index].push_back(id);
		}
		else if (buf == 't')
		{
			std::cin >> std::ws; // ignore space
			unsigned id;
			std::cin >> id;
			delete_from_list<unsigned>(lists, id);
		}
		else if (buf == 'p')
		{
			std::cin >> std::ws; // ignore space
			int id;
			std::cin >> id;
			delete_from_list<unsigned>(lists, id);

			unsigned index;
			std::cin >> index;
			index++;

			lists[index].push_back(id);
		}
		else if (buf == 'l')
		{
			for(int i=LISTS_COUNT-1; i >= 0; i--)
			{
				std::cout << i-1 << ": ";
				lists[i].print();
				std::cout << '\n';
			}
		}
	}
}
