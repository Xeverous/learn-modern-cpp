#include <iostream>

const unsigned MAX_WORD_SIZE = 101;

template <class T>
class vector
{
public:
	vector(): data(nullptr), count(0), allocated(0) {}
	vector(const char* letters): data(nullptr), count(0), allocated(0)
	{
		while(*letters != '\0')
		{
			push_back(*letters);
			letters++;
		}
	}

	vector(const vector<T>& other)
	{
		data = new T[other.size()];
		allocated = other.size();

		for(unsigned i=0; i<other.size(); i++)
			data[i] = other[i];

		count = other.size();
	}

	~vector() { clear(); }

	void push_back(const T& value)
	{
		if(count < allocated)
		{
			data[count] = value;
			count++;
		}
		else
		{
			if(size() == 0)
				resize(2);
			else
				resize(size() * 2);

			push_back(value);
		}
	}
	
	void insert(unsigned pos, const T& value)
	{
		if(count == allocated)
		{
			if(size() == 0)
				resize(2);
			else
				resize(size() * 2);
		}

		//move objects
		for(unsigned i = size(); i > pos; i--)
			data[i] = data[i - 1];

		data[pos] = value;
		count++;
	}

	void pop_back()
	{
		if(count > 0)
			count--;
	}

	T& operator[](unsigned index)
	{
		return data[index];
	}

	const T& operator[](unsigned index) const
	{
		return data[index];
	}

	void operator=(const vector<T>& other)
	{
		if(allocated < other.size())
		{
			if(data)
				delete[] data;

			data = new T[other.size()];
			allocated = other.size();
		}

		for(unsigned i=0; i<other.size(); i++)
			data[i] = other[i];

		count = other.size();
	}

	// based on std::lexicographical_compare
	// http://en.cppreference.com/w/cpp/algorithm
	bool operator<(const vector<T>& other) const
	{
		unsigned i;
		for(i=0; i<size(); i++)
		{
			if(data[i] < other.data[i])
				return true;
			else if(other.data[i] < data[i])
				return false;
		}

		return (i == size()) && (i != other.size());
	}

	bool operator>(const vector<T>& other) const
	{
		return other < *this; // avoid code duplication
	}

	unsigned size() const
	{
		return count;
	}

	bool empty() const
	{
		return count == 0;
	}

	void clear()
	{
		if(data)
		{
			delete[] data;
			data = nullptr;
			count = 0;
			allocated = 0;
		}
	}

	void resize(unsigned new_size)
	{
		T* new_array = new T[new_size];

		if(data)
		{
			for(unsigned i=0; i<count; i++)
				new_array[i] = data[i];

			delete[] data;
		}

		data = new_array;
		allocated = new_size;
	}

	void print() const
	{
		for(unsigned i=0; i<count; i++)
			std::cout << data[i];
	}


private:
	T* data;
	unsigned count;
	unsigned allocated;
};

const char t9_indexes[] = {
	'2', '2', '2',      // a b c
	'3', '3', '3',      // d e f
	'4', '4', '4',      // g h i
	'5', '5', '5',      // j k l
	'6', '6', '6',      // m n o
	'7', '7', '7', '7', // p q r s
	'8', '8', '8',      // t u v
	'9', '9', '9', '9'  // w x y z
};

char get_t9_index(char c)
{
	return t9_indexes[c - 'a'];
}

class node
{
public:
	node(): letter('\0') {}
	node(char letter): letter(letter) { std::cout << "constructing new node with index: " << letter << "\n"; }
	~node() {}

	void clear()
	{
		for(unsigned i=0; i<children.size(); i++)
			delete children[i];

		children.clear();
	}

	node* get_child(char c) const
	{
		for(unsigned i=0; i<children.size(); i++)
			if(children[i]->letter == c)
				return children[i];

		return nullptr;
	}

	node* add_child(char c)
	{
		// find first higher character
		unsigned i;
		for(i = 0; i < children.size(); i++)
		{
			if(children[i]->letter > c)
				break;
		}

		node* temp = new node(c);
		children.insert(i, temp);
		return temp;
	}

	char get_letter() const
	{
		return letter;
	}

	void add_word(const vector<char>& word)
	{
		// find first higher word
		unsigned i;
		for(i = 0; i < words.size(); i++)
		{
			if(words[i] > word) // will call vector<char>::operator<
				break;
		}

		words.insert(i, word);
	}

public:
	char letter;
	vector<node*> children;
	vector<vector<char>> words;
};

class t9
{
public:
	void add_word(const vector<char>& word)
	{
		node* p = &root;
		node* temp;
		std::cout << "adding word with size: " << word.size() << "\n";
		for(unsigned pos = 0; pos < word.size(); pos++)
		{
			temp = p->get_child(get_t9_index(word[pos]));

			if(temp == nullptr)
				p = p->add_child(get_t9_index(word[pos]));
			else
			{
				std::cout << "moving to child node  with index: " << get_t9_index(word[pos]) << "\n";
				p = temp;
			}
		}

		p->add_word(word);
	}

	void print_all_words(const vector<char>& prefix)
	{
		node* p = &root;
		node* temp;
		unsigned pos;
		for(pos = 0; pos < prefix.size(); pos++)
		{
			std::cout << "on letter: " << prefix[pos] << "\n";

			temp = p->get_child(prefix[pos]);

			if(temp == nullptr)
			{
				std::cout << "- no such child -\n";
				return;
			}
			else
			{
				p = temp;
			}
		}

		bool found = false;
		print_all_children(p, found);
		if(found)
			std::cout << "\n";
		else
			std::cout << "-\n";
	}

	void clear()
	{
		root.clear();
	}

private:
	void print_all_children(node* p, bool& found)
	{
		if(!p->words.empty())
		{
			found = true;
			for(unsigned i=0; i<p->words.size(); i++)
			{
				p->words[i].print();
				std::cout << ' ';
			}
		}

		for(unsigned i=0; i < p->children.size(); i++)
			print_all_children(p->children[i], found);
	}

	node root;
};

int main()
{
	t9 dictionary;

	int commands;
	char word[MAX_WORD_SIZE];

	for(std::cin >> commands; commands > 0; commands--)
	{
		std::cout << "add word: " << commands << "\n";
		std::cin >> word;
		dictionary.add_word(word);
	}

	for(std::cin >> commands; commands > 0; commands--)
	{
		std::cout << "ask for word: " << commands << "\n";
		std::cin >> word;
		dictionary.print_all_words(word);
	}
}
















