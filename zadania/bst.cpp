#include <iostream>

template<class T>
const T& min(const T& a, const T& b)
{
    return (b < a) ? b : a;
}

template<class T>
const T& max(const T& a, const T& b)
{
    return (a < b) ? b : a;
}

template<class T>
class binary_search_tree
{
public:
	binary_search_tree(): root(nullptr) {}
	~binary_search_tree()
	{
		if(root)
			delete root;
	}

	struct Node
	{
		T value;
		Node* left;
		Node* right;

		Node(const T& val)
			: value(val), left(nullptr), right(nullptr)
		{
		}

		~Node()
		{
			if(left)
				delete left;

			if(right)
				delete right;
		}
	};

private:
	Node* root;

	void add(Node* node, const T& val)
	{
		if (val < node->value)
		{
			if (node->left)
				add(node->left, val);
			else
				node->left = new Node(val);
		}
		else
		{
			if (node->right)
				add(node->right, val);
			else
				node->right = new Node(val);
		}
	}

	unsigned get_nodes_count(Node* node)
	{
		if (node)
			return 1 + get_nodes_count(node->left) + get_nodes_count(node->right);
		else
			return 0;
	}

	unsigned get_height(Node* node)
	{
		if (node)
			return 1 + max(get_height(node->left), get_height(node->right));
		else
			return 0;
	}

	void print_longest_path(Node* node)
	{
		if (node == nullptr)
			return;

		std::cout << node->value << ' ';
		if (get_height(node->left) > get_height(node->right))
			print_longest_path(node->left);
		else
			print_longest_path(node->right);
	}

	bool erase(Node* parent, Node* current, const T& value)
	{
		if (current == nullptr)
			return false;

		if (current->value == value)
		{
			if (current->left == nullptr || current->right == nullptr)
			{
				Node* temp = current->left;
				if (current->right)
					temp = current->right;

				if (parent)
				{
					if (parent->left == current)
						parent->left = temp;
					else
						parent->right = temp;
				}
				else
				{
					root = temp;
				}
			}
			else
			{
				Node* validSubs = current->right;

				while (validSubs->left)
					validSubs = validSubs->left;

				T temp = current->value;
				current->value = validSubs->value;
				validSubs->value = temp;

				return erase(current, current->right, temp);
			}

			delete current;
			return true;
		}

		return erase(current, current->left,  value) ||
		       erase(current, current->right, value);
	}

public:
	void add(const T& val)
	{
		if (root)
			add(root, val);
		else
			root = new Node(val);
	}

	void print(bool start)
	{
		state = start;
		print(root);
	}

	unsigned get_nodes_count()
	{
		return getnodes_count(root);
	}

	unsigned get_height()
	{
		return get_height(root);
	}

	void print_longest_path()
	{
		print_longest_path(root);
	}

	bool erase(const T& value)
	{
		return erase(nullptr, root, value);
	}

	T* get_min()
	{
		if(root == nullptr)
			return nullptr;

		Node* p = root;

		while(p->left != nullptr)
			p = p->left;

		return &(p->value);
	}

	T* get_max()
	{
		if(root == nullptr)
			return nullptr;

		Node* p = root;

		while(p->right != nullptr)
			p = p->right;

		return &(p->value);
	}

private:
	bool state;

	void print(Node* node)
	{
		if (node == nullptr)
			return;

		print(node->left);

		if(state)
			std::cout << node->value.data << ' ';

		state = !state;

		print(node->right);
	}

//	template <class lambda>
//	void on_every_second_element(Node* node, lambda func)
//	{
//		if(node == nullptr)
//			return;
//
//		on_every_second_element(node->left, func);
//
//		if(state)
//			std::cout << node->value.data << ' ';
//
//		state = !state;
//
//		on_every_second_element(node->left, func);
//	}
//
//public:
//	template <class lambda>
//	void on_every_second_element(bool start, lambda func)
//	{
//		state = start;
//		on_every_second_element(root, func);
//	}
};



struct word
{
	// constexpr static unsigned maxlength() { return 17; }
	static const unsigned max_length = 17;

	char data[max_length];

	word()
	{
		data[0] = '\0';
	}

	word(const word& other)
	{
		for(unsigned i = 0; i < max_length; i++)
			data[i] = other.data[i];
	}

	bool operator<(const word& other) const
	{
		for(unsigned i = 0; i < max_length; i++)
		{
			if(data[i] < other.data[i])
				return true;
			else if(other.data[i] < data[i])
				return false;
		}

		return false;
	}
};

int main()
{
	binary_search_tree<word> bst;
	char command;

//	auto lambda_print = [](const word& w)
//	{
//		std::cout << w.data << ' ';
//	};

	while(std::cin >> command)
	{
		if(command == '+')
		{
			word w;
			std::cin >> w.data;

			bst.add(w);
		}
		else if(command == '<')
		{
			std::cout << bst.get_min()->data << '\n';
		}
		else if(command == '>')
		{
			std::cout << bst.get_max()->data << '\n';
		}
		else if(command == 'h')
		{
			std::cout << bst.get_height() << '\n';
		}
		else if(command == '1')
		{
			bool start = true;
			//bst.on_every_second_element(start, lambda_print);
			bst.print(true);
			std::cout << '\n';
		}
		else if(command == '2')
		{
			bool start = false;
			bst.print(false);
			//bst.on_every_second_element(start, lambda_print);
			std::cout << '\n';
		}
		else if(command == 'p')
		{
			//bst.print();
			std::cout << '\n';
		}
	}
}






