#include <iostream>

struct Item
{
	unsigned weight;
	unsigned value;
};

template<class T>
const T& min(const T& a, const T& b)
{
    return (b < a) ? b : a;
}

template<class T>
const T& max(const T& a, const T& b)
{
    return (a < b) ? b : a;
}

int main()
{
	unsigned capacity;
	unsigned n;

	std::cin >> capacity;
	std::cin >> n;

	Item* items = new Item[n];
	for(unsigned i = 0; i<n; i++)
	{
		std::cin >> items[i].weight;
		std::cin >> items[i].value;
	}

	unsigned size_x = capacity + 1;
	unsigned size_y = n + 1;
	unsigned* max_values = new unsigned[size_y * size_x];

	// a lambda expression converting 2D index to 1D
	auto Index = [&size_y](unsigned x, unsigned y)
	{
		return size_y * y + x;
	};

	// fill 0 for any 0 capacity
	for(unsigned i = 0; i < size_y; i++)
		max_values[Index(i, 0)] = 0;

	// fill 0 for any 0 items
	for(unsigned i = 0; i < size_x; i++)
		max_values[Index(0, i)] = 0;

	for(unsigned i = 1; i < size_y; i++)
		for(unsigned j = 1; j < size_x; j++)
		{
			if(items[i - 1].weight <= j)
				max_values[Index(i, j)] = max
				(
					max_values[Index(i - 1, j - items[i - 1].weight)] + items[i - 1].value,
					max_values[Index(i - 1, j                      )]
				);
			else
				max_values[Index(i, j)] = max_values[Index(i - 1, j)];
		}

	// backtrack for solution
	unsigned* solutions = new unsigned[n];
	unsigned solution_size = 0;
	unsigned max_value = 0;

	for(unsigned i = n, j = capacity; i > 0; i--)
		if(max_values[Index(i, j)] > max_values[Index(i - 1, j)])
		{
			solutions[solution_size] = i;
			solution_size++;

			j -= items[i - 1].weight;
			max_value += items[i - 1].value;
		}

	// print solution in order of items
	std::cout << max_value << "\n";

	for(unsigned i = solution_size; i > 0; i--)
		std::cout << solutions[i - 1] << " ";

	delete[] solutions;
	delete[] max_values;
	delete[] items;
}
