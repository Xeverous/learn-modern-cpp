#include <iostream>
#include <string>
#include <algorithm>
#include <fstream>
#include <iomanip>
#include <map>
#include <experimental/filesystem>

// C++17 fold expressions
template<typename... Args>
void fold_cout(Args&&... args)
{
	if constexpr (sizeof...(args) > 0)
		((std::cout << args << ' '), ...); // space between arguments
	else
		((std::cout << args) << ...) << '\n'; // new line after last argument
}

template<typename... Args>
auto fold_sum(Args&&... args)
{
    return (... + args);
}

namespace fs = std::experimental::filesystem;

int main()
{
	std::cout << "Fold expressions functions:\n";
	fold_cout("abc", 5, 3.14, 'a', std::string("x"), fold_sum(1, 2, 3, 4, 5));

	std::cout << "\nCurrent program path is: ";
	// wcout - native paths for Windows use wchar_t, otherwise you will get template errors about mismatched ostream<char> and string_type<wchar_t>
	std::wcout << fs::current_path().native() << '\n';
	std::cout << "Checking if windows kernel path exists: " << std::boolalpha << fs::exists(fs::path("C:") / "Windows" / "system32" / "kernel32.dll") << '\n';

	fs::path path(u8"sandbox");
	fs::create_directories(path / "a" / "b");
	std::ofstream("sandbox/file1.txt");
	std::ofstream("sandbox/file2.txt");

	unsigned elements = 0;
	for (auto& p : fs::recursive_directory_iterator(path))
	{
		std::cout << p << '\n';
		elements++;
	}

	std::cout << "Number of objects in directory sandbox: " << elements << '\n';

	fs::perms p = fs::status(path).permissions();
	std::cout << "Permissions of the " << path << " directory: ";
	std::cout << ((p & fs::perms::owner_read)   != fs::perms::none ? 'r' : '-')
	          << ((p & fs::perms::owner_write)  != fs::perms::none ? 'w' : '-')
	          << ((p & fs::perms::owner_exec)   != fs::perms::none ? 'x' : '-')
	          << ((p & fs::perms::group_write)  != fs::perms::none ? 'w' : '-')
	          << ((p & fs::perms::group_read)   != fs::perms::none ? 'r' : '-')
	          << ((p & fs::perms::group_exec)   != fs::perms::none ? 'x' : '-')
	          << ((p & fs::perms::others_read)  != fs::perms::none ? 'r' : '-')
	          << ((p & fs::perms::others_write) != fs::perms::none ? 'w' : '-')
	          << ((p & fs::perms::others_exec)  != fs::perms::none ? 'x' : '-')
	          << '\n';

	try
	{
		std::cout << "Removing " << path << " directory\n";
		fs::remove_all(path);
	}
	catch (const fs::filesystem_error& e)
	{
		std::cout << "error: " << e.code() << '\n';
		std::cout << e.what() << '\n';
	}

	try
	{
		std::cout << "Temporary files directory is: " << fs::temp_directory_path() << '\n';
	}
	catch (const fs::filesystem_error& e)
	{
		std::cout << "error: " << e.code() << '\n';
		std::cout << e.what() << '\n';
	}

	try
	{
		std::cout << "root's space:\n";
		fs::space_info space = fs::space(fs::exists("C:/Windows") ? "C:/" : "/dev/sda1");
		std::cout << "  Capacity       Free      Available\n"
		          << std::setw(12) << space.capacity << " " << space.free << " " << space.available << '\n';
	}
	catch (const fs::filesystem_error& e)
	{
		std::cout << "error: " << e.code() << '\n';
		std::cout << e.what() << '\n';
	}

	std::cout << "\nstructed binding to iterate over a map\n";
	std::map<int, std::string> m{ {1, "one"}, {2, "two"}, {3, "three"}, {4, "four"}, {5, "five"} };
	for (const auto& [key, val] : m)
		std::cout << "key: " << key << " val: " << val << '\n';

	std::cin.get();
}
