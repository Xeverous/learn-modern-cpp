//A lambda (or lambda expression) is an anonymous function object that can capture objects in the scope it's defined in.

//the simplest (empty lambda, works as empty void function)
//auto empty_lambda = [](){};

// lambdas can be packed into
// std::function<>;
// because std::function uses type erasure
// template< class R, class... Args >
// class function<R(Args...)> // std::function can have any signature, though mostly used is void()

// with proper template approach, you can utilize it and even create a container of unnamed functions
// std::vector< std::function<void()> > callables;
// and call them this way
// callables[0]();
// this is insanely helpful when eg binding multiple fucntions to a button
// static_cast is used to satify std::bind<std::function<void()>> requiring no arguments
// this effectively hides this, which is needed because a member function needs to know on which object to be called
// settings_button_recalc->GetSignal( sfg::ToggleButton::OnToggle ).Connect( std::bind( static_cast<void(UserInterface::*)()>(&UserInterface::Recalculate), this ) );


// learn more and get more info how to use it on SO documentation
// http://stackoverflow.com/documentation/c%2b%2b/572/lambdas

#include <algorithm>
#include <functional>
#include <iostream>
#include <memory>
#include <vector>


class Item {
public:
	int value;
	Item(int v) { value = v; };
};


bool Compare(const std::shared_ptr<Item>& a, const std::shared_ptr<Item>& b) {
	return a->value < b->value;
}

//C++11 using TYPE_2 = TYPE_1; can be used the same way as keyword typedef TYPE_1 TYPE_2;
using sorting_function = std::function<
	void(std::vector<std::shared_ptr<Item>>::iterator,
         std::vector<std::shared_ptr<Item>>::iterator,
		 std::function<bool(const std::shared_ptr<Item>&,
		                    const std::shared_ptr<Item>&)>)>;


class Algorithm {
public:
	sorting_function func;

	Algorithm() = delete;
	Algorithm(sorting_function func): func(func) {}
};


struct std_sort {
	template <typename RndIt, typename Cmp>
	void operator()(RndIt begin, RndIt end, Cmp cmp) {
		std::sort(begin, end, cmp);
	}
};


int main() {
	std::vector<std::shared_ptr<Algorithm>> vect;
	std::vector<std::shared_ptr<Item>> v;

	vect.push_back(std::make_shared<Algorithm>([](
			std::vector<std::shared_ptr<Item>>::iterator first,
        	std::vector<std::shared_ptr<Item>>::iterator last,
            std::function<bool(const std::shared_ptr<Item>&,
                               const std::shared_ptr<Item>&)> compare_func) {
        std_sort()(first, last, compare_func);
    }));

	for(int i=0; i<10; i++) {
		v.push_back(std::make_shared<Item>(10 - i));
	}

	vect[0]->func(v.begin(), v.end(), Compare);

	for (const auto& x : v) { std::cout << x->value << '\n'; };

	return 0;
}






