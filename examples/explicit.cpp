#include <iostream>

// Class that holds fractions as 2 separate inegers (d is denominator)

class Fraction
{
    int gcd(int a, int b) { return b == 0 ? a : gcd(b, a % b); }
    int n, d;

public:
    /* explicit */ Fraction(int n, int d=1) : n(n/gcd(n, d)), d(d/gcd(n, d)) { }

    int num() const { return n; }
    int den() const { return d; }

    Fraction& operator*=(const Fraction& rhs)
    {
        int new_n = n * rhs.n/gcd(n * rhs.n, d * rhs.d);
        d = d * rhs.d/gcd(n * rhs.n, d * rhs.d);
        n = new_n;
        return *this;
    }

	// function that lets to convert Fraction to double
	/* explicit */ operator double()
    {
        return static_cast<double>(n) / static_cast<double>(d);
    }
};

std::ostream& operator<<(std::ostream& out, const Fraction& f)
{
   return out << f.num() << '/' << f.den() ;
}

bool operator==(const Fraction& lhs, const Fraction& rhs)
{
    return lhs.num() == rhs.num() && lhs.den() == rhs.den();
}

bool operator!=(const Fraction& lhs, const Fraction& rhs)
{
    return !(lhs == rhs);
}

Fraction operator*(Fraction lhs, const Fraction& rhs)
{
    return lhs *= rhs;
}

int main()
{
	Fraction f1(3, 8), f2(1, 2), f3(10, 2);
	std::cout << f1 << " * " << f2 << " = " << f1 * f2 << '\n'
              << f2 << " * " << f3 << " = " << f2 * f3 << '\n'
              <<  2 << " * " << f1 << " = " <<  2 * f1 << '\n'; /////

	if( 7 == Fraction(7, 1)) /////
		std::cout << "true\n";
	else
        std::cout << "false\n";

	double d = f1; /////
	std::cout << "f1 as double: " << d << '\n';
}

// If you uncomment "explicit" keyword:
// 2 * f1              will have to be rewrittem as Fraction(2) * f1
// 7 == Fraction(7, 1) will have to be rewrittem as Fraction(7) == Fraction(7, 1)
// d = f1              will have to be rewrittem as d = static_cast<double>(f1)

// keyword explicit can only be used for
// - constructors
// - convertion functions (for example operator bool())

/*

example output:

3/8 * 1/2 = 3/16
1/2 * 5/1 = 5/2
2 * 3/8 = 3/4
true
f1 as double: 0.375000

 */

// other examples
// http://en.cppreference.com/w/cpp/language/explicit






